#cmake ver req
cmake_minimum_required(VERSION 2.8.3)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)   #for executables
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_LIST_DIR}/cmake/modules/")

#project name
set(project_name darkest_matter)
project(${project_name} CXX)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14  -Wall -Wextra -Wshadow -Wconversion -Wno-long-long -pedantic -Wno-unused-parameter")

file(GLOB_RECURSE source_files "src/*.cpp")
file(GLOB_RECURSE header_files "src/*.hpp")

find_package(OpenGL REQUIRED)
if(OPENGL_FOUND)
    include_directories(${OPENGL_INCLUDE_DIRS})
endif(OPENGL_FOUND)

find_package(OpenAL REQUIRED)
if(OPENAL_FOUND)
    include_directories(${OPENAL_INCLUDE_DIR})
endif(OPENAL_FOUND)

find_package(GLM REQUIRED)
if(GLM_FOUND)
    include_directories(${GLM_INCLUDE_DIRS})
endif()

find_package(Featherkit COMPONENTS structure entity sdl2 audio rendering ui util REQUIRED)
if(FEATHERKIT_FOUND)
    include_directories(${FEATHERKIT_INCLUDE_DIRS})
endif()

find_package(GimGui REQUIRED)
if(GIMGUI_FOUND)
    include_directories(${GIMGUI_INCLUDE_DIRS})
endif()

find_package(Thero REQUIRED)
if(THERO_FOUND)
    include_directories(${THERO_INCLUDE_DIRS})
endif()

add_executable(${project_name} ${header_files} ${source_files})

target_link_libraries(${project_name} ${FEATHERKIT_LIBRARIES} ${OPENGL_LIBRARIES} ${GIMGUI_LIBRARIES})

include_directories("src")

set(BUILD_WALLS_FLOOR FALSE CACHE BOOL "build the walls_floor addon")
if(BUILD_WALLS_FLOOR)
      add_subdirectory(walls_floor)
endif()

set(BUILD_TOPDOWN_RENDERER FALSE CACHE BOOL "build the topdown_renderer addon")
if(BUILD_TOPDOWN_RENDERER)
      add_subdirectory(topdown_renderer)
endif()

set(BUILD_AI_TASK FALSE CACHE BOOL "build the ai_task addon")
if(BUILD_AI_TASK)
      add_subdirectory(ai_task)
endif()
