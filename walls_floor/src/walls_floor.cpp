#include "walls_floor.hpp"
#include <fea/ui/sdl2windowbackend.hpp>
#include <fea/ui/sdl2inputbackend.hpp>
#include <util/texturemaker.hpp>
#include <resources/walls.hpp>
#include <resources/floors.hpp>
#include <util/selector.hpp>

Walls_floor::Walls_floor() :
    mWindow(new fea::SDL2WindowBackend(), fea::VideoMode(1024, 768), "Walls_floor"),
    mInputHandler(new fea::SDL2InputBackend()),
    mRenderer(fea::Viewport({1024, 768}, {0, 0}, fea::Camera({1024 / 2.0f, 768 / 2.0f}))),
    mWallTileMap({100, 100}, {32, 32}, {16, 16}),
    mFloorTileMap({100, 100}, {32, 32}, {16, 16}),
    mHolding(false),
    mWalls(true),
    mButton(fea::Mouse::LEFT)
{
    mWindow.setVSyncEnabled(true);
    mWindow.setFramerateLimit(60);

    mWallsTexture = makeTexture("data/textures/walls/walls.png");
    mFloorsTexture = makeTexture("data/textures/floors/floors.png");

    mWallTileMap.setTexture(mWallsTexture);
    mFloorTileMap.setTexture(mFloorsTexture);

    int32_t index = 1;
    for(auto wallType : rWalls)
    {
        std::cout << "adding " << wallType.first << " as floor id " << index << "\n";
        mWallTileMap.defineType(index, wallType.second.textureCoordinates);

        index++;
    }

    index = 1;
    for(auto floorType : rFloors)
    {
        std::cout << "adding " << floorType.first << " as floor id " << index << "\n";
        mFloorTileMap.defineType(index, floorType.second.textureCoordinates);

        index++;
    }
}

void Walls_floor::loop()
{
    handleInput();

    //update code

//    mRenderer.clear();
//    mRenderer.queue(mFloorTileMap);
    mRenderer.render(mFloorTileMap);
//    mRenderer.queue(mWallTileMap);
    mRenderer.render(mWallTileMap);
//    mRenderer.render();
//    mWindow.swapBuffers();
}

void Walls_floor::handleInput()
{
    fea::Event event;

    while(mInputHandler.pollEvent(event))
    {
        if(event.type == fea::Event::KEYPRESSED)
        {
            if(event.key.code == fea::Keyboard::ESCAPE)
                quit();
            else if(event.key.code == fea::Keyboard::W)
                mWalls = true;
            else if(event.key.code == fea::Keyboard::F)
                mWalls = false;
        }
        else if(event.type == fea::Event::CLOSED)
        {
            quit();
        }
        else if(event.type == fea::Event::RESIZED)
        {
            float w = (float)event.size.width / 2.0f;
            float h = (float)event.size.height / 2.0f;
            mRenderer.setViewport(fea::Viewport({event.size.width, event.size.height}, {0, 0}, fea::Camera({w, h})));
        }
        else if(event.type == fea::Event::MOUSEBUTTONPRESSED)
        {
            mHolding = true;
            mButton = event.mouseButton.button;
        }
        else if(event.type == fea::Event::MOUSEBUTTONRELEASED)
        {
            mHolding = false;
        }
        else if(event.type == fea::Event::MOUSEMOVED)
        {
            if(mHolding)
            {
                glm::uvec2 position(event.mouseMove.x / 32, event.mouseMove.y / 32);
                if(mButton == fea::Mouse::MIDDLE)
                {
                    if(mWalls)
                        mWallTileMap.unset((glm::ivec2)position);
                    else
                        mFloorTileMap.unset((glm::ivec2)position);
                }
                else
                {
                    if(mWalls)
                        mWallTileMap.set((glm::ivec2)position, mButton == fea::Mouse::LEFT ? 1 : 2);
                    else
                        mFloorTileMap.set((glm::ivec2)position, mButton == fea::Mouse::LEFT ? 1 : 2);
                }
            }
        }
    }
}
