#include "gotosolver.hpp"
#include "world.hpp"
#include "entity.hpp"
#include "jobs.hpp"
#include "util/grid.hpp"
#include "tiles.hpp"

GotoSolver::GotoSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mFailed(false)
{
    mTarget = job.jobData.at("position").get<glm::ivec2>();
}

JobSolver::UpdateResult GotoSolver::update()
{
    if(mFailed)
        return fail();

    Entity& entity = mExternal.at("entity").get<std::reference_wrapper<Entity>>().get();
    glm::ivec2 entityTilePosition = entity.tilePosition();
    glm::vec2 entityPosition = entity.position();

    const Grid<TileType>& worldMap = mExternal.at("world").get<std::reference_wrapper<const World>>().get().map();

    if(!mPath)
    {
        //find path
        TileAdaptor adaptor(worldMap);
        auto path = mPathfinder.findPath(adaptor, entityTilePosition, mTarget);

        if(path.empty())
        {
            return fail();
        }
        else
            mPath = path;
    }



    //walk towards path, ticking of the completed ones. 
    if(mPath->size() > 0)
    {
        auto nextTilePosition = (*mPath)[0];
        auto nextPosition = static_cast<glm::vec2>(nextTilePosition) * 32.0f + glm::vec2(16.0f, 16.0f);

        TileType tileType = worldMap.at(nextTilePosition);

        if(tileType == LOCKED_DOOR)
        {
            mPath.reset();
            return require({UNLOCK_DOOR, {{"door_position", nextTilePosition}}});
        }
        else if(tileType == METAL_DOOR)
        {
            mPath.reset();
            return require({OPEN_BUTTON_DOOR, {{"door_position", nextTilePosition}}});
        }

        if(glm::distance(entityPosition, nextPosition) > 2.0f)
        {
            entity.walkInDirection(glm::normalize(static_cast<glm::vec2>(nextPosition - entityPosition)));
        }
        else
        {
            mPath->pop_front();
        }
    }
    else
    {
        return succeed();
    }

    return working({{"state_string", std::string("Walking towards target")}});
}

void GotoSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void GotoSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == LOCKED_DOOR)
    {
        mFailed = true;
    }
}
