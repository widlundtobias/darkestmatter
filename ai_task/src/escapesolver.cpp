#include "escapesolver.hpp"
#include "entity.hpp"
#include "jobs.hpp"
#include "world.hpp"

EscapeSolver::EscapeSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mFailed(false)
{
}

JobSolver::UpdateResult EscapeSolver::update()
{
    if(mFailed)
    {
        return fail();
    }

    if(mExitPosition)
    {
        glm::ivec2 entityPosition = mExternal.at("entity").get<std::reference_wrapper<Entity>>().get().tilePosition();

        if(entityPosition != *mExitPosition)
        {
            return require({GOTO, {{"position", *mExitPosition}}});
        }
        else
        {
            return succeed();
        }
    }
    else
    {
        mExitPosition.reset(mExternal.at("world").get<std::reference_wrapper<const World>>().get().exitPosition());
    }

    return working({{"state_string", std::string("Escaping dungeon")}});
}

void EscapeSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void EscapeSolver::notifyFail(AnyMap<std::string> failedData)
{
    mFailed = true;
}
