#pragma once
#include <fea/util.hpp>
#include "jobai/jobsolver.hpp"
#include "glm.hpp"
#include <fea/rendering/glmhash.hpp>
#include "tileadaptor.hpp"

class GotoSolver : public JobSolver
{
    public:
        GotoSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::Pathfinder<TileAdaptor> mPathfinder;
        th::Optional<fea::Pathfinder<TileAdaptor>::Path> mPath;
        glm::ivec2 mTarget;
        bool mFailed;
};
