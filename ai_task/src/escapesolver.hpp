#pragma once
#include "jobai/jobsolver.hpp"
#include "glm.hpp"

class EscapeSolver : public JobSolver
{
    public:
        EscapeSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        th::Optional<glm::ivec2> mExitPosition;
        bool mFailed;
};
