#include "ai_task.hpp"
#include <fea/ui/sdl2windowbackend.hpp>
#include <fea/ui/sdl2inputbackend.hpp>
#include "jobs.hpp"
#include "escapesolver.hpp"
#include "gotosolver.hpp"
#include "unlockdoorsolver.hpp"
#include "openbuttondoorsolver.hpp"
#include "jobai/solverfactory.hpp"
#include "texturemaker.hpp"
#include <fstream>

Ai_task::Ai_task() :
    mWindow(new fea::SDL2WindowBackend(), fea::VideoMode(1024, 768), "Ai_task"),
    mInputHandler(new fea::SDL2InputBackend()),
    mRenderer(fea::Viewport({1024, 768}, {0, 0}, fea::Camera({1024 / 2.0f, 768 / 2.0f}))),
    mFont(std::ifstream("data/fonts/Timeless.ttf", std::ios::binary)),
    mGui({"root"},
    {//attributes
        {"position", glm::ivec2(1024 - 442, 768 - 129) },
        {"size", glm::ivec2(442, 129) },
        {"image_coords", Rectangle{{0, 0}, {32, 32}}},
        {"color", fea::Color(61, 184, 130)},
        {"text", std::string("Idle")},
        {"text_color", fea::Color(237, 237, 237)},
        {"text_size", 25},
        {"font", mFont.name()},
    },
    {//children
    }),
    mGuiDrawable(mGui),
    mEscapeJob{ESCAPE, {}},
    mWorld({32, 24}, mDungeonTexture,
    {
      {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,1,2,1,1,1,1,1,1,1,1,1,1,0,0,5,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,3,3,3,1,0,1,0,1,1,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,0,1,0,0,1,0,0,0,0,0,0,0,0,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,3,3,3,0,1,0,1,1,0,1,1,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,0,0,1,0,0,3,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,3,0,1,1,0,1,0,1,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,3,0,0,0,1,0,0,0,0,1,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,3,0,3,1,0,1,1,1,1,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,0,0,0,0,0,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,3,0,0,1,1,1,4,1,1,1,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,3,0,1,6,0,0,0,0,6,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,3,1,0,0,3,3,0,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,3,3,0,0,1,0,3,3,3,3,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,3,1,0,3,3,3,3,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,3,0,3,0,0,0,0,3,3,0,0,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,0,0,0,0,1,6,0,0,0,0,6,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
      {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,8,8,8,8,8,8,8,8,8,8,8,8,8},
    }),
    mEntity({32.0f, 32.0f}, mFaceTexture, mWorld),
    mBrain(
    {
       { ESCAPE, solverFactory<EscapeSolver> },
       { GOTO, solverFactory<GotoSolver> },
       { UNLOCK_DOOR, solverFactory<UnlockDoorSolver> },
       { OPEN_BUTTON_DOOR, solverFactory<OpenButtonDoorSolver> },
    }, {{"world", std::cref(mWorld)},
        {"entity", std::ref(mEntity)}})
{
    mWindow.setVSyncEnabled(true);
    mWindow.setFramerateLimit(60);

    mFaceTexture = makeTexture("data/textures/test/face.png");
    mDungeonTexture = makeTexture("data/textures/test/dungeontiles.png");
    mWhiteTexture = makeTexture("data/textures/test/white.png");

    mGuiDrawable.registerTexture("white", mWhiteTexture);
    mGui.createAttribute("texture", std::string("white"));

    mGuiDrawable.addFont(mFont);

    mEntity.quad().setTexture(mFaceTexture);
    mEntity.quad().setSubrect({0, 0}, {32, 32});
    mEntity.setPosition({128, 128});

    mBrain.solve(mEscapeJob);
}

void Ai_task::loop()
{
    handleInput();

    //update code
    auto result = mBrain.update();

    if(result.status == JobStatus::IDLE)
        mCurrentTaskString = "Idle";
    else
    {
        auto stateStringIterator = result.output.find("state_string");
        if(stateStringIterator != result.output.end())
        {
            mCurrentTaskString = stateStringIterator->second.get<std::string>();
        }
        else
            mCurrentTaskString = "";
    }

    updateTaskText();

//    mRenderer.clear();
//    mRenderer.queue(mWorld);
    mRenderer.render(mWorld);
//    mRenderer.queue(mEntity);
    mRenderer.render(mEntity);
//    mRenderer.queue(mGuiDrawable);
    mRenderer.render(mGuiDrawable);
//    mRenderer.render();

    mWindow.swapBuffers();
}

void Ai_task::handleInput()
{
    fea::Event event;
    while(mInputHandler.pollEvent(event))
    {
        if(event.type == fea::Event::KEYPRESSED)
        {
            if(event.key.code == fea::Keyboard::ESCAPE)
                quit();
        }
        else if(event.type == fea::Event::CLOSED)
        {
            quit();
        }
        else if(event.type == fea::Event::RESIZED)
        {
            float w = (float)event.size.width / 2.0f;
            float h = (float)event.size.height / 2.0f;
            mRenderer.setViewport(fea::Viewport({event.size.width, event.size.height}, {0, 0}, fea::Camera({w, h})));
        }
        else if(event.type == fea::Event::MOUSEBUTTONPRESSED)
        {
            //
        }
        else if(event.type == fea::Event::MOUSEBUTTONRELEASED)
        {
            //
        }
        else if(event.type == fea::Event::MOUSEMOVED)
        {
            //
        }
    }
}

void Ai_task::updateTaskText()
{
    mGui.setAttribute("text", mCurrentTaskString);
}
