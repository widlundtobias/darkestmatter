#pragma once
#include <fea/render2d.hpp>
#include <thero/optional.hpp>
#include <unordered_set>
#include <deque>
#include "util/grid.hpp"
#include "tiles.hpp"

class World
{
    public:
        World(const glm::ivec2& size, const fea::Texture& tileTexture, std::deque<std::deque<TileType>> tiles);
        const Grid<TileType>& map() const;
        glm::ivec2 exitPosition() const;
        th::Optional<glm::ivec2> searchKey(const glm::ivec2& position) const;
        std::vector<fea::RenderEntity> getRenderInfo() const;
        bool isSolidAt(const glm::vec2& position) const;
        void interact(const glm::ivec2& tilePosition);
        const std::unordered_set<glm::ivec2>& buttonPositions() const;
    private:
        void setTile(const glm::ivec2& position, TileType tile);
        Grid<TileType> mMap;
        fea::TileMap mTiles;
        glm::ivec2 mKeyPosition;
        glm::ivec2 mLockedDoorPosition;
        glm::ivec2 mButtonDoorPosition;
        std::unordered_set<glm::ivec2> mButtonPositions;
};
