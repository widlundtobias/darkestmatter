#include "unlockdoorsolver.hpp"
#include "world.hpp"
#include "entity.hpp"
#include "jobs.hpp"
#include "util/grid.hpp"
#include "tiles.hpp"

UnlockDoorSolver::UnlockDoorSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mStatus(SEARCHING),
    mCurrentSearchDirection(0.0f, 1.0f)
{
    mDoorPosition = job.jobData.at("door_position").get<glm::ivec2>();
}

JobSolver::UpdateResult UnlockDoorSolver::update()
{
    Entity& entity = mExternal.at("entity").get<std::reference_wrapper<Entity>>().get();
    const World& world = mExternal.at("world").get<std::reference_wrapper<const World>>().get();

    if(mStatus == SEARCHING)
    {
        bool collided = entity.walkInDirection(mCurrentSearchDirection);   

        if(!(rand() % 200) || collided)
        {
            int32_t x = rand() % 20 - 10;
            int32_t y = rand() % 20 - 10;
            if(x == 0 && y == 0)
                y = 1;

            mCurrentSearchDirection = glm::normalize(glm::vec2(x, y));
        }

        mKeyPosition = world.searchKey(entity.tilePosition());

        if(mKeyPosition)
        {
            mStatus = GRABBING_KEY;
        }
    }
    else if(mStatus == GRABBING_KEY)
    {
        auto keyWorldLocation = static_cast<glm::vec2>(*mKeyPosition) * 32.0f + glm::vec2(16.0f, 16.0f);
        auto entityLocation = entity.position();

        if(glm::distance(keyWorldLocation, entityLocation) > 2.0f)
        {
            entity.walkInDirection(glm::normalize(static_cast<glm::vec2>(keyWorldLocation - entityLocation)));
        }
        else
        {
            entity.interact();
            return succeed();
        }
    }

    std::string stateText;

    if(mStatus == SEARCHING)
        stateText = "Searching for key...";
    else
        stateText = "Picking up key";

    return working({{"state_string", stateText}});
}

void UnlockDoorSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void UnlockDoorSolver::notifyFail(AnyMap<std::string> failedData)
{
}
