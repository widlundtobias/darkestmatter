#pragma once
#include "glm/glm.hpp"
#include <fea/render2d.hpp>

class World;

class Entity
{
    public:
        Entity(const glm::vec2& size, const fea::Texture& texture, World& world);
        void setPosition(const glm::vec2& position);
        glm::ivec2 tilePosition() const;
        glm::vec2 position() const;
        std::vector<fea::RenderEntity> getRenderInfo() const;
        bool walkInDirection(const glm::vec2& direction);
        fea::SubrectQuad& quad();
        void interact();
    private:
        fea::SubrectQuad mQuad;
        float mMoveSpeed;
        World& mWorld;
};
