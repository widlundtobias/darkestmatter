#include "openbuttondoorsolver.hpp"
#include "world.hpp"
#include "entity.hpp"
#include "jobs.hpp"
#include "util/grid.hpp"
#include "tiles.hpp"

OpenButtonDoorSolver::OpenButtonDoorSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external)
{
}

JobSolver::UpdateResult OpenButtonDoorSolver::update()
{
    const World& world = mExternal.at("world").get<std::reference_wrapper<const World>>().get();

    const auto& buttonPositions = world.buttonPositions();

    if(!buttonPositions.empty())
    {
        Entity& entity = mExternal.at("entity").get<std::reference_wrapper<Entity>>().get();
        glm::ivec2 entityTilePosition = entity.tilePosition();

        const Grid<TileType>& worldMap = mExternal.at("world").get<std::reference_wrapper<const World>>().get().map();
        TileAdaptor adaptor(worldMap);

        int32_t shortestPathLength = 100000;
        glm::ivec2 selectedPosition;

        for(const auto& position : buttonPositions)
        {
            auto path = mPathfinder.findPath(adaptor, entityTilePosition, position);
            int32_t pathLength = static_cast<int32_t>(path.size());

            if(pathLength < shortestPathLength)
            {
                shortestPathLength = pathLength;
                selectedPosition = position;
            }
        }

        return require({GOTO, {{"position", selectedPosition}}});
    }
    else
    {
        return succeed();
    }

    return working({{"state_string", std::string("Opening button door")}});
}

void OpenButtonDoorSolver::notifySuccess(AnyMap<std::string> completedData)
{
    Entity& entity = mExternal.at("entity").get<std::reference_wrapper<Entity>>().get();
    entity.interact();
}

void OpenButtonDoorSolver::notifyFail(AnyMap<std::string> failedData)
{
}
