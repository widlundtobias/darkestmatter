#include "world.hpp"
#include <thero/assert.hpp>

World::World(const glm::ivec2& size, const fea::Texture& tileTexture, std::deque<std::deque<TileType>> tiles):
    mMap(size, EMPTY),
    mTiles({32, 32}, {16, 16})
{
    mTiles.addTileDefinition(EMPTY, {{0, 0}});
    mTiles.addTileDefinition(WALL, {{1, 0}});
    mTiles.addTileDefinition(LOCKED_DOOR, {{2, 0}});
    mTiles.addTileDefinition(METAL_DOOR, {{0, 1}});
    mTiles.addTileDefinition(SPIKES, {{1, 1}});
    mTiles.addTileDefinition(KEY, {{2, 1}});
    mTiles.addTileDefinition(BUTTON, {{0, 2}});
    mTiles.addTileDefinition(PUSHED_BUTTON, {{1, 2}});
    mTiles.addTileDefinition(GRASS, {{2, 2}});

    TH_ASSERT(tiles.size() == static_cast<size_t>(size.y), "Not valid size of tiles given");

    for(int32_t y = 0; y < size.y; y++)
    {
        const auto& xRow = tiles[static_cast<size_t>(y)];

        TH_ASSERT(xRow.size() == static_cast<size_t>(size.x), "Not valid size of tiles given");

        for(int32_t x = 0; x < size.x; x++)
        {
            setTile({x, y}, xRow[static_cast<size_t>(x)]);
        }
    }

    mTiles.setTexture(tileTexture);
}

const Grid<TileType>& World::map() const
{
    return mMap;
}

glm::ivec2 World::exitPosition() const
{
    return {26, 9};
}

void World::setTile(const glm::ivec2& position, TileType tile)
{
    TileType oldTile = mMap.at(position);

    if(oldTile == BUTTON)
        mButtonPositions.erase(position);

    mTiles.setTile(position, tile);
    mMap.set(position, static_cast<TileType>(tile));

    if(tile == KEY)
        mKeyPosition = position;
    else if(tile == LOCKED_DOOR)
        mLockedDoorPosition = position;
    else if(tile == METAL_DOOR)
        mButtonDoorPosition = position;
    else if(tile == BUTTON)
        mButtonPositions.insert(position);
}

th::Optional<glm::ivec2> World::searchKey(const glm::ivec2& position) const
{
    th::Optional<glm::ivec2> result;

    int32_t distance = std::abs(position.x - mKeyPosition.x) + std::abs(position.y - mKeyPosition.y);

    if(distance < 4)
        result.reset(mKeyPosition);

    return result;
}

std::vector<fea::RenderEntity> World::getRenderInfo() const
{
    return mTiles.getRenderInfo();
}

bool World::isSolidAt(const glm::vec2& position) const
{
    TileType type = mMap.at(static_cast<glm::ivec2>(position / 32.0f));
    return !(type == EMPTY || type == SPIKES || type == KEY || type == BUTTON || type == PUSHED_BUTTON || type == GRASS);
}

void World::interact(const glm::ivec2& tilePosition)
{
    if(tilePosition == mKeyPosition)
    {
        setTile(mKeyPosition, EMPTY);
        setTile(mLockedDoorPosition, EMPTY);
    }
    if(mButtonPositions.count(tilePosition))
    {
        setTile(tilePosition, PUSHED_BUTTON);

        if(mButtonPositions.empty())
            setTile(mButtonDoorPosition, EMPTY);
    }
}

const std::unordered_set<glm::ivec2>& World::buttonPositions() const
{
    return mButtonPositions;
}
