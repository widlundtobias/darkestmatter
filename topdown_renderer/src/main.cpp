#include "topdown_renderer.hpp"

int main(int argc, char *argv[])
{
    Topdown_renderer topdown_renderer;
    topdown_renderer.run(argc, argv);
}
