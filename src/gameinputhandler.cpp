#include "gameinputhandler.hpp"

GameInputHandler::GameInputHandler(fea::MessageBus& bus, fea::InputHandler& handler):
    mHandler(handler),
    mBus(bus)
{
}

void GameInputHandler::process()
{
    fea::Event event;
    while(mHandler.pollEvent(event))
    {
        if(event.type == fea::Event::KEYPRESSED)
        {
            if(event.key.code == fea::Keyboard::ESCAPE)
                mBus.send(QuitMessage());
            else if(event.key.code == fea::Keyboard::A)
                mDirection.leftActive(true);
            else if(event.key.code == fea::Keyboard::D)
                mDirection.rightActive(true);
            else if(event.key.code == fea::Keyboard::W)
                mDirection.upActive(true);
            else if(event.key.code == fea::Keyboard::S)
                mDirection.downActive(true);

            mBus.send(KeyPressedMessage{event.key.code});
        }
        if(event.type == fea::Event::KEYRELEASED)
        {
            if(event.key.code == fea::Keyboard::A)
                mDirection.leftActive(false);
            else if(event.key.code == fea::Keyboard::D)
                mDirection.rightActive(false);
            else if(event.key.code == fea::Keyboard::W)
                mDirection.upActive(false);
            else if(event.key.code == fea::Keyboard::S)
                mDirection.downActive(false);
            else if(event.key.code == fea::Keyboard::P)
                mBus.send(TogglePauseMessage{});

            mBus.send(KeyPressedMessage{event.key.code});
        }
        else if(event.type == fea::Event::CLOSED)
        {
            mBus.send(QuitMessage());
        }
        else if(event.type == fea::Event::RESIZED)
        {
            mBus.send(ResizeMessage{{event.size.width, event.size.height}});
        }
        else if(event.type == fea::Event::MOUSEBUTTONPRESSED)
        {
            mBus.send(MouseClickMessage{{event.mouseButton.x, event.mouseButton.y}, event.mouseButton.button});
        }
        else if(event.type == fea::Event::MOUSEBUTTONRELEASED)
        {
            mBus.send(MouseReleaseMessage{{event.mouseButton.x, event.mouseButton.y}});
        }
        else if(event.type == fea::Event::MOUSEMOVED)
        {
            glm::ivec2 newPosition = {event.mouseMove.x, event.mouseMove.y};
            mBus.send(MouseMoveMessage{newPosition, mLastMousePosition});
            mLastMousePosition = newPosition;
        }
        else if(event.type == fea::Event::MOUSEWHEELMOVED)
        {
            int32_t mouseWheelMagnitude = std::abs(event.mouseWheel.delta); //needs to normalise mousewheel since it's not always 1 or -1. In chrome it is 53 and -53
            mBus.send(ViewZoomedMessage{event.mouseWheel.delta / mouseWheelMagnitude});
        }
    }

    mBus.send(ViewPanDirectionMessage{static_cast<glm::vec2>(mDirection.direction())});
}
