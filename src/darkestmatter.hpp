#include "messages.hpp"
#include "audioplayer.hpp"
#include "util/logger.hpp"

class GameSession;

class DarkestMatter : public fea::Application,
    public fea::MessageReceiver<
        QuitMessage,
        LogMessage
    >
{
    public:
        DarkestMatter();
        ~DarkestMatter();
        void handleMessage(const QuitMessage& message) override;
        void handleMessage(const LogMessage& message) override;
    protected:
        virtual void loop() override;
    private:
        Logger mLogger;
        fea::MessageBus mBus;
        fea::Window mWindow;
        fea::InputHandler mFeaInputHandler;

        //AudioPlayer mAudioPlayer;

        std::unique_ptr<GameSession> mGameSession;
        fea::FrameTimer mFrameTimer;
};
