#pragma once
#include "messages.hpp"
#include "util/directionresolver.hpp"

class GameInputHandler
{
    public:
        GameInputHandler(fea::MessageBus& bus, fea::InputHandler& handler);
        void process();
    private:
        fea::InputHandler& mHandler;
        fea::MessageBus& mBus;
        DirectionResolver mDirection;

        glm::ivec2 mLastMousePosition;
};
