#include "darkestmatter.hpp"
#include "gamesession.hpp"

DarkestMatter::DarkestMatter() :
    mLogger(LogLevel::VERBOSE),
    mWindow(new fea::SDL2WindowBackend(), fea::VideoMode(1024, 768), "DarkestMatter"),
    mFeaInputHandler(new fea::SDL2InputBackend())
    //mAudioPlayer(mBus)
{
    subscribe(mBus, *this, false);

    mLogger.log("initialising the game", LogLevel::INFO);
    bool vsync = true;
    uint32_t framerateLimit = 60;
    //bool vsync = false;
    //uint32_t framerateLimit = 6000;

    mLogger.log(vsync ? "enabling vsync" : "disabling vsync", LogLevel::VERBOSE);
    mWindow.setVSyncEnabled(vsync);
    mLogger.log("setting framerate limit to " + std::to_string(framerateLimit), LogLevel::VERBOSE);
    mWindow.setFramerateLimit(framerateLimit);

    mLogger.log("creating game session", LogLevel::VERBOSE);
    mGameSession = std::make_unique<GameSession>(mBus, mFeaInputHandler);
}

DarkestMatter::~DarkestMatter()
{
    mLogger.log("shutting down", LogLevel::INFO);
}

void DarkestMatter::handleMessage(const QuitMessage& message)
{
    (void)message;
    quit();
}

void DarkestMatter::handleMessage(const LogMessage& message)
{
    mLogger.log(message.message, message.level);
}

void DarkestMatter::loop()
{
    mGameSession->update();
    //mAudioPlayer.update();

    mWindow.swapBuffers();

    mFrameTimer.sample();

    std::string fps = std::to_string(mFrameTimer.fps());
    mWindow.setTitle("DarkestMatter - " + fps + " FPS");
}
