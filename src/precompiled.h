#include <cstdint>
#include <unordered_map>
#include <map>
#include <set>
#include <unordered_set>
#include <vector>
#include <list>
#include <deque>
#include <queue>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <memory>
#include <chrono>
#include <random>

#include <thero/smartenum.hpp>
#include <thero/optional.hpp>
#include <thero/polymorphicwrapper.hpp>
#include <thero/randomselector.hpp>

#include <fea/render2d.hpp>
#include <fea/userinterface.hpp>
#include <fea/ui/sdl2windowbackend.hpp>
#include <fea/ui/sdl2inputbackend.hpp>
#include <fea/util.hpp>
#include <fea/entitysystem.hpp>
#include <fea/audio.hpp>
#include <fea/structure.hpp>
#include <fea/entity/jsonentityloader.hpp>
#include <fea/entity/glmtypeadder.hpp>

#include <gimgui/data/element.hpp>
#include <gimgui/data/font.hpp>
#include <gimgui/data/bitmap.hpp>
#include <gimgui/logic/renderdatagenerator.hpp>

#include <glm/gtx/io.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "util/log.hpp"
#include "util/anymap.hpp"
#include "util/anytools.hpp"
#include "util/numberpool.hpp"
#include "util/pimpl.hpp"
