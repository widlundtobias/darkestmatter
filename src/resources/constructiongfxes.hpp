#pragma once
#include "precompiled.h"

struct ConstructionGfxResource
{
    std::string texture;
    glm::ivec2 start;
    glm::ivec2 size;
};

extern std::unordered_map<std::string, ConstructionGfxResource> rConstructionGfxes;
