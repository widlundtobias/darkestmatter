#pragma once
#include <string>
#include <unordered_map>

struct TextureResource
{
    std::string texturePath;
};

extern std::unordered_map<std::string, TextureResource> rTextures;
