#include "textures.hpp"

std::unordered_map<std::string, TextureResource> rTextures =
{
    {"yellowstar", { "data/textures/test/yellowstar.png" },},
    {"soldier", { "data/textures/sprites/soldier.png" },},
    {"blob", { "data/textures/test/blob.png" },},
    {"blob2", { "data/textures/test/blob2.png" },},
    //items
    {"goldbar", { "data/textures/sprites/items/gold.png" },},
    {"ironbar", { "data/textures/sprites/items/iron.png" },},
    {"crate", { "data/textures/sprites/items/crate.png" },},
    //tiles
    {"walls", { "data/textures/walls/walls.png" },},
    {"ores", { "data/textures/ores/ores.png" },},
    {"floors", { "data/textures/floors/floors.png" },},
    //vehicles
    {"rocket_r5", { "data/textures/sprites/vehicles/rocket_r5.png" },},
    //construction
    {"constructions", { "data/textures/structures/constructions.png" },},
    //ui
    {"white", { "data/textures/ui/white.png" },},
    {"hammericon", { "data/textures/ui/hammericon.png" },},
    {"wallicon", { "data/textures/ui/wallicon.png" },},
    {"flooricon", { "data/textures/ui/flooricon.png" },},
};
