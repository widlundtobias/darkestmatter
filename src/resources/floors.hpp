#pragma once
#include "precompiled.h"

struct FloorResource
{
    struct MaterialRequirement
    {
        std::string materialName;
        int32_t amount;
    };

    std::string name;
    std::string description;
    std::string texture;
    glm::ivec2 textureCoordinates;
    int32_t constructionTime;
    std::string constructionGraphics;
    bool isConstructed;
    std::deque<MaterialRequirement> materialsNeeded;
};

extern const std::unordered_map<std::string, FloorResource> rFloors;
