#pragma once
#include "precompiled.h"

struct GroundResource
{
    std::string name;
    std::string description;
    std::string texture;
    glm::ivec2 textureCoordinates;
};

extern const std::unordered_map<std::string, GroundResource> rGrounds;
