#include "fonts.hpp"

std::unordered_map<std::string, FontResource> rFonts =
{
    {"ubuntu", { "data/fonts/ubuntu/Ubuntu-R.ttf" },},
    {"ubuntu-mono", { "data/fonts/ubuntu/UbuntuMono-R.ttf" },},
};
