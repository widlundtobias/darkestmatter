#include "walls.hpp"

const std::unordered_map<std::string, WallResource> rWalls =
{
    {
        "steel_wall",
        {
            "Steel wall",                               //name
            "This is a very thick wall out of steel!",  //description
            100,                                        //density
            250,                                        //time to construct
            "walls",                                    //texture
            {0, 0},                                     //texture coordinates
            "wall_construction",                        //construction graphics
            true,
            {                                           //materials needed
                {
                    "iron_bar",                           //material name
                    3,                                    //amount needed
                },
                {
                    "gold_bar",                           //material name
                    1,                                    //amount needed
                },
            },
        },
    },
    {
        "rock_wall",
        {
            "Rock wall",
            "This is a very thick wall out of rock!",
            50,
            100,
            "walls",
            {0, 1},
            "wall_construction",                        //construction graphics
            false,
            {
                {
                    "iron_bar",
                    3,
                },
            },
        },
    },
};

const std::unordered_map<std::string, WallOreResource> rWallOre =
{
    {
        "gold_ore",
        {
            "Gold ore",                               //name
            "Very valuable!",                         //description
            "ores",                                   //texture
            {0, 0},                                   //texture coordinates

            2,4,8,
            0.5f, 0.4f, 0.1f
        },
    },
    {
        "iron_ore",
        {
            "Iron ore",                               //name
            "Good for construction",                  //description
            "ores",                                   //texture
            {1, 0},                                   //texture coordinates

            2,4,8,
            0.5f, 0.4f, 0.1f
        },
    },
};
