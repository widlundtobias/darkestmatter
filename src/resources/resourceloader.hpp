#pragma once
#include <functional>
#include <string>

class ResourceStorage;
class RenderingSystem;

struct WallResource;
struct WallOreResource;
struct FloorResource;
struct GroundResource;

struct ItemResource;

struct EntityGfxResource;

struct ConstructionGfxResource;

class ResourceLoader
{
    public:
        void loadTextures(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const std::string&)> loadedCallback) const;
        void loadFonts(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const std::string&)> loadedCallback) const;
        void loadWalls(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const WallResource&)> loadedCallback) const;
        void loadWallOre(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const WallOreResource&)> loadedCallback) const;
        void loadFloors(ResourceStorage& storage, std::function<void(int32_t, const std::string& name, const FloorResource&)> loadedCallback) const;
        void loadGrounds(ResourceStorage& storage, std::function<void(int32_t, const std::string& name, const GroundResource&)> loadedCallback) const;
        void loadItems(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const ItemResource&)> loadedCallback) const;
//        void loadRooms(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const RoomResource&)> loadedCallback) const;
        void loadAnimatedSprites(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const EntityGfxResource&)> loadedCallback) const;
        void loadConstructionSprites(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const ConstructionGfxResource&)> loadedCallback) const;
    private:
};
