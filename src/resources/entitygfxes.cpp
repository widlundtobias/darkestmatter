#include "entitygfxes.hpp"

std::unordered_map<std::string, EntityGfxResource> rEntityGfxes = 
{
    {
        "star",
        {
            {16, 16},              //size
            {0, 0},                //offset
            "yellowstar",          //texture
            "twinkle",             //default animation
            {
                {                  //animation1
                    "twinkle",     //name
                    {
                        {0, 0},    //frame start
                        {16, 16},  //frame end
                        3,         //frame amount
                        10,        //delay between frames
                        true,      //loop
                    },
                },
                {
                    "break",
                    {
                        {16, 16},
                        {16, 16},
                        3,
                        10,
                        false,
                    },
                }
            },
        },
    }, {
        "soldier",
        {
            {21, 31},
            {0, 0},                //offset
            "soldier",
            "idle",
            {
                {
                    "idle",
                    {
                        {0, 0},
                        {21, 31},
                        2,
                        30,
                        true,
                    },
                },
            },
        },
    }, {
        "rocket",
        {
            {122, 256},
            {0, -80},                //offset
            "rocket_r5",
            "idle",
            {
                {
                    "idle",
                    {
                        {0, 0},
                        {122, 256},
                        1,
                        0,
                        true,
                    },
                },
            },
        },
    }, {
        "blob",
        {
            {20, 32},
            {0, 0},                //offset
            "blob",
            "walk_down",
            {
                {
                    "idle",
                    {
                        {0, 0},
                        {20, 32},
                        4,
                        7,
                        true,
                    },
                },
                {
                    "walk_right",
                    {
                        {0, 32},
                        {20, 32},
                        4,
                        7,
                        true,
                    },
                },
                {
                    "walk_left",
                    {
                        {0, 64},
                        {20, 32},
                        4,
                        7,
                        true,
                    },
                },
                {
                    "walk_up",
                    {
                        {0, 96},
                        {20, 32},
                        4,
                        7,
                        true,
                    },
                },
                {
                    "walk_down",
                    {
                        {0, 128},
                        {20, 32},
                        4,
                        7,
                        true,
                    },
                },
            },
        },
    }, {
        "blob2",
        {
            {32, 32},
            {0, 0},                //offset
            "blob2",
            "idle",
            {
                {
                    "idle",
                    {
                        {0, 0},
                        {32, 32},
                        5,
                        7,
                        true,
                    },
                },
            },
        },
    },
};
