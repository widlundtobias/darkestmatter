#pragma once
#include <string>
#include <deque>
#include <glm/glm.hpp>

struct ItemGfxResource
{
    std::string texture;
    glm::vec2 size;
    glm::ivec2 subRectStart;
    glm::ivec2 subRectSize;
    std::deque<int32_t> amountThreshold;
};
