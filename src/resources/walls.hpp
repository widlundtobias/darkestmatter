#pragma once
#include "precompiled.h"

struct WallResource
{
    struct MaterialRequirement
    {
        std::string materialName;
        int32_t amount;
    };

    std::string name;
    std::string description;
    int32_t density;
    int32_t constructionTime;
    std::string texture;
    glm::ivec2 textureCoordinates;
    std::string constructionGraphics;
    bool isConstructed;
    std::deque<MaterialRequirement> materialsNeeded;
};

struct WallOreResource
{
    std::string name;
    std::string description;
    std::string texture;
    glm::ivec2 textureCoordinates;

    int32_t lowAmount;
    int32_t midAmount;
    int32_t higAmount;

    float lowProb;
    float midProb;
    float higProb;
};

extern const std::unordered_map<std::string, WallResource> rWalls;
extern const std::unordered_map<std::string, WallOreResource> rWallOre;
