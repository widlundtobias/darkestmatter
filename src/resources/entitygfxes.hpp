#pragma once
#include <unordered_map>
#include <string>
#include "animationresource.hpp"

struct EntityGfxResource
{
    glm::ivec2 size;
    glm::ivec2 offset;
    std::string texture;
    std::string defaultAnimation;
    std::unordered_map<std::string, AnimationResource> animations;
};

extern std::unordered_map<std::string, EntityGfxResource> rEntityGfxes;
