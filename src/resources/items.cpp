#include "items.hpp"

std::unordered_map<std::string, ItemResource> rItems =
{
    {
        "gold_bar",
        {
            12400,                //weight in grams
            5,                    //max stack limit
            Sizes::CUP * 3,
            false,                //big
            {                     //properties
                "metal",
                "construction_material"
            },
            {                     //default data
                {
                    "carbs",
                    {
                        INT,
                        "5"
                    },
                },
            },
            {                     //invoke handlers
                {
                    ItemAction::EAT,
                    "feed_handler"
                },
            },
            {                     //gfx
                "goldbar",          //texture
                {16.0f, 16.0f},     //size
                {0, 0},             //texture start
                {16, 16},           //texture size
                {2, 5},             //stack gfx thresholds
            },
        },
    },
    {
        "iron_bar",
        {
            4960,
            5,
            Sizes::CUP * 3,
            false,                //big
            {                     //properties
                "metal",
                "construction_material",
            },
            {                     //default data
                {
                    "carbs",
                    {
                        INT, "12"
                    },
                },
            },
            {                     //invoke handlers
                //{
                //    ItemAction::EAT, "feed_handler"
                //},
            },
            {
                "ironbar",
                {16.0f, 16.0f},
                {0, 0},
                {16, 16},
                {2, 5},
            },
        },
    },
    {
        "crate",
        {
            20000,
            1,                    //max stack
            Sizes::BATH_TUB * 2,
            true,                //big
            {                     //properties
                "container",
            },
            {                     //default data
            },
            {                     //invoke handlers
                {ItemAction::PUT, "put_handler"},
                {ItemAction::INSPECT, "inspect_handler"},
                {ItemAction::TAKE_OUT, "take_out_handler"}
            },
            {
                "crate",
                {32.0f, 32.0f},
                {0, 0},
                {32, 32},
                {1},
            },
        },
    },
};
