#pragma once
#include <string>
#include <unordered_map>

struct FontResource
{
    std::string fontPath;
};

extern std::unordered_map<std::string, FontResource> rFonts;
