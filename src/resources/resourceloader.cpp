#include "resourceloader.hpp"
#include "renderingsystem/renderingsystem.hpp"
#include "resources/resourcestorage.hpp"
#include "resources/textures.hpp"
#include "resources/fonts.hpp"
#include "resources/walls.hpp"
#include "resources/floors.hpp"
#include "resources/items.hpp"
#include "resources/constructiongfxes.hpp"
#include "items/itemtype.hpp"

void ResourceLoader::loadTextures(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const std::string&)> loadedCallback) const
{
    for(auto texture : rTextures)
    {
        int32_t addedId = storage.addTexture(texture.first, texture.second.texturePath);
        loadedCallback(addedId, texture.first, texture.second.texturePath);
    }
}

void ResourceLoader::loadFonts(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const std::string&)> loadedCallback) const
{
    for(auto font : rFonts)
    {
        int32_t addedId = storage.addFont(font.first, font.second.fontPath);
        loadedCallback(addedId, font.first, font.second.fontPath);
    }
}

void ResourceLoader::loadWalls(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const WallResource&)> loadedCallback) const
{
    for(auto wallType : rWalls)
    {
        int32_t addedId = storage.addWall(wallType.first, wallType.second);
        loadedCallback(addedId, wallType.first, wallType.second);
    }
}

void ResourceLoader::loadWallOre(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const WallOreResource&)> loadedCallback) const
{
    for(auto wallOre : rWallOre)
    {
        int32_t addedId = storage.addWallOre(wallOre.first, wallOre.second);
        loadedCallback(addedId, wallOre.first, wallOre.second);
    }
}

void ResourceLoader::loadFloors(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const FloorResource&)> loadedCallback) const
{
    for(auto floorType : rFloors)
    {
        int32_t addedId = storage.addFloor(floorType.first, floorType.second);
        loadedCallback(addedId, floorType.first, floorType.second);
    }
}

void ResourceLoader::loadGrounds(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const GroundResource&)> loadedCallback) const
{
    for(auto groundType : rGrounds)
    {
        int32_t addedId = storage.addGround(groundType.first, groundType.second);
        loadedCallback(addedId, groundType.first, groundType.second);
    }
}

void ResourceLoader::loadItems(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const ItemResource&)> loadedCallback) const
{
    for(auto itemType : rItems)
    {
        int32_t addedId = storage.addItem(itemType.first, itemType.second);
        loadedCallback(addedId, itemType.first, itemType.second);
    }
}

void ResourceLoader::loadAnimatedSprites(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const EntityGfxResource&)> loadedCallback) const
{
    for(auto animatedSprites : rEntityGfxes)
    {
        int32_t addedId = storage.addAnimatedSprite(animatedSprites.first, animatedSprites.second);
        loadedCallback(addedId, animatedSprites.first, animatedSprites.second);
    }
}

void ResourceLoader::loadConstructionSprites(ResourceStorage& storage, std::function<void(int32_t, const std::string&, const ConstructionGfxResource&)> loadedCallback) const
{
    for(auto constructionSprites : rConstructionGfxes)
    {
        int32_t addedId = storage.addConstructionSprite(constructionSprites.first, constructionSprites.second);
        loadedCallback(addedId, constructionSprites.first, constructionSprites.second);
    }
}
