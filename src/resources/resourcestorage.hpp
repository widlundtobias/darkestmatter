#pragma once

#include "resources/fonts.hpp"
#include "resources/walls.hpp"
#include "resources/floors.hpp"
#include "resources/grounds.hpp"
#include "resources/items.hpp"
#include "resources/entitygfxes.hpp"
#include "resources/constructiongfxes.hpp"

class ResourceStorage
{
    using IdMap = std::unordered_map<std::string, int32_t>;
    using TextureMap = std::unordered_map<int32_t, std::string>;
    using FontMap = std::unordered_map<int32_t, std::string>;
    using WallOreMap = std::unordered_map<int32_t, WallOreResource>;
    using WallMap = std::unordered_map<int32_t, WallResource>;
    using FloorMap = std::unordered_map<int32_t, FloorResource>;
    using GroundMap = std::unordered_map<int32_t, GroundResource>;
    using ItemMap = std::unordered_map<int32_t, ItemResource>;
    using AnimatedSpriteMap = std::unordered_map<int32_t, EntityGfxResource>;
    using ConstructionSpriteMap = std::unordered_map<int32_t, ConstructionGfxResource>;

    public:
        //textures
        int32_t addTexture(std::string name, std::string texturePath);
        const IdMap& textureIds() const;
        const TextureMap& textures() const;
        //fonts
        int32_t addFont(std::string name, std::string fontPath);
        const IdMap& fontIds() const;
        const FontMap& fonts() const;
        //walls
        int32_t addWall(std::string name, WallResource wall);
        const IdMap& wallIds() const;
        const WallMap& walls() const;
        //wall ores
        int32_t addWallOre(std::string name, WallOreResource wallOre);
        const IdMap& wallOreIds() const;
        const WallOreMap& wallOre() const;
        //ground
        int32_t addGround(std::string name, GroundResource ground);
        const IdMap& groundIds() const;
        const GroundMap& grounds() const;
        //floors
        int32_t addFloor(std::string name, FloorResource floor);
        const IdMap& floorIds() const;
        const FloorMap& floors() const;
        //items
        int32_t addItem(std::string name, ItemResource item);
        const IdMap& itemIds() const;
        const ItemMap& items() const;
        //animated sprites
        int32_t addAnimatedSprite(std::string name, EntityGfxResource animatedSprite);
        const IdMap& animatedSpriteIds() const;
        const AnimatedSpriteMap& animatedSprites() const;
        //construction sprites
        int32_t addConstructionSprite(std::string name, ConstructionGfxResource constructionSprite);
        const IdMap& constructionSpriteIds() const;
        const ConstructionSpriteMap& ConstructionSprites() const;
    private:
        //textures
        IdMap mTextureIds;
        TextureMap mTextures;
        //fonts
        IdMap mFontIds;
        FontMap mFonts;
        //walls
        IdMap mWallIds;
        WallMap mWalls;
        //wall ors
        IdMap mWallOreIds;
        WallOreMap mWallOre;
        //ground
        IdMap mGroundIds;
        GroundMap mGrounds;
        //floors
        IdMap mFloorIds;
        FloorMap mFloors;
        //items
        IdMap mItemIds;
        ItemMap mItems;
        //animated sprites
        IdMap mAnimatedSpriteIds;
        AnimatedSpriteMap mAnimatedSprites;
        //construction sprites
        IdMap mConstructionSpriteIds;
        ConstructionSpriteMap mConstructionSprites;
};
