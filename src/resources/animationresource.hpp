#pragma once
#include <glm/glm.hpp>

struct AnimationResource
{
    glm::ivec2 start;
    glm::ivec2 size;
    int32_t frameCount;
    int32_t delay;
    bool loop;
};
