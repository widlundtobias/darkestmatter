#include "gamesession.hpp"
#include "resources/resourceloader.hpp"
#include "util/texturemaker.hpp"
#include "generation/landscapegenerator.hpp"
#include "items/itemtype.hpp"
#include "world/tiles.hpp"
#include "renderingsystem/renderingsystem.hpp"
#include "world/gameworld.hpp"
#include "gamecontroller.hpp"
#include "standardgameui.hpp"
#include "facility/facility.hpp"
#include "world/itemmanager.hpp"

const glm::ivec2 viewSize = {1024, 768};

GameSession::GameSession(fea::MessageBus& bus, fea::InputHandler& inputHandler):
    mBus(bus),
    mInputHandler(bus, inputHandler),
    mRenderingSystem(fea::Viewport(viewSize, {0, 0}, fea::Camera(viewSize / 2)), bus, Tiles::size()),
    mPaused(false)
{

    subscribe(mBus, *this);

    ResourceLoader loader;

    LOG_I("loading textures");
    loader.loadTextures(mResources, [this] (int32_t id, const std::string& name, const std::string& texturePath)
    {
        LOG_V("loading '" << name << "' as texture id '" << id << "'");
        mRenderingSystem->addTexture(id, makeTexture(texturePath), name);
    });

    LOG_I("loading fonts");
    loader.loadFonts(mResources, [this] (int32_t id, const std::string& name, const std::string& fontPath)
    {
        LOG_V("loading '" << name << "' as font id '" << id << "'");
        mRenderingSystem->addFont(id, gim::Font(std::ifstream(fontPath, std::ios::binary)), name);
    });

    LOG_I("loading construction sprites");
    loader.loadConstructionSprites(mResources, [this] (int32_t id, const std::string& name, const ConstructionGfxResource& constructionSprite)
    {
        LOG_V("loading '" << name << "' as construction sprite id '" << id << "'");
        FEA_ASSERT(mResources.textureIds().count(constructionSprite.texture) != 0, "Construction sprite '" << name << "' has texture '" << constructionSprite.texture << "' which does not exist");

        mRenderingSystem->addConstructionSpriteType(id,
        {
            mResources.textureIds().at(constructionSprite.texture),
            constructionSprite.start,
            constructionSprite.size,
        });
    });

    LOG_I("loading walls");
    loader.loadWalls(mResources, [this] (int32_t id, const std::string& name, const WallResource& wall)
    {
        LOG_V("loading '" << name << "' as wall id '" << id << "'");

        FEA_ASSERT(mResources.textureIds().count(wall.texture) != 0, "Wall '" << name << "' has texture '" << wall.texture << "' which does not exist");
        mRenderingSystem->addWallType(id, mResources.textureIds().at(wall.texture), wall.textureCoordinates);

        FEA_ASSERT(mResources.constructionSpriteIds().count(wall.constructionGraphics) != 0, "Wall '" << name << "' has construction graphics '" << wall.constructionGraphics << "' which does not exist");
        int32_t constructionGraphicsId = mResources.constructionSpriteIds().at(wall.constructionGraphics);

        mRenderingSystem->addConstructionType({ConstructionCategory::WALL, id}, constructionGraphicsId);
    });

    LOG_I("loading wall ores");
    loader.loadWallOre(mResources, [this] (int32_t id, const std::string& name, const WallOreResource& wallOre)
    {
        LOG_V("loading '" << name << "' as wallOre id '" << id << "'");
        FEA_ASSERT(mResources.textureIds().count(wallOre.texture) != 0, "wallOre '" << name << "' has texture '" << wallOre.texture << "' which does not exist");

        mRenderingSystem->addWallOreType(id, {wallOre.lowAmount, wallOre.midAmount, wallOre.higAmount}, mResources.textureIds().at(wallOre.texture), wallOre.textureCoordinates);
    });

    LOG_I("loading grounds");
    loader.loadGrounds(mResources, [this] (int32_t id, const std::string& name, const GroundResource& ground)
    {
        LOG_V("loading '" << name << "' as ground id '" << id << "'");

        mRenderingSystem->addGroundType(id, mResources.textureIds().at(ground.texture), ground.textureCoordinates);
    });

    LOG_I("loading floors");
    loader.loadFloors(mResources, [this] (int32_t id, const std::string& name, const FloorResource& floor)
    {
        LOG_V("loading '" << name << "' as floor id '" << id << "'");
        FEA_ASSERT(mResources.textureIds().count(floor.texture) != 0, "Floor '" << name << "' has texture '" << floor.texture << "' which does not exist");

        FEA_ASSERT(mResources.constructionSpriteIds().count(floor.constructionGraphics) != 0, "Floor '" << name << "' has construction graphics '" << floor.constructionGraphics << "' which does not exist");
        int32_t constructionGraphicsId = mResources.constructionSpriteIds().at(floor.constructionGraphics);

        mRenderingSystem->addFloorType(id, mResources.textureIds().at(floor.texture), floor.textureCoordinates);
        mRenderingSystem->addConstructionType({ConstructionCategory::FLOOR, id}, constructionGraphicsId);
    });

    LOG_I("loading items");
    loader.loadItems(mResources, [this] (int32_t id, const std::string& name, const ItemResource& item)
    {
        LOG_V("loading '" << name << "' as item id '" << id << "'");
        FEA_ASSERT(mResources.textureIds().count(item.gfx.texture) != 0, "Item '" << name << "' has texture '" << item.gfx.texture << "' which does not exist");

        mRenderingSystem->addItemType(id,
        {
            mResources.textureIds().at(item.gfx.texture),
            item.gfx.size,
            item.gfx.subRectStart,
            item.gfx.subRectSize,
            item.gfx.amountThreshold,
        });
    });

//    mGameWorld->items().

    LOG_I("loading animated sprites");
    loader.loadAnimatedSprites(mResources, [this] (int32_t id, const std::string& name, const EntityGfxResource& animatedSprite)
    {
        LOG_V("loading '" << name << "' as animated sprite id '" << id << "'");
        FEA_ASSERT(mResources.textureIds().count(animatedSprite.texture) != 0, "Animated sprite '" << name << "' has texture '" << animatedSprite.texture << "' which does not exist");

        th::Optional<int32_t> defaultAnimation;
        std::unordered_map<int32_t, fea::Animation> animations;

        for(const auto& animation : animatedSprite.animations)
        {
            int32_t animId = static_cast<int32_t>(animations.size());

            if(animation.first == animatedSprite.defaultAnimation)
                defaultAnimation.reset(animId);

            animations.emplace(animId,
            fea::Animation(
                animation.second.start,
                animation.second.size,
                static_cast<uint32_t>(animation.second.frameCount),
                static_cast<uint32_t>(animation.second.delay)
            ));
        }

        FEA_ASSERT(defaultAnimation, "Default animation '" << animatedSprite.defaultAnimation << "' given to animated sprite '" << name << "' does not exist");

        mRenderingSystem->addAnimatedSpriteType(id,
        {
            mResources.textureIds().at(animatedSprite.texture),
            *defaultAnimation,
            (glm::vec2)animatedSprite.size,
            (glm::vec2)animatedSprite.offset,
            std::move(animations)
        });
    });

    fea::JsonEntityLoader entityLoader;
    LOG_I("loading entity attributes");
    auto entityAttributes = entityLoader.loadEntityAttributes("data/entities/attributes.json");

    for(const auto& attribute: entityAttributes)
    {
        LOG_V("loading attribute '" << attribute.first << "' as '" << attribute.second << "'");
    }

    LOG_I("loading entity templates");
    auto entityTemplates = entityLoader.loadEntityTemplates("data/entities/templates.json");

    for(auto& entityTemplate : entityTemplates)
    {
        LOG_V("loading template '" << entityTemplate.first << "'");

        th::Optional<int32_t> toInsert;
        for(auto& entityAttribute : entityTemplate.second.mAttributes)
        {
            if(entityAttribute.first == "sprite")
            {
                FEA_ASSERT(mResources.animatedSpriteIds().count(entityAttribute.second),"Entity '" << entityTemplate.first << "' has sprite '" << entityAttribute.second << "' which does not exist");

                toInsert.reset(mResources.animatedSpriteIds().at(entityAttribute.second));
            }
        }

        if(toInsert)
            entityTemplate.second.mAttributes.emplace("sprite_id", std::to_string(*toInsert));
    }

    //gameworld
    LOG_I("setting up gameworld");

    //item types
    LOG_I("preparing item types");
    std::unordered_map<int32_t, ItemTypeData> itemTypeData;

    for(const auto& item : mResources.items())
    {
        //default data
        AnyMap<std::string> defaultData;

        for(auto defaultValue : item.second.defaultData)
        {
            ItemDataType type = defaultValue.second.first;

            if(type == INT)
                defaultData.emplace(defaultValue.first, std::stoi(defaultValue.second.second));
            else if(type == STRING)
                defaultData.emplace(defaultValue.first, defaultValue.second.second);
            else if(type == FLOAT)
                defaultData.emplace(defaultValue.first, std::stof(defaultValue.second.second));
            else
            {
                FEA_ASSERT(false, "Invalid data type " << type);
            }
        }

        //item type
        ItemType itemType
        {
            item.first,
            item.second.weight,
            item.second.size,
            item.second.maxStack,
            item.second.big,
            {}, //properties are filled out below
            {}, //invoke handlers are filled out in item manager
            std::move(defaultData), //default values are
        };

        for(const std::string& property : item.second.properties)
        {
            itemType.properties.emplace(itemPropertyFromString(property));
        }

        std::unordered_map<ItemAction, std::string> invokeHandlers;

        //invoke handlers
        for(auto invokeHandler : item.second.invokeHandlers)
        {
            invokeHandlers.emplace(invokeHandler.first, invokeHandler.second);
        }

        itemTypeData.emplace(item.first,
        ItemTypeData{
            std::move(itemType),
            std::move(invokeHandlers)
        });
    }

    //construction types
    LOG_I("preparing construction types");
    std::unordered_map<int32_t, ConstructionTypeData> wallConstructions;
    for(const auto& wall : mResources.walls())
    {
        ConstructionTypeData wallType
        {
            wall.second.constructionTime,
            {1, 1},
            {} //materials needed filled in below
        };

        for(const WallResource::MaterialRequirement& neededMaterial : wall.second.materialsNeeded)
        {
            FEA_ASSERT(mResources.itemIds().count(neededMaterial.materialName) != 0, "Wall '" << wall.second.name << "' requires material '" << neededMaterial.materialName << "' but such a material does not exist");
            wallType.materialsNeeded.emplace(mResources.itemIds().at(neededMaterial.materialName), neededMaterial.amount);
        }

        wallConstructions.emplace(wall.first, std::move(wallType));
    }

    std::unordered_map<int32_t, ConstructionTypeData> floorConstructions;
    for(const auto& floor : mResources.floors())
    {
        ConstructionTypeData floorType
        {
            floor.second.constructionTime,
            {1, 1},
            {} //materials needed filled in below
        };

        for(const FloorResource::MaterialRequirement& neededMaterial : floor.second.materialsNeeded)
        {
            FEA_ASSERT(mResources.itemIds().count(neededMaterial.materialName) != 0, "Floor '" << floor.second.name << "' requires material '" << neededMaterial.materialName << "' but such a material does not exist");
            floorType.materialsNeeded.emplace(mResources.itemIds().at(neededMaterial.materialName), neededMaterial.amount);
        }

        floorConstructions.emplace(floor.first, std::move(floorType));
    }

    //landscape
    LOG_I("generating map");
    int32_t ore = mResources.wallOreIds().at("gold_ore");
    LandscapeGenerator generator({mResources.wallIds().at("rock_wall"), ore, mResources.groundIds().at("dry_rock"), {mResources.wallOre().at(ore).lowAmount, mResources.wallOre().at(ore).midAmount, mResources.wallOre().at(ore).higAmount}});
    Landscape landscape = generator.generate({100, 100});
    LOG_I("completed");

    //some hardcoded room type D:
    RoomType storageRoom
    {
        0,
        {},
        {},
        {//default room properties
            {"required_items", std::unordered_set<ItemProperty>{ItemProperty::CONTAINER}},
            {"require_floor", true},
            {"require_wall", true}
        }
    };


    //creating the world
    LOG_I("putting world together");
    mGameWorld = std::make_unique<GameWorld>(mBus, std::move(landscape), GameWorld::Definitions{entityAttributes, entityTemplates, wallConstructions, floorConstructions}, std::move(itemTypeData), std::unordered_map<int32_t, RoomTypeData>{{0, {storageRoom, {}}}});

    //setting up facility controller
    LOG_I("preparing facility");
    mGameController = std::make_unique<GameController>(mBus, mGameWorld->facility(), *mGameWorld, mResources);
    mGameUI = std::make_unique<StandardGameUI>(*mGameController, mBus, mResources, viewSize, [this] (const glm::vec2& screenCoords)
    {
        return mRenderingSystem->renderer().getViewport().untransformPoint(screenCoords);
    },
    [this] (const glm::vec2& worldCoords)
    {
        return mRenderingSystem->renderer().getViewport().transformPoint(worldCoords);
    });

    mGameWorld->facility().setFacilityController(*mGameController);

    //creating initial entities
    LOG_I("creating entities");
    for(int32_t i = 0; i < 5; i++)
    {
        auto worker = mGameWorld->createEntity("worker");

        glm::vec2 position = glm::vec2(400.0f + static_cast<float>(rand() % 20) * 4.0f, 400.0f + static_cast<float>(rand() % 20) * 10.0f);
        worker->setAttribute("position", position);
        mBus.send(AnimatedSpriteMovedMessage{worker->getId(), position});
    }

    auto rocket = mGameWorld->createEntity("landing_rocket");

    rocket->setAttribute("position", glm::vec2(400.0f, 400.0f));
    mBus.send(AnimatedSpriteMovedMessage{rocket->getId(), glm::vec2(400.0f, 400.0f)});

    mBus.send(OverlayCreatedMessage{0, {}});

    //creating initial material
    LOG_I("creating material");
    for(int32_t i = 0; i < 5000; i++)
    {
        auto item = mGameWorld->items().createItem(i % 3);

        glm::vec2 position = glm::vec2(360.0f + static_cast<float>(rand() % 35) * 4.0f, 200.0f + static_cast<float>(rand() % 60) * 10.0f);
        mGameWorld->items().putItem(item, HalfTiles::toTile(position));
    }

    LOG_I("game session created");
}

GameSession::~GameSession()
{
}

void GameSession::update()
{
    mInputHandler.process();

    if(!mPaused)
        mGameWorld->update(1.0f);

    mGameUI->update();
    mRenderingSystem->render();
}

void GameSession::handleMessage(const TogglePauseMessage& message)
{
    mPaused = !mPaused;

    mRenderingSystem->disableTick(mPaused);
}

void GameSession::handleMessage(const ResizeMessage& message)
{
    mGameUI->resize(message.size);
}
