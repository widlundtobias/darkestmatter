#include "floortilemap.hpp"
#include "util/selector.hpp"

const int32_t tileTypeTextureWidth = 2;
const int32_t tileTypeTextureHeight = 2;

FloorTileMap::FloorTileMap(const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize):
    mTiles(tileSize / 2, textureUnitSize, chunkSize)
{
}

FloorTileMap::FloorTileMap(const glm::ivec2& mapSize, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize):
    mTiles(tileSize / 2, textureUnitSize, chunkSize),
    mFloorTypes(mapSize, 0)
{
}

void FloorTileMap::reset(const Grid<FloorType>& floors)
{
    mTiles.clear();
    mFloorTypes = floors;

    for(int32_t y = 0; y < floors.size().y; y++)
    {
        for(int32_t x = 0; x < floors.size().x; x++)
        {
            auto type = floors.at({x, y});
            if(type != 0)
                set({x, y}, type);
        }
    }
}

void FloorTileMap::setTexture(const fea::Texture& texture)
{
    mTiles.setTexture(texture);
}

void FloorTileMap::defineType(FloorType type, const glm::ivec2& textureCoordinates)
{
    glm::ivec2 baseCoordinates = textureCoordinates * glm::ivec2(tileTypeTextureWidth, tileTypeTextureHeight);

    addTileDefinition(type, VARIATION1, baseCoordinates + glm::ivec2(0, 0));
    addTileDefinition(type, VARIATION2, baseCoordinates + glm::ivec2(1, 0));
    addTileDefinition(type, VARIATION3, baseCoordinates + glm::ivec2(0, 1));
    addTileDefinition(type, VARIATION4, baseCoordinates + glm::ivec2(1, 1));

    mAddedTypes.emplace(type);
}

void FloorTileMap::set(const glm::ivec2& coordinate, FloorType type)
{
    FEA_ASSERT(mAddedTypes.count(type) != 0, "Cannot set floor tile of non-existent type " + std::to_string(type));
    mFloorTypes.set(coordinate, type);

    updateTiles(coordinate);
}

void FloorTileMap::unset(const glm::ivec2& coordinate)
{
    mFloorTypes.set(coordinate, 0);

    updateTiles(coordinate);
}

const std::vector<fea::RenderEntity>& FloorTileMap::getRenderInfo() const
{
    return mTiles.getRenderInfo();
}

void FloorTileMap::setCullRegion(const glm::vec2& start, const glm::vec2& end)
{
    mTiles.setCullRegion(start, end);
}

void FloorTileMap::setCullEnabled(bool enabled)
{
    mTiles.setCullEnabled(enabled);
}

TypeVariationHash FloorTileMap::typeVariationHasher(FloorType type, FloorVariation floorVariation) const
{
    return ((int32_t)type << 16) | (int32_t)floorVariation;
}

uint64_t FloorTileMap::coordinateHasher(const glm::ivec2& coordinate) const
{
    return static_cast<uint64_t>(coordinate.x << 16 | coordinate.y);
}

void FloorTileMap::addTileDefinition(FloorType type, FloorVariation direction, const glm::ivec2& coordinate)
{
    mTypeDefinitions[typeVariationHasher(type, direction)] = coordinate;
    mTiles.addTileDefinition(typeVariationHasher(type, direction), fea::TileDefinition(coordinate));
}

FloorType FloorTileMap::typeAt(const glm::ivec2& coordinate) const
{
    return mFloorTypes.at(coordinate);
}

FloorType FloorTileMap::typeAtGfxTile(const glm::ivec2& coordinate) const
{
    return mFloorTypes.at(coordinate / 2);
}

void FloorTileMap::updateTiles(const glm::ivec2& coordinate)
{
    if(isWithin(coordinate))
    {
        glm::ivec2 baseCoordinate = coordinate * 2;
        std::deque<glm::ivec2> tilesToUpdate =
        {
            baseCoordinate,
            baseCoordinate + glm::ivec2(1, 0),
            baseCoordinate + glm::ivec2(0, 1),
            baseCoordinate + glm::ivec2(1, 1)
        };

        for(const auto& tileCoordinate : tilesToUpdate)
        {
            FloorType type  = typeAtGfxTile(tileCoordinate);

            if(type != 0)
            {
                FloorVariation variation = Selector<FloorVariation>(
                        {
                        {VARIATION1, 0.9f},
                        {VARIATION2, 0.05f},
                        {VARIATION3, 0.01f},
                        {VARIATION4, 0.002f},
                        }, coordinateHasher(tileCoordinate)).get();
                mTiles.setTile(tileCoordinate, typeVariationHasher(type, variation));
            }
            else
                mTiles.unsetTile(tileCoordinate);
        }
    }
}

bool FloorTileMap::isWithin(const glm::ivec2& coordinate) const
{
    return coordinate.x >= 0 && coordinate.y >= 0 && coordinate.x < (int32_t)mFloorTypes.size().x && coordinate.y < (int32_t)mFloorTypes.size().y;
}
