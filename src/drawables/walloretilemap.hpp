#pragma once
#include "../util/grid.hpp"
#include "../world/wallore.hpp"

enum WallOreVariation {
    VARIATION1_ORE_LOW,
    VARIATION1_ORE_MID,
    VARIATION1_ORE_HIG,
    VARIATION2_ORE_LOW,
    VARIATION2_ORE_MID,
    VARIATION2_ORE_HIG,
};

using TypeVariationHash = int32_t;

class WallOreTileMap
{
    public:

        struct OreAmountThreshold
        {
            int32_t low;
            int32_t mid;
            int32_t hig;
        };

        WallOreTileMap(const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize = 32);
        WallOreTileMap(const glm::ivec2& mapSize, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize = 32);
        void reset(const Grid<WallOre>& floors);
        void setTexture(const fea::Texture& texture);
        void defineType(WallOreType type, OreAmountThreshold thresholds, const glm::ivec2& textureCoordinates);
        void set(const glm::ivec2& coordinate, WallOre type);
        void unset(const glm::ivec2& coordinate);
        const std::vector<fea::RenderEntity>& getRenderInfo() const;
        void setCullRegion(const glm::vec2& start, const glm::vec2& end);
        void setCullEnabled(bool enabled);
    private:
        TypeVariationHash typeVariationHasher(WallOreType type, WallOreVariation wallOreVariation) const;
        uint64_t coordinateHasher(const glm::ivec2& coordinate) const;
        void addTileDefinition(WallOreType type, WallOreVariation variation, const glm::ivec2& coordinate);
        WallOre typeAt(const glm::ivec2& coordinate) const;
        WallOre typeAtGfxTile(const glm::ivec2& coordinate) const;
        void updateTiles(const glm::ivec2& coordinate);
        bool isWithin(const glm::ivec2& coordinate) const;
        fea::TileMap mTiles;
        Grid<WallOre> mWallOreType;
        std::unordered_set<WallOreType> mAddedTypes;
        std::unordered_map<WallOreType, OreAmountThreshold> mThresholds;
        std::unordered_map<TypeVariationHash, glm::ivec2> mTypeDefinitions;
};
