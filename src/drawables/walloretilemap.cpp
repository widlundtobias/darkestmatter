#include "walloretilemap.hpp"
#include "util/selector.hpp"

const int32_t tileTypeTextureWidth = 2;
const int32_t tileTypeTextureHeight = 2;

WallOreTileMap::WallOreTileMap(const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize):
    mTiles(tileSize / 2, textureUnitSize, chunkSize)
{
}

WallOreTileMap::WallOreTileMap(const glm::ivec2& mapSize, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize):
    mTiles(tileSize / 2, textureUnitSize, chunkSize),
    mWallOreType(mapSize, {})
{
}

void WallOreTileMap::reset(const Grid<WallOre>& floors)
{
    mTiles.clear();
    mWallOreType = floors;

    for(int32_t y = 0; y < floors.size().y; y++)
    {
        for(int32_t x = 0; x < floors.size().x; x++)
        {
            auto type = floors.at({x, y}).type;
            auto amount = floors.at({x, y}).amount;
            if(type != 0)
                set({x, y}, {type, amount});
        }
    }
}

void WallOreTileMap::setTexture(const fea::Texture& texture)
{
    mTiles.setTexture(texture);
}

void WallOreTileMap::defineType(WallOreType type, OreAmountThreshold thresholds, const glm::ivec2& textureCoordinates)
{
    glm::ivec2 baseCoordinates = textureCoordinates * glm::ivec2(tileTypeTextureWidth, tileTypeTextureHeight);

    addTileDefinition(type, VARIATION1_ORE_HIG, baseCoordinates + glm::ivec2(0, 0));
    addTileDefinition(type, VARIATION2_ORE_HIG, baseCoordinates + glm::ivec2(1, 0));
    addTileDefinition(type, VARIATION1_ORE_MID, baseCoordinates + glm::ivec2(0, 1));
    addTileDefinition(type, VARIATION2_ORE_MID, baseCoordinates + glm::ivec2(1, 1));
    addTileDefinition(type, VARIATION1_ORE_LOW, baseCoordinates + glm::ivec2(0, 2));
    addTileDefinition(type, VARIATION2_ORE_LOW, baseCoordinates + glm::ivec2(1, 2));

    mThresholds[type] = thresholds;

    mAddedTypes.emplace(type);
}

void WallOreTileMap::set(const glm::ivec2& coordinate, WallOre type)
{
    FEA_ASSERT(mAddedTypes.count(type.type) != 0, "Cannot set wall ore tile of non-existent type " + std::to_string(type.type));
    mWallOreType.set(coordinate, type);

    updateTiles(coordinate);
}

void WallOreTileMap::unset(const glm::ivec2& coordinate)
{
    mWallOreType.set(coordinate, {});

    updateTiles(coordinate);
}

const std::vector<fea::RenderEntity>& WallOreTileMap::getRenderInfo() const
{
    return mTiles.getRenderInfo();
}

void WallOreTileMap::setCullRegion(const glm::vec2& start, const glm::vec2& end)
{
    mTiles.setCullRegion(start, end);
}

void WallOreTileMap::setCullEnabled(bool enabled)
{
    mTiles.setCullEnabled(enabled);
}

TypeVariationHash WallOreTileMap::typeVariationHasher(WallOreType type, WallOreVariation wallOreVariation) const
{
    return ((int32_t)type << 16) | (int32_t)wallOreVariation;
}

uint64_t WallOreTileMap::coordinateHasher(const glm::ivec2& coordinate) const
{
    return static_cast<uint64_t>(coordinate.x << 16 | coordinate.y);
}

void WallOreTileMap::addTileDefinition(WallOreType type, WallOreVariation direction, const glm::ivec2& coordinate)
{
    mTypeDefinitions[typeVariationHasher(type, direction)] = coordinate;
    mTiles.addTileDefinition(typeVariationHasher(type, direction), fea::TileDefinition(coordinate));
}

WallOre WallOreTileMap::typeAt(const glm::ivec2& coordinate) const
{
    return mWallOreType.at(coordinate);
}

WallOre WallOreTileMap::typeAtGfxTile(const glm::ivec2& coordinate) const
{
    return mWallOreType.at(coordinate / 2);
}

void WallOreTileMap::updateTiles(const glm::ivec2& coordinate)
{
    if(isWithin(coordinate))
    {
        glm::ivec2 baseCoordinate = coordinate * 2;
        std::deque<glm::ivec2> tilesToUpdate =
        {
            baseCoordinate,
            baseCoordinate + glm::ivec2(1, 0),
            baseCoordinate + glm::ivec2(0, 1),
            baseCoordinate + glm::ivec2(1, 1)
        };

        for(const auto& tileCoordinate : tilesToUpdate)
        {
            WallOre wallOre  = typeAtGfxTile(tileCoordinate);

            if(wallOre.type != 0)
            {
                WallOreVariation v1;
                WallOreVariation v2;

                if(wallOre.amount <= mThresholds[wallOre.type].low)
                {
                    v1 = VARIATION1_ORE_LOW;
                    v2 = VARIATION2_ORE_LOW;
                }
                else if(wallOre.amount <= mThresholds[wallOre.type].mid)
                {
                    v1 = VARIATION1_ORE_MID;
                    v2 = VARIATION2_ORE_MID;
                }
                else
                {
                    v1 = VARIATION1_ORE_HIG;
                    v2 = VARIATION2_ORE_HIG;
                }

                WallOreVariation variation = Selector<WallOreVariation>(
                        {
                            {v1, 0.6f},
                            {v2, 0.4f},
                        }, coordinateHasher(tileCoordinate)).get();
                mTiles.setTile(tileCoordinate, typeVariationHasher(wallOre.type, variation));
            }
            else
                mTiles.unsetTile(tileCoordinate);
        }
    }
}

bool WallOreTileMap::isWithin(const glm::ivec2& coordinate) const
{
    return coordinate.x >= 0 && coordinate.y >= 0 && coordinate.x < (int32_t)mWallOreType.size().x && coordinate.y < (int32_t)mWallOreType.size().y;
}
