#pragma once
#include "gimgui/feacoloradaptor.hpp"
#include "gimgui/glmrectangleadaptor.hpp"
#include "gimgui/glmvec2adaptor.hpp"
#include "gimgui/featextureadaptor.hpp"

class GuiElement
{
    public:
        GuiElement();
        GuiElement(const gim::Element& element);
        void addFont(gim::Font& font);
        std::vector<fea::RenderEntity> getRenderInfo() const;
        void registerTexture(const std::string name, fea::Texture& texture);
        void setElement(const gim::Element& element);
    private:
        std::vector<float> discardZ(const std::vector<float>& positions) const;
        const gim::Element* mElement;
        std::unordered_map<uint32_t, std::unique_ptr<fea::Texture>> mFontTextures;
        std::vector<std::reference_wrapper<gim::Font>> mFonts;
        mutable gim::RenderDataGenerator<Vec2Adaptor, RectangleAdaptor, ColorAdaptor, FeaTextureAdaptor> mRenderDataGenerator;
};
