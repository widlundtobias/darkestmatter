#include "walltilemap.hpp"
#include "util/selector.hpp"

const int32_t tileTypeTextureWidth = 5 * 2;
const int32_t tileTypeTextureHeight = 4 * 2;

WallTileMap::WallTileMap(const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize):
    mTiles(tileSize / 2, textureUnitSize, chunkSize)
{
}

WallTileMap::WallTileMap(const glm::ivec2& mapSize, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize, int32_t chunkSize):
    mTiles(tileSize / 2, textureUnitSize, chunkSize),
    mWallTypes(mapSize, 0)
{
}

void WallTileMap::reset(const Grid<WallType>& walls)
{
    mTiles.clear();
    mWallTypes = walls;

    for(int32_t y = 0; y < walls.size().y; y++)
    {
        for(int32_t x = 0; x < walls.size().x; x++)
        {
            auto type = walls.at({x, y});
            if(type != 0)
                set({x, y}, type);
        }
    }
}

void WallTileMap::setTexture(const fea::Texture& texture)
{
    mTiles.setTexture(texture);
}

void WallTileMap::defineType(WallType type, const glm::ivec2& textureCoordinates)
{
    glm::ivec2 baseCoordinates = textureCoordinates * glm::ivec2(tileTypeTextureWidth, tileTypeTextureHeight);

    addTileDefinition(type, ES,   baseCoordinates + glm::ivec2(0, 0));
    addTileDefinition(type, ESW1, baseCoordinates + glm::ivec2(1, 0));
    addTileDefinition(type, ESW2, baseCoordinates + glm::ivec2(2, 0));
    addTileDefinition(type, ESW3, baseCoordinates + glm::ivec2(3, 0));
    addTileDefinition(type, SW,   baseCoordinates + glm::ivec2(4, 0));

    addTileDefinition(type, NES1, baseCoordinates + glm::ivec2(0, 1));
    addTileDefinition(type, NESW1,baseCoordinates + glm::ivec2(1, 1));
    addTileDefinition(type, ESC,  baseCoordinates + glm::ivec2(2, 1));
    addTileDefinition(type, SWC,  baseCoordinates + glm::ivec2(3, 1));
    addTileDefinition(type, NSW1, baseCoordinates + glm::ivec2(4, 1));

    addTileDefinition(type, NES2, baseCoordinates + glm::ivec2(0, 2));
    addTileDefinition(type, NESW2,baseCoordinates + glm::ivec2(1, 2));
    addTileDefinition(type, NEC,  baseCoordinates + glm::ivec2(2, 2));
    addTileDefinition(type, NWC,  baseCoordinates + glm::ivec2(3, 2));
    addTileDefinition(type, NSW2, baseCoordinates + glm::ivec2(4, 2));

    addTileDefinition(type, NE,   baseCoordinates + glm::ivec2(0, 3));
    addTileDefinition(type, NEW1, baseCoordinates + glm::ivec2(1, 3));
    addTileDefinition(type, NEW2, baseCoordinates + glm::ivec2(2, 3));
    addTileDefinition(type, NEW3, baseCoordinates + glm::ivec2(3, 3));
    addTileDefinition(type, NW,   baseCoordinates + glm::ivec2(4, 3));

    mAddedTypes.emplace(type);
}

void WallTileMap::set(const glm::ivec2& coordinate, WallType type)
{
    FEA_ASSERT(mAddedTypes.count(type) != 0, "Cannot set wall tile of non-existent type " + std::to_string(type));
    mWallTypes.set(coordinate, type);

    updateDirection(coordinate);
    updateDirection(coordinate + glm::ivec2(0, -1));
    updateDirection(coordinate + glm::ivec2(1, 0));
    updateDirection(coordinate + glm::ivec2(0, 1));
    updateDirection(coordinate + glm::ivec2(-1, 0));
    updateDirection(coordinate + glm::ivec2(-1, -1));
    updateDirection(coordinate + glm::ivec2(1, -1));
    updateDirection(coordinate + glm::ivec2(-1, 1));
    updateDirection(coordinate + glm::ivec2(1, 1));
}

void WallTileMap::unset(const glm::ivec2& coordinate)
{
    mWallTypes.set(coordinate, 0);

    updateDirection(coordinate);
    updateDirection(coordinate + glm::ivec2(0, -1));
    updateDirection(coordinate + glm::ivec2(1, 0));
    updateDirection(coordinate + glm::ivec2(0, 1));
    updateDirection(coordinate + glm::ivec2(-1, 0));
    updateDirection(coordinate + glm::ivec2(-1, -1));
    updateDirection(coordinate + glm::ivec2(1, -1));
    updateDirection(coordinate + glm::ivec2(-1, 1));
    updateDirection(coordinate + glm::ivec2(1, 1));
}

const std::vector<fea::RenderEntity>& WallTileMap::getRenderInfo() const
{
    return mTiles.getRenderInfo();
}

void WallTileMap::setCullRegion(const glm::vec2& start, const glm::vec2& end)
{
    mTiles.setCullRegion(start, end);
}

void WallTileMap::setCullEnabled(bool enabled)
{
    mTiles.setCullEnabled(enabled);
}

TypeDirectionHash WallTileMap::typeDirectionHasher(WallType type, WallDirection wallDirection) const
{
    return ((int32_t)type << 16) | (int32_t)wallDirection;
}

uint64_t WallTileMap::coordinateHasher(const glm::ivec2& coordinate) const
{
    return static_cast<uint64_t>(coordinate.x << 16 | coordinate.y);
}

void WallTileMap::addTileDefinition(WallType type, WallDirection direction, const glm::ivec2& coordinate)
{
    mTypeDefinitions[typeDirectionHasher(type, direction)] = coordinate;
    mTiles.addTileDefinition(typeDirectionHasher(type, direction), fea::TileDefinition(coordinate));
}

WallType WallTileMap::typeAt(const glm::ivec2& coordinate) const
{
    return mWallTypes.at(coordinate);
}

WallType WallTileMap::typeAtGfxTile(const glm::ivec2& coordinate) const
{
    if(isWithin(coordinate / 2))
        return mWallTypes.at(coordinate / 2);
    else
        return -1;
}

void WallTileMap::updateDirection(const glm::ivec2& coordinate)
{
    if(isWithin(coordinate))
    {
        glm::ivec2 baseCoordinate = coordinate * 2;
        std::deque<glm::ivec2> tilesToUpdate =
        {
            baseCoordinate,
            baseCoordinate + glm::ivec2(1, 0),
            baseCoordinate + glm::ivec2(0, 1),
            baseCoordinate + glm::ivec2(1, 1)
        };

        for(const auto& tileCoordinate : tilesToUpdate)
        {
            WallType type  = typeAtGfxTile(tileCoordinate);

            if(type != 0)
            {
                bool nESame = typeAtGfxTile(tileCoordinate + glm::ivec2(1, -1))  == type;
                bool eSSame = typeAtGfxTile(tileCoordinate + glm::ivec2(1, 1))   == type;
                bool sWSame = typeAtGfxTile(tileCoordinate + glm::ivec2(-1, 1))  == type;
                bool wNSame = typeAtGfxTile(tileCoordinate + glm::ivec2(-1, -1)) == type;

                int32_t cornerAmount = nESame + eSSame + sWSame + wNSame;

                WallDirection direction = ES;

                if(cornerAmount == 1)
                {
                    if(nESame)
                        direction = NE;
                    else if(eSSame)
                        direction = ES;
                    else if(sWSame)
                        direction = SW;
                    else if(wNSame)
                        direction = NW;
                }
                else if(cornerAmount == 2)
                {
                    if(nESame)
                    {
                        if(eSSame)
                            direction = Selector<WallDirection>({{NES1, 0.8}, {NES2, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        else if(wNSame)
                            direction = Selector<WallDirection>({{NEW1, 0.8f}, {NEW2, 0.1f}, {NEW3, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        else
                        {
                            bool eSame = typeAtGfxTile(tileCoordinate + glm::ivec2(1, 0)) == type;

                            if(eSame)
                                direction = NE;
                            else
                                direction = SW;
                        }
                    }
                    else
                    {
                        if(eSSame && wNSame)
                        {
                            bool eSame = typeAtGfxTile(tileCoordinate + glm::ivec2(1, 0)) == type;

                            if(eSame)
                                direction = ES;
                            else
                                direction = NW;
                        }
                        else if(eSSame)
                            direction = Selector<WallDirection>({{ESW1, 0.8f}, {ESW2, 0.1f}, {ESW3, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        else if(wNSame)
                            direction = Selector<WallDirection>({{NSW1, 0.8}, {NSW2, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                    }
                }
                else if(cornerAmount == 3)
                {
                    if(!nESame)
                    {
                        bool nSame = typeAtGfxTile(tileCoordinate + glm::ivec2(0, -1)) == type;
                        bool eSame = typeAtGfxTile(tileCoordinate + glm::ivec2(1, 0)) == type;

                        if(nSame && eSame)
                            direction = NEC;
                        else
                        {
                            if(eSame)
                                direction = Selector<WallDirection>({{ESW1, 0.8f}, {ESW2, 0.1f}, {ESW3, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                            else
                                direction = Selector<WallDirection>({{NSW1, 0.8}, {NSW2, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        }
                    }
                    else if(!eSSame)
                    {
                        bool eSame = typeAtGfxTile(tileCoordinate + glm::ivec2(1, 0)) == type;
                        bool sSame = typeAtGfxTile(tileCoordinate + glm::ivec2(0, 1)) == type;

                        if(eSame && sSame)
                            direction = ESC;
                        else
                        {
                            if(sSame)
                                direction = Selector<WallDirection>({{NSW1, 0.8}, {NSW2, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                            else
                                direction = Selector<WallDirection>({{NEW1, 0.8f}, {NEW2, 0.1f}, {NEW3, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        }
                    }
                    else if(!sWSame)
                    {
                        bool sSame = typeAtGfxTile(tileCoordinate + glm::ivec2(0, 1)) == type;
                        bool wSame = typeAtGfxTile(tileCoordinate + glm::ivec2(-1, 0)) == type;

                        if(sSame && wSame)
                            direction = SWC;
                        else
                        {
                            if(wSame)
                                direction = Selector<WallDirection>({{NEW1, 0.8f}, {NEW2, 0.1f}, {NEW3, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                            else
                                direction = Selector<WallDirection>({{NES1, 0.8}, {NES2, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        }
                    }
                    else
                    {
                        bool wSame = typeAtGfxTile(tileCoordinate + glm::ivec2(-1, 0)) == type;
                        bool nSame = typeAtGfxTile(tileCoordinate + glm::ivec2(0, -1)) == type;

                        if(wSame && nSame)
                            direction = NWC;
                        else
                        {
                            if(nSame)
                                direction = Selector<WallDirection>({{NES1, 0.8}, {NES2, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                            else
                                direction = Selector<WallDirection>({{ESW1, 0.8f}, {ESW2, 0.1f}, {ESW3, 0.1f}}, coordinateHasher(tileCoordinate)).get();
                        }
                    }
                }
                else if(cornerAmount == 4)
                    direction = Selector<WallDirection>({{NESW1, 0.95f}, {NESW2, 0.05f}}, coordinateHasher(tileCoordinate)).get();
                else
                    FEA_ASSERT(true, "this shouldn't happen!");

                mTiles.setTile(tileCoordinate, typeDirectionHasher(type, direction));
            }
            else
                mTiles.unsetTile(tileCoordinate);
        }
    }
}

bool WallTileMap::isWithin(const glm::ivec2& coordinate) const
{
    return coordinate.x >= 0 && coordinate.y >= 0 && coordinate.x < (int32_t)mWallTypes.size().x && coordinate.y < (int32_t)mWallTypes.size().y;
}
