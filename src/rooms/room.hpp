#pragma once
#include <thero/optional.hpp>
#include <fea/rendering/glmhash.hpp>
#include "rooms/roomtype.hpp"
#include "rooms/roomaction.hpp"
#include "rooms/roominvoker.hpp"


class Room
{
    public:
        Room(const RoomType& type, AnyMap<std::string> attributes, const std::unordered_set<glm::ivec2>& tiles);
        th::Optional<AnyMap<std::string>> invoke(RoomAction action, RoomInvoker& invoker, AnyMap<std::string> parameters = {});
        const RoomType& type() const;
        void destroy(const RoomInvokerHandler&);
        const AnyMap<std::string>& attributes() const;
        AnyMap<std::string>& attributes();
        const std::unordered_set<glm::ivec2>& tiles() const;
        std::unordered_set<glm::ivec2> halfTiles() const;
        bool destroyed() const;
    private:
        std::reference_wrapper<const RoomType> mType;
        bool mDestroyed;
        AnyMap<std::string> mAttributes;
        std::unordered_set<glm::ivec2> mTiles;
};
