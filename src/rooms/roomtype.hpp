#pragma once
#include <unordered_set>
#include <unordered_map>
#include "roomproperties.hpp"
#include "rooms/roomaction.hpp"
#include "rooms/roominvoker.hpp"

using RoomTypeId = int32_t;

struct RoomType
{
    RoomTypeId typeId;
    std::unordered_set<RoomProperty> properties;
    std::unordered_map<RoomAction, std::reference_wrapper<RoomInvokerHandler>> mInvokeHandlers;
    AnyMap<std::string> defaultValues;
};

bool operator==(const RoomType& a, const RoomType& b);
