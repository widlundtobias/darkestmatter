#pragma once
#include <thero/smartenum.hpp>
#include <unordered_map>
#include <string>

smart_enum(RoomProperty, SHITTY_ROOM_WHIT_NO_NOTHING)

namespace std
{
    template<> struct hash<RoomProperty>
    {
        size_t operator()(const RoomProperty& x) const
        {
            return static_cast<size_t>(x);
        }
    };
}

extern std::unordered_map<RoomProperty, std::vector<RoomProperty>> gRoomPropertyRelations;
extern std::unordered_map<std::string, RoomProperty> gRoomPropertyStringMap;

RoomProperty roomPropertyFromString(const std::string& string);
