#include "rooms/roomproperties.hpp"
#include <fea/assert.hpp>

//shoudl probably be made a more global class that both room and item can use

std::unordered_map<RoomProperty, std::vector<RoomProperty>> gRoomPropertyRelations =
{
    //{ ANIMAL, { LIVING, ORGANIC }},
    //{ FELINE, { ANIMAL }},
    //{ TIGER, { FELINE, }},
    //{ LION, { FELINE, }},
    //{ TOMATO, { LIVING, ORGANIC }},
};

std::unordered_map<std::string, RoomProperty> gRoomPropertyStringMap =
{
    {"you know what it is", SHITTY_ROOM_WHIT_NO_NOTHING},
};

RoomProperty roomPropertyFromString(const std::string& string)
{
    class RoomPropertyStringMapVerifier
    {
        public:
            RoomPropertyStringMapVerifier()
            {
                for(RoomProperty property : RoomProperty_list)
                {
                    bool existed = false;

                    for(const auto& entry : gRoomPropertyStringMap)
                    {
                        if(entry.second == property)
                        {
                            existed = true;
                            break;
                        }
                    }

                    FEA_ASSERT(existed, "Item property '" << to_string(property) << "' has no string representation given, or it is a duplicate!");
                }
            };
    };

    static RoomPropertyStringMapVerifier verifier;

    FEA_ASSERT(gRoomPropertyStringMap.count(string) != 0, "Given property '" << string << "' does not exist!");

    return gRoomPropertyStringMap.at(string);
}
