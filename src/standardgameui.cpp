#include "standardgameui.hpp"
#include "gamecontroller.hpp"
#include "renderingsystem/renderingmessages.hpp"
#include "standarduifactory.hpp"
#include "gimgui/uievents.hpp"
#include "gimgui/uitools.hpp"
#include "gimgui/layouter.hpp"
#include "gimgui/glmrectangleadaptor.hpp"

const fea::Color idleWallColor  = {82,  145, 0, 100};
const fea::Color busyWallColor  = {103, 183, 0, 100};

const fea::Color idleFloorColor = {125, 147, 0, 100};
const fea::Color busyFloorColor = {170, 201, 0, 100};

const fea::Color idleMineColor  = {172, 117, 0, 100};
const fea::Color busyMineColor  = {205, 140, 0, 100};

StandardGameUI::StandardGameUI(GameController& gameController, fea::MessageBus& bus, const ResourceStorage& resources, const glm::ivec2& size, CoordinateTransformer worldFromScreenTransformer, CoordinateTransformer screenFromWorldTransformer):
    mGameController(gameController),
    mBus(bus),
    mResources(resources),
    mWorldFromScreenTransformer(worldFromScreenTransformer),
    mScreenFromWorldTransformer(screenFromWorldTransformer),
    mUIRoot(StandardUIFactory::create(size))
{
    subscribe(mBus, *this);
    mGameController.setUI(*this);   
    
    gim::Element& hammerIcon = *mUIRoot.recursiveFindChildren({"hammer"})[0];
    gim::Element& wallIcon = *mUIRoot.recursiveFindChildren({"wall"})[0];
    gim::Element& selectionWindow = *mUIRoot.recursiveFindChildren({"selection_window"})[0];

    UiTools::addCallback(hammerIcon, "on_click", [] (gim::Element& element, const Parameters& parameters)
    {
        UiTools::toggleChildrenVisibility(element);
    });

    UiTools::addCallback(wallIcon, "on_click", [&selectionWindow] (gim::Element& element, const Parameters& parameters)
    {
        UiTools::toggleVisibility(selectionWindow);
    });

    for(auto& child : mUIRoot.recursiveFindChildren({"action_button"}))
    {
        UiTools::addCallback(*child, "on_hover", UiTools::setOnHoverColor);
        UiTools::addCallback(*child, "on_blur", UiTools::setDefaultColor);
    };

    resize(size);
}

void StandardGameUI::handleMessage(const MouseClickMessage& message)
{
    bool uiConsumedClick = false;

    uiConsumedClick = mouseClicked(mUIRoot, message.position, message.button);

    if(!uiConsumedClick)
    {//ui did not prevent the click, so we can propagate it further to the world
        glm::vec2 worldPosition = mWorldFromScreenTransformer(static_cast<glm::vec2>(message.position));

        PositionAction action;

        if(message.button == fea::Mouse::LEFT)
            action = PositionAction::PRIMARY;
        if(message.button == fea::Mouse::MIDDLE)
            action = PositionAction::TERTIARY;
        else if(message.button == fea::Mouse::RIGHT)
            action = PositionAction::SECONDARY;


        mGameController.positionAction(worldPosition, action);
    }
}

void StandardGameUI::handleMessage(const MouseMoveMessage& message)
{
    mouseMoved(mUIRoot, message.newPosition, message.lastPosition);
}

void StandardGameUI::handleMessage(const EntityMovedMessage& message)
{
    auto iter = mEntityNumbers.find(message.entityId);

    if(iter == mEntityNumbers.end())
    {
        gim::Element entity = 
        {
            {"entity_number"},
            {
                {"position", static_cast<glm::ivec2>(mScreenFromWorldTransformer(message.position) + glm::vec2(-10, -32)) },
                {"size", glm::ivec2(10, 20) },
                {"texture", std::string("white")},
                {"image_coords", Rectangle{{0, 0}, {32, 32}}},
                {"color", fea::Color(0, 0, 0)},
                {"text", std::to_string(message.entityId)},
                {"text_color", fea::Color(255, 200, 0)},
                {"text_size", 16},
                {"font", std::string("Ubuntu Mono Regular")},
            }
        };

        mEntityNumbers.emplace(message.entityId, &mUIRoot.append(std::move(entity)));
    }
    else
    {
        iter->second->setAttribute("position", static_cast<glm::ivec2>(mScreenFromWorldTransformer(message.position) + glm::vec2(-10, -32)));
    }
}

void StandardGameUI::highlightTiles(std::unordered_set<glm::ivec2> tiles, HighlightMode mode)
{
    for(const auto& tile : tiles)
        highlightTile(tile, mode);

    mBus.send(OverlayDestroyedMessage{0});
    mBus.send(OverlayCreatedMessage{0, mOverlayTiles});
}

void StandardGameUI::dehighlightTiles(std::unordered_set<glm::ivec2> tiles)
{
    for(const auto& tile : tiles)
        mOverlayTiles.erase(tile);

    mBus.send(OverlayDestroyedMessage{0});
    mBus.send(OverlayCreatedMessage{0, mOverlayTiles});
}

void StandardGameUI::update()
{
    mBus.send(UIRenderRequestedMessage{mUIRoot});
}

void StandardGameUI::setAvailableWalls(std::deque<std::reference_wrapper<const WallResource>> currentWallKnowledge)
{
    mAvailableWalls = std::move(currentWallKnowledge);

    gim::Element& selectionWindow = *mUIRoot.findChildren({"selection_window"})[0];

    while(selectionWindow.children().size() > 0)
        selectionWindow.detachChild(0);

    for(const auto& wall : mAvailableWalls)
    {
        gim::Element wallListing = StandardUIFactory::wallSelection(wall, mResources);
        selectionWindow.append(std::move(wallListing));
    }
}

void StandardGameUI::resize(const glm::ivec2 newSize)
{
    mUIRoot.setAttribute("size", newSize);

    Layouter::applyLayout(mUIRoot, true);
}

void StandardGameUI::highlightTile(const glm::ivec2& tile, HighlightMode mode)
{
    if(mode == HighlightMode::IDLE_WALL_ORDER)
        mOverlayTiles[tile] = idleWallColor;
    else if(mode == HighlightMode::BUSY_WALL_ORDER)
        mOverlayTiles[tile] = busyWallColor;
    else if(mode == HighlightMode::IDLE_FLOOR_ORDER)
        mOverlayTiles[tile] = idleFloorColor;
    else if(mode == HighlightMode::BUSY_FLOOR_ORDER)
        mOverlayTiles[tile] = busyFloorColor;
    else if(mode == HighlightMode::IDLE_MINE_ORDER)
        mOverlayTiles[tile] = idleMineColor;
    else if(mode == HighlightMode::BUSY_MINE_ORDER)
        mOverlayTiles[tile] = busyMineColor;
}
