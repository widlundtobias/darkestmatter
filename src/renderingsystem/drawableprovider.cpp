#include "drawableprovider.hpp"

DrawableProvider::DrawableProvider(const TextureMap& textures):
    mTextures(textures)
{
}

void DrawableProvider::setViewSize(const glm::vec2& viewSize)
{
    mViewSize = viewSize;
}
