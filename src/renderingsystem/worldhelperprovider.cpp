#include "worldhelperprovider.hpp"
#include <fea/assert.hpp>

WorldHelperProvider::WorldHelperProvider(const TextureMap& textures, const glm::ivec2& tileSize):
    DrawableProvider(textures),
    mTileSize(tileSize)
{
}


void WorldHelperProvider::render(fea::Renderer2D& renderer) const
{
    for(const auto& overlay : mOverlays)
    {
        renderer.render(overlay.second);
    }
}

void WorldHelperProvider::tick(int32_t frames)
{
}

int32_t WorldHelperProvider::createTileOverlay(const std::unordered_map<glm::ivec2, fea::Color>& tileColors)
{
    int32_t newId = static_cast<int32_t>(mOverlays.size());

    auto& tileMap = (*mOverlays.emplace(newId, fea::TileMap(mTileSize, {4, 4})).first).second;

    tileMap.addTileDefinition(0, fea::TileDefinition({0, 0}));

    setOverlayColors(newId, tileColors);

    return newId;
}

void WorldHelperProvider::setOverlayColors(int32_t overlayId, const std::unordered_map<glm::ivec2, fea::Color>& tileColors)
{
    FEA_ASSERT(mOverlays.count(overlayId) != 0, "Overlay id " << overlayId << " does not exist.");

    auto& tileMap = mOverlays.at(overlayId);

    for(const auto& tileColor : tileColors)
    {
        tileMap.setTile(tileColor.first, 0);
        tileMap.setTileColor(tileColor.first, tileColor.second);
    }
}

void WorldHelperProvider::unsetOverlayColors(int32_t overlayId, const std::unordered_set<glm::ivec2>& tiles)
{
    FEA_ASSERT(mOverlays.count(overlayId) != 0, "Overlay id " << overlayId << " does not exist.");

    auto& tileMap = mOverlays.at(overlayId);

    for(const auto& tile : tiles)
    {
        tileMap.unsetTile(tile);
    }
}

void WorldHelperProvider::destroyTileOverlay(int32_t overlayId)
{
    FEA_ASSERT(mOverlays.count(overlayId) != 0, "Overlay id " << overlayId << " does not exist.");

    mOverlays.erase(overlayId);
}
