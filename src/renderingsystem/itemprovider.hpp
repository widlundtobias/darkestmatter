#pragma once
#include "drawableprovider.hpp"
#include "itemtypegfx.hpp"
#include <util/numberpool.hpp>

class ItemProvider : public DrawableProvider
{
    struct Item
    {
        int32_t type;
        int32_t stackAmount;
        fea::SubrectQuad quad;
    };

    public:
        ItemProvider(const TextureMap& textures);
        void registerItemType(int32_t typeId, ItemTypeGfx type);
        int32_t createItem(int32_t typeId, const glm::vec2& position, int32_t stackAmount);
        void destroyItem(int32_t itemId);
        void render(fea::Renderer2D& renderer) const override;
        void tick(int32_t frames) override;
        void setItemPosition(int32_t itemId, const glm::vec2& position);
        void setItemStackAmount(int32_t itemId, int32_t amount);
    private:
        glm::ivec2 getSubrectStart(int32_t typeId, int32_t stackAmount) const;
        NumberPool<int32_t> mIdPool;
        std::unordered_map<int32_t, ItemTypeGfx> mItemTypes;
        std::unordered_map<int32_t, Item> mItems;
};
