#pragma once

struct ConstructionRenderType
{
    int32_t texture;
    glm::ivec2 start;
    glm::ivec2 size;
};
