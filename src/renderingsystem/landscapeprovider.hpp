#pragma once
#include "drawableprovider.hpp"
#include "drawables/floortilemap.hpp"
#include "drawables/walltilemap.hpp"
#include "drawables/walloretilemap.hpp"

class Landscape;

class LandscapeProvider : public DrawableProvider
{
    public:
        LandscapeProvider(const TextureMap& textures, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize);
        void render(fea::Renderer2D& renderer) const override;
        void tick(int32_t frames) override;
        void addWallType(int32_t wallType, int32_t textureId, const glm::ivec2& textureIndex);
        void addWallOreType(int32_t wallOreType, WallOreTileMap::OreAmountThreshold thresholds, int32_t textureId, const glm::ivec2& textureIndex);
        void addGroundType(int32_t groundType, int32_t textureId, const glm::ivec2& textureIndex);
        void addFloorType(int32_t floorType, int32_t textureId, const glm::ivec2& textureIndex);
        void setLandscape(const Landscape& landscape);
        void setWall(const glm::ivec2& position, int32_t wallType);
        void setGround(const glm::ivec2& position, int32_t groundType);
        void setFloor(const glm::ivec2& position, int32_t floorType);
        void setCullRegion(const glm::vec2& start, const glm::vec2& end);
        void setCullEnabled(bool enabled);
    private:
        FloorTileMap mGroundTiles;
        FloorTileMap mFloorTiles;
        WallTileMap mWallTiles;
        WallOreTileMap mWallOreTiles;
};
