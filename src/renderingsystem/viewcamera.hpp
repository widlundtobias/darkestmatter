#pragma once
#include <fea/render2d.hpp>
#include "util/accelerator.hpp"
#include "util/cappedint.hpp"

class ViewCamera
{
    public:
        ViewCamera(fea::Camera initialCamera);
        operator fea::Camera() const;
        void update();
        void pan(const glm::vec2& direction);
        void zoom(int32_t delta);
    private:
        glm::vec2 zoomToVecZoom(int32_t zoomLevel) const;
        fea::Camera mCamera;

        Accelerator mAccelerator;
        glm::vec2 mDirection;
        glm::vec2 mAcceleration;
        glm::vec2 mVelocity;
        float mMaxSpeed;
        float mMaxAcceleration;
        CappedInt mZoomLevel;
};
