#pragma once

struct AnimatedObjectType
{
    int32_t texture;
    int32_t defaultAnimation;
    glm::vec2 size;
    glm::vec2 offset;
    std::unordered_map<int32_t, fea::Animation> animations;
};
