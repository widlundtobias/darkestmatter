#include "constructionprovider.hpp"
#include <fea/assert.hpp>

ConstructionProvider::ConstructionProvider(const TextureMap& textures, const glm::ivec2& tileWidth):
    DrawableProvider(textures),
    mTileWidth(tileWidth)
{
}

void ConstructionProvider::registerConstructionRenderType(int64_t typeId, ConstructionRenderType type)
{
    FEA_ASSERT(mTextures.count(type.texture) != 0, "invalid texture given");
    FEA_ASSERT(mObjectTypes.count(typeId) == 0, "typeId already used");

    mObjectTypes.emplace(typeId, std::move(type));
}

int32_t ConstructionProvider::createConstruction(int64_t typeId, const glm::ivec2& tilePosition)
{
    FEA_ASSERT(mObjectTypes.count(typeId) != 0, "typeId '" << typeId << "' not registered");
    int32_t newId = mIdPool.next();

    const auto& objectType = mObjectTypes.at(typeId);

    glm::vec2 position = (glm::vec2)(tilePosition * mTileWidth);
    fea::SubrectQuad newQuad((glm::vec2)objectType.size);
    newQuad.setPosition(position);
    newQuad.setTexture(mTextures.at(objectType.texture));
    newQuad.setSubrect(objectType.start, objectType.start + objectType.size);

    mObjects.emplace(newId, Construction{typeId, newQuad});

    return newId;
}

void ConstructionProvider::destroyConstruction(int32_t objectId)
{
    FEA_ASSERT(mObjects.count(objectId) != 0, "Destroying nonexisting object");

    mObjects.erase(objectId);
}

void ConstructionProvider::render(fea::Renderer2D& renderer) const
{
    for(auto& construction : mObjects)
    {
        renderer.render(construction.second.quad);
    }
}

void ConstructionProvider::tick(int32_t)
{
}
