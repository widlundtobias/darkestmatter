#include "landscapeprovider.hpp"
#include "world/landscape.hpp"

const int32_t chunkSize = 32;

LandscapeProvider::LandscapeProvider(const TextureMap& textures, const glm::ivec2& tileSize, const glm::ivec2& textureUnitSize):
    DrawableProvider(textures),
    mGroundTiles(tileSize, textureUnitSize, chunkSize),
    mFloorTiles(tileSize, textureUnitSize, chunkSize),
    mWallTiles(tileSize, textureUnitSize, chunkSize),
    mWallOreTiles(tileSize, textureUnitSize, chunkSize)
{
}


void LandscapeProvider::render(fea::Renderer2D& renderer) const
{
    renderer.render(mGroundTiles);
    renderer.render(mFloorTiles);
    renderer.render(mWallTiles);
    renderer.render(mWallOreTiles);
}

void LandscapeProvider::tick(int32_t frames)
{
}

void LandscapeProvider::addWallType(int32_t wallType, int32_t textureId, const glm::ivec2& textureIndex)
{
    mWallTiles.defineType(static_cast<WallType>(wallType), textureIndex);

    mWallTiles.setTexture(mTextures.at(textureId)); //make layers instead
}

void LandscapeProvider::addWallOreType(int32_t wallOre, WallOreTileMap::OreAmountThreshold thresholds, int32_t textureId, const glm::ivec2& textureIndex)
{
    mWallOreTiles.defineType(static_cast<WallOreType>(wallOre), thresholds, textureIndex);

    mWallOreTiles.setTexture(mTextures.at(textureId)); //make layers instead
}

void LandscapeProvider::addGroundType(int32_t groundType, int32_t textureId, const glm::ivec2& textureIndex)
{
    mGroundTiles.defineType(static_cast<WallType>(groundType), textureIndex);

    mGroundTiles.setTexture(mTextures.at(textureId)); //make layers instead
}

void LandscapeProvider::addFloorType(int32_t floorType, int32_t textureId, const glm::ivec2& textureIndex)
{
    mFloorTiles.defineType(static_cast<WallType>(floorType), textureIndex);

    mFloorTiles.setTexture(mTextures.at(textureId)); //make layers instead
}

void LandscapeProvider::setLandscape(const Landscape& landscape)
{
    mGroundTiles.reset(landscape.ground());
    mFloorTiles.reset(landscape.floors());
    mWallTiles.reset(landscape.walls());
    mWallOreTiles.reset(landscape.wallOres());
}

void LandscapeProvider::setWall(const glm::ivec2& position, int32_t wallType)
{
    if(wallType == 0)
    {
        mWallTiles.unset(position);
    }
    else
    {
        mWallTiles.set(position, wallType);
    }
}

void LandscapeProvider::setGround(const glm::ivec2& position, int32_t groundType)
{
    if(groundType == 0)
    {
        mGroundTiles.unset(position);
    }
    else
    {
        mGroundTiles.set(position, groundType);
    }
}

void LandscapeProvider::setFloor(const glm::ivec2& position, int32_t floorType)
{
    if(floorType == 0)
    {
        mFloorTiles.unset(position);
    }
    else
    {
        mFloorTiles.set(position, floorType);
    }
}

void LandscapeProvider::setCullRegion(const glm::vec2& start, const glm::vec2& end)
{
    mGroundTiles.setCullRegion(start, end);
    mFloorTiles.setCullRegion(start, end);
    mWallTiles.setCullRegion(start, end);
    mWallOreTiles.setCullRegion(start, end);
}

void LandscapeProvider::setCullEnabled(bool enabled)
{
    mGroundTiles.setCullEnabled(enabled);
    mFloorTiles.setCullEnabled(enabled);
    mWallTiles.setCullEnabled(enabled);
    mWallOreTiles.setCullEnabled(enabled);
}
