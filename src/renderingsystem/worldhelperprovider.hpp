#pragma once
#include "drawableprovider.hpp"
#include <unordered_map>
#include <unordered_set>

class WorldHelperProvider : public DrawableProvider
{
    public:
        WorldHelperProvider(const TextureMap& textures, const glm::ivec2& tileSize);
        void render(fea::Renderer2D& renderer) const override;
        void tick(int32_t frames) override;
        int32_t createTileOverlay(const std::unordered_map<glm::ivec2, fea::Color>& tileColors);
        void setOverlayColors(int32_t overlayId, const std::unordered_map<glm::ivec2, fea::Color>& tileColors);
        void unsetOverlayColors(int32_t overlayId, const std::unordered_set<glm::ivec2>& tiles);
        void destroyTileOverlay(int32_t overlayId);
    private:
        glm::ivec2 mTileSize;
        std::unordered_map<int32_t, fea::TileMap> mOverlays;
};
