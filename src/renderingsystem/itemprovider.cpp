#include "itemprovider.hpp"
#include <fea/assert.hpp>

ItemProvider::ItemProvider(const TextureMap& textures):
    DrawableProvider(textures)
{
}

void ItemProvider::registerItemType(int32_t typeId, ItemTypeGfx type)
{
    FEA_ASSERT(mTextures.count(type.texture) != 0, "invalid texture given");
    FEA_ASSERT(mItemTypes.count(typeId) == 0, "typeId already used");

    mItemTypes.emplace(typeId, std::move(type));
}

int32_t ItemProvider::createItem(int32_t typeId, const glm::vec2& position, int32_t stackAmount)
{
    FEA_ASSERT(mItemTypes.count(typeId) != 0, "typeId not registered");
    int32_t newId = mIdPool.next();

    const auto& itemType = mItemTypes.at(typeId);

    Item newItem;
    newItem.type = typeId;
    newItem.stackAmount = stackAmount;
    newItem.quad = fea::SubrectQuad(itemType.size);
    newItem.quad.setPosition(position);
    newItem.quad.setOrigin(newItem.quad.getSize() / 2.0f);
    newItem.quad.setTexture(mTextures.at(itemType.texture));

    glm::ivec2 subRectStart = getSubrectStart(typeId, stackAmount);

    newItem.quad.setSubrect(subRectStart, subRectStart + itemType.subRectSize);
    mItems.emplace(newId, newItem);

    return newId;
}

void ItemProvider::destroyItem(int32_t itemId)
{
    FEA_ASSERT(mItems.count(itemId) != 0, "Destroying nonexisting item");

    mItems.erase(itemId);
}

void ItemProvider::render(fea::Renderer2D& renderer) const
{
    for(auto& item : mItems)
    {
        renderer.render(item.second.quad);
    }
}

void ItemProvider::tick(int32_t frames)
{
}

void ItemProvider::setItemPosition(int32_t itemId, const glm::vec2& position)
{
    FEA_ASSERT(mItems.count(itemId) != 0, "Setting position of nonexisting item");

    mItems.at(itemId).quad.setPosition(position);
}

void ItemProvider::setItemStackAmount(int32_t itemId, int32_t stackAmount)
{
    FEA_ASSERT(mItems.count(itemId) != 0, "Setting stack amount of nonexisting item");
    FEA_ASSERT(stackAmount > 0, "stackAmount must be at least 1");

    auto& item = mItems.at(itemId);
    item.stackAmount = stackAmount;
    auto& type = mItemTypes.at(item.type);

    auto subRectStart = getSubrectStart(item.type, item.stackAmount);

    item.quad.setSubrect(subRectStart, subRectStart + type.subRectSize);
}

glm::ivec2 ItemProvider::getSubrectStart(int32_t typeId, int32_t stackAmount) const
{
    auto type = mItemTypes.at(typeId);
    auto thresholds = type.amountThresholds;

    size_t index = 0;
    int32_t currentElement;
    bool broke = false;

    for(; index < thresholds.size(); ++index)
    {
        currentElement = thresholds[index];

        if(stackAmount < currentElement)
        {
            broke = false;
            break;
        }
    }

    if(broke)
        index = thresholds.size();

    return type.subRectStart + glm::ivec2(type.subRectSize.x * static_cast<int32_t>(index), 0);
}
