#pragma once
#include <deque>
#include <glm/glm.hpp>

struct ItemTypeGfx
{
    int32_t texture;
    glm::vec2 size;
    glm::ivec2 subRectStart;
    glm::ivec2 subRectSize;
    std::deque<int32_t> amountThresholds;
};
