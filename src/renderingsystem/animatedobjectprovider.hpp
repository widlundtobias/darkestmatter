#pragma once
#include "drawableprovider.hpp"
#include "animatedobjecttype.hpp"
#include <util/numberpool.hpp>

class AnimatedObjectProvider : public DrawableProvider
{
    struct AnimatedObject
    {
        int32_t type;
        fea::AnimatedQuad quad;
    };

    public:
        AnimatedObjectProvider(const TextureMap& textures);
        void registerAnimatedObjectType(int32_t typeId, AnimatedObjectType type);
        int32_t createAnimatedObject(int32_t typeId, const glm::vec2& position);
        void destroyAnimatedObject(int32_t objectId);
        void render(fea::Renderer2D& renderer) const override;
        void tick(int32_t frames) override;
        void setAnimatedObjectPosition(int32_t objectId, const glm::vec2& position);
        void setAnimatedObjectAnimation(int32_t objectId, int32_t animation);
    private:
        NumberPool<int32_t> mIdPool;
        std::unordered_map<int32_t, AnimatedObjectType> mObjectTypes;
        std::unordered_map<int32_t, AnimatedObject> mObjects;
};
