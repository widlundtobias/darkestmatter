#include "uiprovider.hpp"

UIProvider::UIProvider(const TextureMap& textures):
    DrawableProvider(textures),
    mElementToRender(nullptr)
{
}


void UIProvider::render(fea::Renderer2D& renderer) const
{
    if(mElementToRender)
    {
        fea::Camera oldCam = renderer.getViewport().getCamera();
        renderer.getViewport().setCamera({mViewSize / 2.0f});

        mGuiDrawable.setElement(*mElementToRender);
        renderer.render(mGuiDrawable);

        renderer.getViewport().setCamera(oldCam);
    }
}

void UIProvider::tick(int32_t frames)
{
    mElementToRender = nullptr;
}

void UIProvider::queueElement(const gim::Element& element)
{
    mElementToRender = &element;
}

void UIProvider::textureAdded(int32_t id, fea::Texture& texture, const std::string& textureName)
{
    mGuiDrawable.registerTexture(textureName, texture);
}

void UIProvider::addFont(int32_t fontId, gim::Font font, const std::string& fontName)
{
    mFonts.emplace_back(std::make_unique<gim::Font>(std::move(font)));

    mGuiDrawable.addFont(*mFonts.back());
}
