#include "viewcamera.hpp"

ViewCamera::ViewCamera(fea::Camera initialCamera):
    mCamera(initialCamera),
    mMaxSpeed(18.0f),
    mMaxAcceleration(1.8f),
    mZoomLevel(1, 4)
{
}

ViewCamera::operator fea::Camera() const
{
    return mCamera;
}

void ViewCamera::update()
{
    mAcceleration = mAccelerator.get(mDirection, mMaxSpeed, mVelocity, mMaxAcceleration);
    mVelocity += mAcceleration;
    glm::vec2 newPosition = static_cast<glm::vec2>(mCamera.getPosition() + (mVelocity / zoomToVecZoom(mZoomLevel))); //effectively caps to even pixels
    newPosition.x = std::floor(newPosition.x);
    newPosition.y = std::floor(newPosition.y);
    mCamera.setPosition(newPosition);
}

void ViewCamera::pan(const glm::vec2& direction)
{
    mDirection = direction;
}

void ViewCamera::zoom(int32_t delta)
{
    mZoomLevel += delta;

    mCamera.setZoom(zoomToVecZoom(mZoomLevel));
}

glm::vec2 ViewCamera::zoomToVecZoom(int32_t zoomLevel) const
{
    return glm::vec2(1.0f, 1.0f) * static_cast<float>(mZoomLevel);
}
