#pragma once

using WallOreType = int32_t;

struct WallOre
{
    WallOreType type = 0;
    int32_t amount;

};

