#include "landscape.hpp"
#include "landscapecallbackhandler.hpp"

Landscape::Landscape(const glm::ivec2& size, LandscapeCallbackHandler* gameWorld):
    mGround(size, 0),
    mFloors(size, 0),
    mWalls(size, 0),
    mWallOre(size, {}),
    mCallbackHandler(gameWorld)
{
}

void Landscape::setGround(const glm::ivec2& position, GroundType type)
{
    mGround.set(position, type);

    if(mCallbackHandler)
        mCallbackHandler->groundSet(position, type);
}

const Grid<GroundType>& Landscape::ground() const
{
    return mGround;
}

void Landscape::setFloor(const glm::ivec2& position, FloorType type)
{
    mFloors.set(position, type);

    if(mCallbackHandler)
        mCallbackHandler->floorSet(position, type);
}

const Grid<FloorType>& Landscape::floors() const
{
    return mFloors;
}

void Landscape::setWall(const glm::ivec2& position, WallType type)
{
    mWalls.set(position, type);

    if(mCallbackHandler)
        mCallbackHandler->wallSet(position, type);
}

const Grid<WallType>& Landscape::walls() const
{
    return mWalls;
}

void Landscape::setWallOre(const glm::ivec2& position, const WallOre& ore)
{
    mWallOre.set(position, ore);

    if(mCallbackHandler)
        mCallbackHandler->wallOreSet(position, ore);
}

const Grid<WallOre>& Landscape::wallOres() const
{
    return mWallOre;
}

bool Landscape::inBounds(const glm::ivec2& position) const
{
    return !(position.x < 0 || position.y < 0 || position.x >= mWalls.size().x || position.y >= mWalls.size().y);
}

void Landscape::setCallbackHandler(LandscapeCallbackHandler& gameWorld)
{
    mCallbackHandler = &gameWorld;
}
