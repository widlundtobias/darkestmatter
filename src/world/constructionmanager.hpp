#pragma once
#include "construction.hpp"
#include "world/constructiontype.hpp"
#include <util/numberpool.hpp>
#include <unordered_map>
#include <fea/rendering/glmhash.hpp>

class ConstructionManagerCallbackHandler;
class Landscape;

class ConstructionManager
{
    public:
        ConstructionManager(ConstructionManagerCallbackHandler& constructionManagerCallbackHandler, Landscape& landscape);
        void addConstructionType(ConstructionType type, ConstructionTypeData typeData);
        th::Optional<int32_t> create(ConstructionType type, const glm::ivec2& position);
        th::Optional<int32_t> at(const glm::ivec2& position) const;
        const Construction* get(int32_t id) const;
        bool constructionAllowed(const glm::ivec2& position) const;
        int32_t construct(int32_t id, int32_t constructionAmount);
        th::Optional<Item> addMaterial(int32_t id, Item item);
    private:
        ConstructionManagerCallbackHandler& mConstructionManagerCallbackHandler;
        Landscape& mLandscape;
        //types
        std::unordered_map<ConstructionType, ConstructionTypeData> mTypes;
        //instances
        NumberPool<int32_t> mConstructionIdPool;
        std::unordered_map<int32_t, std::unique_ptr<Construction>> mConstructions;
        std::unordered_map<glm::ivec2, int32_t> mPositionIndex;
};
