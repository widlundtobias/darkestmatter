#include "itemmanager.hpp"
#include "items/item.hpp"
#include "items/handlers/feedhandler.hpp"
#include "items/handlers/puthandler.hpp"
#include "items/handlers/inspecthandler.hpp"
#include "items/handlers/takeouthandler.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "items/itemproperties.hpp"
#include "items/inventory.hpp"

ItemManager::ItemManager(const RelationshipDatabase& propertyRelations, std::unordered_map<int32_t, ItemTypeData> itemTypeData, GameWorld& world):
    mPropertyRelations(propertyRelations),
    mGameWorld(world),
    mInvokeHandlers(
{
    {"feed_handler", FeedHandler()},
    {"put_handler", PutHandler()},
    {"inspect_handler", InspectHandler()},
    {"take_out_handler", TakeOutHandler()}
})
{
    for(auto itemTypeDataIter : itemTypeData)
    {

        for(auto invokeHandler : itemTypeDataIter.second.invokeHandlers)
        {
            FEA_ASSERT(mInvokeHandlers.count(invokeHandler.second) != 0, "Invoker handler '" << invokeHandler.second << "' given but that handler doesn't exist");

            itemTypeDataIter.second.type.mInvokeHandlers.emplace(invokeHandler.first, *mInvokeHandlers.at((invokeHandler.second)));
        }

        mItemTypes.emplace(itemTypeDataIter.first, itemTypeDataIter.second.type);
    }
}

Item ItemManager::createItem(int32_t itemType)
{
    FEA_ASSERT(mItemTypes.count(itemType) != 0, "Cannot create invalid item ID '" << itemType << "'");

    Item newItem(mItemTypes.at(itemType), mItemTypes.at(itemType).defaultValues);

    if(typeSatisfiesProperty(newItem.type(), ItemProperty::CONTAINER))
    {
        //containers should have an inventory
//        newItem.attributes().emplace("inventory", Inventory({}, {}, newItem.type().size));
        newItem.attributes().emplace("inventory", Inventory({}, {}, {}));
    }

    return newItem;
}

th::Optional<Item> ItemManager::putItem(Item item, const glm::ivec2& position)
{
    if(item.destroyed())
    {
        //just let it vanish
        return {};
    }

    auto iter = mItemStackIndex.find(position);

    if(iter == mItemStackIndex.end())
    {
        //if no stack exists, it means we can create one

        std::deque<glm::ivec2> extraOccupy; //big items will occupy these extra tiles

        bool isBig = item.type().big;
        glm::ivec2 placedOnTile = position; //the actual tile it is placed on. if it is a big item, this will be the topleft of the big item

        if(isBig)
        {
            //except that if the item is big, we must ensure that the whole Tile (not half tile) it is in is free

            placedOnTile = getTopLeftOfTile(placedOnTile);

            //these will be the neighbours in the same Tile group of HalfTiles
            glm::ivec2 neighbourX  = placedOnTile + glm::ivec2(1, 0);
            glm::ivec2 neighbourY  = placedOnTile + glm::ivec2(0, 1);
            glm::ivec2 neighbourXY = placedOnTile + glm::ivec2(1, 1);

            if(mItemStackIndex.count(placedOnTile) || mItemStackIndex.count(neighbourX) || mItemStackIndex.count(neighbourY) || mItemStackIndex.count(neighbourXY))
            {
                //placing the big item won't work since it doesn't fit in these 2x2 half-tiles for this position
                return {std::move(item)};
            }
            else
            {
                extraOccupy.emplace_back(neighbourX);
                extraOccupy.emplace_back(neighbourY);
                extraOccupy.emplace_back(neighbourXY);
            }
        }

        int32_t newId = mItemStackIdPool.next();

        mItemStacks.emplace(newId, ItemStack {item.type(), {std::move(item)}});

        mItemStackIndex.emplace(placedOnTile, newId);
        for(const auto& extraPosition : extraOccupy)
            mItemStackIndex.emplace(extraPosition, newId);

        glm::vec2 worldPosition = item.type().big ? HalfTiles::tileTopLeft(placedOnTile) + static_cast<glm::vec2>(HalfTiles::size()) : HalfTiles::tileCenter(placedOnTile);
        mGameWorld.itemStackCreated(item.type(), newId, worldPosition, 1);
    }
    else
    {
        //if a stack does exist, we can check if we can put it in it
        FEA_ASSERT(mItemStacks.count(iter->second) != 0, "Item stack doesn't exist but it should");
        auto& stack = mItemStacks.at(iter->second);

        FEA_ASSERT(static_cast<int32_t>(stack.items.size()) <= stack.type.maxStack, "Stack count exceeds maximum");
        bool stackFull = static_cast<int32_t>(stack.items.size()) == stack.type.maxStack;
        bool sameType = stack.type.typeId == item.type().typeId;

        if(!stackFull && sameType)
        {
            //put it in
            stack.items.push_back(std::move(item));
            mGameWorld.itemStackUpdated(iter->second, static_cast<int32_t>(stack.items.size()));
        }
        else
        {
            //give up and return item
            return {std::move(item)};
        }
    }

    return {};
}

th::Optional<ItemManager::ItemStackView> ItemManager::stackAt(const glm::ivec2& position) const
{
    auto iter = mItemStackIndex.find(position);

    if(iter != mItemStackIndex.end())
    {
        FEA_ASSERT(mItemStacks.count(iter->second) != 0, "Item stack doesn't exist but it should");
        return ItemManager::ItemStackView {mItemStacks.at(iter->second), iter->second, mItemStacks.at(iter->second).type.typeId, position};
    }

    return {};
}

th::Optional<Item> ItemManager::takeItem(const glm::ivec2& position, int32_t stackIndex)
{
    auto stackIter = mItemStackIndex.find(position);
    if(stackIter != mItemStackIndex.end())
    {
        //does the stack exist?
        int32_t stackId = mItemStackIndex.at(position);
        FEA_ASSERT(mItemStacks.count(stackId) != 0, "Item stack doesn't exist but it should");
        auto& itemStack = mItemStacks.at(stackId);

        if(itemStack.items.size() > static_cast<size_t>(stackIndex))
        {
            //can only take an item if the stack fits the index
            Item toReturn = std::move(itemStack.items[static_cast<size_t>(stackIndex)]);
            itemStack.items.erase(itemStack.items.begin() + stackIndex);

            if(itemStack.items.empty())
            {
                //stack depleted
                bool big = itemStack.type.big;
                mGameWorld.itemStackDepleted(stackId);
                mItemStacks.erase(stackId);

                if(!big)
                {
                    //if it is small you can just delete the single position
                    mItemStackIndex.erase(position);
                }
                else
                {
                    //otherwise we must delete all 4 entries
                    glm::ivec2 topLeft = getTopLeftOfTile(position);
                    mItemStackIndex.erase(topLeft + glm::ivec2(1, 0));
                    mItemStackIndex.erase(topLeft + glm::ivec2(0, 1));
                    mItemStackIndex.erase(topLeft + glm::ivec2(1, 1));
                    mItemStackIndex.erase(topLeft);
                }
            }
            else
            {
                //stack updated
                mGameWorld.itemStackUpdated(stackId, static_cast<int32_t>(itemStack.items.size()));
            }

            return toReturn;
        }
    }

    return {};
}

ItemManager::ItemStackList ItemManager::findItems(th::Optional<int32_t> itemType)
{
    ItemStackList views;

    std::unordered_set<glm::ivec2> skipThese; //these positions belong to already checked big items so they can be skipped

    for(auto& stackIndex : mItemStackIndex)
    {
        const glm::ivec2 position = stackIndex.first;

        if(skipThese.count(position) != 0)
        {
            //this position is already checked through another one via a big object
            continue;
        }

        const ItemStack& stack = mItemStacks.at(stackIndex.second);

        if(itemType ? stack.type.typeId == mItemTypes.at(*itemType).typeId : true)
        {
            views.emplace_back(ItemStackView {stack, stackIndex.second, stack.type.typeId, position});
            if(stack.type.big)
            {
                //it's a big one, we need to put all its positions as skippable
                glm::ivec2 topLeft = getTopLeftOfTile(position);
                skipThese.emplace(topLeft + glm::ivec2(1, 0));
                skipThese.emplace(topLeft + glm::ivec2(0, 1));
                skipThese.emplace(topLeft + glm::ivec2(1, 1));
                skipThese.emplace(std::move(topLeft));
            }
        }
    }

    return views;
}

ItemManager::ItemStackList ItemManager::findItems(ItemProperty property)
{
    ItemStackList views;

    std::unordered_set<glm::ivec2> skipThese; //these positions belong to already checked big items so they can be skipped

    for(auto& stackIndex : mItemStackIndex)
    {
        const glm::ivec2 position = stackIndex.first;

        if(skipThese.count(position) != 0)
        {
            //this position is already checked through another one via a big object
            continue;
        }

        const ItemStack& stack = mItemStacks.at(stackIndex.second);

        if(typeSatisfiesProperty(stack.type, property))
        {
            views.emplace_back(ItemStackView {stack, stackIndex.second, stack.type.typeId, position});
            if(stack.type.big)
            {
                //it's a big one, we need to put all its positions as skippable
                glm::ivec2 topLeft = getTopLeftOfTile(position);
                skipThese.emplace(topLeft + glm::ivec2(1, 0));
                skipThese.emplace(topLeft + glm::ivec2(0, 1));
                skipThese.emplace(topLeft + glm::ivec2(1, 1));
                skipThese.emplace(std::move(topLeft));
            }
        }
    }

    return views;
}

ItemManager::ItemStackList ItemManager::findItems(const std::unordered_set<glm::ivec2>& tiles, th::Optional<int32_t> itemType)
{
    ItemStackList views;

    for(auto tile : tiles)
    {
        auto stack = stackAt(tile);
        if(stack && (!itemType || stack->typeId == *itemType))
            views.emplace_back(*stack);
    }

    return views;
}

ItemManager::ItemStackList ItemManager::findItems(const std::unordered_set<glm::ivec2>& tiles, ItemProperty property)
{
    ItemStackList views;

    for(auto tile : tiles)
    {
        auto stack = stackAt(tile);
        if(stack && typeSatisfiesProperty(stack->stack.get().type, property))
            views.emplace_back(*stack);
    }

    return views;
}

const ItemType& ItemManager::itemType(int32_t itemTypeID)
{
    return mItemTypes.at(itemTypeID);
}

th::Optional<ItemManager::ItemStackView> ItemManager::stack(int32_t stackId) const
{
    auto stackIterator = mItemStacks.find(stackId);

    if(stackIterator != mItemStacks.end())
    {
        return ItemStackView {stackIterator->second, stackId, stackIterator->second.type.typeId, stackPosition(stackId)};
    }
    else
    {
        return {};
    }
}

th::Optional<AnyMap<std::string>> ItemManager::invokeItem(int32_t stackId, int32_t stackIndex, ItemAction action, ItemInvoker& invoker, AnyMap<std::string> parameters)
{
    FEA_ASSERT(mItemStacks.count(stackId) != 0, "cannot invoke item in invalid stack");

    auto& stack = mItemStacks.at(stackId);

    FEA_ASSERT(static_cast<size_t>(stackIndex) < stack.items.size(), "cannot invoke item index " << stackIndex << " in a stack with just " << stack.items.size() << " items\n");

    auto& item = stack.items[static_cast<size_t>(stackIndex)];

    //FIXME: remove object if it is consumed
    return item.invoke(action, invoker, std::move(parameters));
}

bool ItemManager::typeSatisfiesProperty(const ItemType& type, ItemProperty property) const
{
    for(auto typeProperty : type.properties)
        if(mPropertyRelations.is(typeProperty, property))
            return true;

    return false;
}

th::Optional<ItemManager::ItemStackView> ItemManager::renewStackView(const ItemStackView& stackView) const
{
    auto renewedStack = stackAt(stackView.position);

    if(renewedStack && renewedStack->typeId == stackView.typeId)
    {
        //there is a stack of the same type and position. Return this one
        return renewedStack;
    }
    else
    {
        //either type or position differs, cannot renew
        return {};
    }
}

glm::ivec2 ItemManager::getTopLeftOfTile(glm::ivec2 coordinate) const
{
    if(coordinate.x % 2 == 1)
        --coordinate.x;
    if(coordinate.y % 2 == 1)
        --coordinate.y;

    return coordinate;
}

glm::ivec2 ItemManager::stackPosition(int32_t stackId) const
{
    FEA_ASSERT(mItemStacks.count(stackId) != 0, "cannot get position of invalid stack");

    for(auto indexIter : mItemStackIndex)
        if(indexIter.second == stackId)
            return indexIter.first;

    FEA_ASSERT(false, "didn't find any position. bug");

    return {0, 0};
}
