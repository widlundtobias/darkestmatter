#pragma once
#include <deque>
#include <unordered_set>
#include <unordered_map>
#include <glm/glm.hpp>
#include <fea/rendering/glmhash.hpp>
#include "constructiontype.hpp"
#include "items/item.hpp"

struct Construction
{
    ConstructionType  constructionType;
    int32_t untilDone;
    glm::ivec2 position;
    std::unordered_set<glm::ivec2> occupiedPositions;
    std::unordered_map<int32_t, int32_t> materialsNeeded;
    std::deque<Item> materialsAdded;
};
