#include "landscapepathadaptor.hpp"
#include "landscapesemantics.hpp"

LandscapePathAdaptor::LandscapePathAdaptor(const Grid<WallType>& map) :
    mMap(map)
{
}

const glm::ivec2 LandscapePathAdaptor::getNeighbor(const glm::ivec2& tile, uint32_t index) const
{
    uint32_t skip = index;

    glm::ivec2 up = glm::ivec2(0, -1);
    if(!isSolid(tile + up))
    {
        if(skip == 0)
            return tile + up;
        --skip;
    }
    
    glm::ivec2 right = glm::ivec2(1, 0);
    if(!isSolid(tile + right))
    {
        if(skip == 0)
            return tile + right;
        --skip;
    }

    glm::ivec2 down = glm::ivec2(0, 1);
    if(!isSolid(tile + down))
    {
        if(skip == 0)
            return tile + down;
        --skip;
    }
    
    glm::ivec2 left = glm::ivec2(-1, 0);
    if(!isSolid(tile + left))
    {
        if(skip == 0)
            return tile + left;
        --skip;
    }

    FEA_ASSERT(false, "Woops, bug in tile adaptor");

    return glm::ivec2();
}

uint32_t LandscapePathAdaptor::getNeighborAmount(const glm::ivec2& tile) const
{
    uint32_t amount = 0;

    glm::ivec2 up = glm::ivec2(0, -1);
    if(!isSolid(tile + up))
        amount++;

    glm::ivec2 right = glm::ivec2(1, 0);
    if(!isSolid(tile + right))
        amount++;

    glm::ivec2 down = glm::ivec2(0, 1);
    if(!isSolid(tile + down))
        amount++;
    
    glm::ivec2 left = glm::ivec2(-1, 0);
    if(!isSolid(tile + left))
        amount++;

    return amount;
}

int32_t LandscapePathAdaptor::getStepCost(const glm::ivec2& tileA, const glm::ivec2& tileB) const
{
    return 1;
}

int32_t LandscapePathAdaptor::estimateDistance(const glm::ivec2& start, const glm::ivec2& target) const
{
    return static_cast<int32_t>(glm::distance((glm::vec2)start, (glm::vec2)target) * 10.0f);
}

bool LandscapePathAdaptor::isSolid(const glm::ivec2& tile) const
{
    if(mMap.inBounds(tile))
        return LandscapeSemantics::isSolid(mMap.at(tile));

    return true;
}
