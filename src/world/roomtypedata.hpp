#pragma once
#include "rooms/roomtype.hpp"

struct RoomTypeData
{
    RoomType type;
    std::unordered_map<RoomAction, std::string> invokeHandlers;
};
