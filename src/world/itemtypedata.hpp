#pragma once
#include "items/itemtype.hpp"

struct ItemTypeData
{
    ItemType type;
    std::unordered_map<ItemAction, std::string> invokeHandlers;
};
