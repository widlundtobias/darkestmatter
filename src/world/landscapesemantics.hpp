#pragma once
#include "world/landscape.hpp"

class LandscapeSemantics
{
    public:
        static bool isSolid(WallType type);
};
