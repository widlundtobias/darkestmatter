#pragma once
#include "items/itemtype.hpp"

class ItemManagerCallbackHandler
{
    public:
        virtual void itemStackCreated(const ItemType& type, int32_t id, const glm::vec2& position, int32_t itemCount) = 0;
        virtual void itemStackUpdated(int32_t id, int32_t itemCount) = 0;
        virtual void itemStackDepleted(int32_t id) = 0;
};
