#pragma once
#include <util/numberpool.hpp>
#include <unordered_map>
#include <thero/polymorphicwrapper.hpp>
#include "rooms/room.hpp"
#include "rooms/roomtype.hpp"
#include "rooms/roominvoker.hpp"
#include "world/roomtypedata.hpp"
#include "items/invokehandler.hpp"
#include "items/iteminvoker.hpp"

class GameWorld;

using RoomId = int32_t;

class RoomManager
{
    public:
        RoomManager(std::unordered_map<int32_t, RoomTypeData> roomTypeData, GameWorld& world);
        RoomId createRoom(RoomTypeId roomType, const std::unordered_set<glm::ivec2>& tiles);
        RoomId createRoom(RoomTypeId roomType, glm::ivec2 pos, glm::ivec2 size);
        bool isRoomValid(RoomId roomID); // maybe change Valid -> Ready || Complete || Working
        th::Optional<RoomId> roomAt(const glm::ivec2& position) const;
        std::unordered_set<RoomId> rooms();
        std::unordered_set<RoomId> rooms(RoomTypeId typeId);
        const Room* room(RoomId roomId);
        bool typeSatisfiesProperty(const RoomType& type, RoomProperty property) const; //oh i actually did not need this lol
    private:
        GameWorld& mGameWorld;

        //room type stuff
        std::unordered_map<std::string, th::PolymorphicWrapper<RoomInvokerHandler>> mInvokeHandlers;
        std::unordered_map<RoomTypeId, RoomType> mRoomTypes;

        NumberPool<RoomId> mRoomIdPool;
        std::unordered_map<RoomId, Room> mRooms;
        std::unordered_map<glm::ivec2, RoomId> mRoomIndex;
};
