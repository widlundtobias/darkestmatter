#include "roommanager.hpp"
#include "rooms/roomproperties.hpp"
#include "items/itemproperties.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "world/itemmanager.hpp"
#include "world/landscape.hpp"
#include "util/anytools.hpp"

RoomManager::RoomManager(std::unordered_map<int32_t, RoomTypeData> roomTypeData, GameWorld& world):
    mGameWorld(world),
    mInvokeHandlers(
{
////    {"feed_handler", FeedHandler()},
////    {"put_handler", PutHandler()},
////    {"inspect_handler", InspectHandler()},
////    {"take_out_handler", TakeOutHandler()}
})
{
    for(auto roomTypeDataIter : roomTypeData)
    {
        for(auto invokeHandler : roomTypeDataIter.second.invokeHandlers)
        {
            FEA_ASSERT(mInvokeHandlers.count(invokeHandler.second) != 0, "Invoker handler '" << invokeHandler.second << "' given but that handler doesn't exist");

            roomTypeDataIter.second.type.mInvokeHandlers.emplace(invokeHandler.first, *mInvokeHandlers.at((invokeHandler.second)));
        }

        mRoomTypes.emplace(roomTypeDataIter.first, roomTypeDataIter.second.type);
    }
}

RoomId RoomManager::createRoom(RoomTypeId roomType, const std::unordered_set<glm::ivec2>& tiles)
{
    FEA_ASSERT(mRoomTypes.count(roomType) != 0, "Cannot create invalid room ID '" << roomType << "'");

    RoomId roomId = mRoomIdPool.next();

    mRooms.emplace(std::make_pair(roomId, Room(mRoomTypes.at(roomType), mRoomTypes.at(roomType).defaultValues, tiles)));

    for(auto tile : tiles)
    {
        mRoomIndex[tile] = roomId;
    }

    return roomId;

//    if(typeSatisfiesProperty(newItem.type(), ItemProperty::CONTAINER))
//    {
//        //containers should have an inventory
//        newItem.attributes().emplace("inventory", Inventory({}, {}, newItem.type().size));
//    }

//    return newItem;
}

RoomId RoomManager::createRoom(RoomTypeId roomType, glm::ivec2 pos, glm::ivec2 size)
{
    std::unordered_set<glm::ivec2> tiles;
    for(int x = 0; x < size.x; x++)
    {
        for(int y = 0; y < size.y; y++)
            tiles.insert({pos.x + x, pos.y + y});
    }

    return createRoom(roomType, tiles);
}

th::Optional<RoomId> RoomManager::roomAt(const glm::ivec2& position) const
{
    auto iter = mRoomIndex.find(position);

    if(iter != mRoomIndex.end())
    {
        FEA_ASSERT(mRooms.count(iter->second) != 0, "room doesn't exist but it should");
        return {iter->second};
    }

    return {};
}

bool RoomManager::isRoomValid(RoomId roomID)
{
    FEA_ASSERT(mRooms.find(roomID) != mRooms.end(), "Room dosnt exist");
    auto& room = mRooms.at(roomID);

    auto requiredItems = AnyTools::getOrNone<std::unordered_set<ItemProperty>>(std::string("required_items"), room.attributes());
    if(requiredItems)
    {
        for(auto tile : room.halfTiles())
        {
            auto stack = mGameWorld.items().stackAt(tile);
            if(stack)
            {
                auto& itemType = stack->stack.get().type;
                for(auto it = requiredItems->begin(); it != requiredItems->end();)
                {
                    if(mGameWorld.items().typeSatisfiesProperty(itemType, *it))
                        it = requiredItems->erase(it);
                    else
                        it++;
                }
            }

            if(requiredItems->empty())
                break;
        }

        if(requiredItems->size()) //room miss one of the required item
            return false;
    }

    auto requireFloor = AnyTools::getOrNone<bool>(std::string("require_floor"), room.attributes());
    if(requireFloor && *requireFloor)
    {
        for(auto tile : room.tiles())
        {
            if(!mGameWorld.landscape().floors().at(tile)) ///TODO: find a way to get if the floor is contructed
                return false;
        }
    }

    auto requireWall = AnyTools::getOrNone<bool>(std::string("require_wall"), room.attributes());
    if(requireWall && *requireWall)
    {
        std::unordered_set<glm::ivec2> toCheck;
        for(auto tile : room.tiles())
        {
            toCheck.insert(tile + glm::ivec2(0, 1));
            toCheck.insert(tile + glm::ivec2(1, 0));
            toCheck.insert(tile - glm::ivec2(0, 1));
            toCheck.insert(tile - glm::ivec2(1, 0));
        }

        int canSkipWall = 0; // skip one missing wall because we have no door yet
        for(auto tile : toCheck)
        {
            if((!mGameWorld.landscape().walls().at(tile) && canSkipWall++) && (!mGameWorld.rooms().roomAt(tile) || *mGameWorld.rooms().roomAt(tile) != roomID))  ///TODO: find a way to get if the wall is contructed
                return false;
        }
    }

    return true;
}

std::unordered_set<RoomId> RoomManager::rooms()
{
    std::unordered_set<RoomId> ids;
    for(auto& pair : mRooms)
        ids.insert(pair.first);

    return ids;
}

std::unordered_set<RoomId> RoomManager::rooms(RoomTypeId typeId)
{
    std::unordered_set<RoomId> ids;
    for(auto& pair : mRooms)
    {
        if(pair.second.type().typeId == typeId)
            ids.insert(pair.first);
    }

    return ids;
}

const Room* RoomManager::room(RoomId roomId)
{
    if(mRooms.find(roomId) != mRooms.end())
        return &mRooms.at(roomId);

    return nullptr;
}

bool RoomManager::typeSatisfiesProperty(const RoomType& type, RoomProperty property) const
{
    for(auto typeProperty : type.properties)
//        if(mPropertyRelations.is(typeProperty, property)) //if rooms ever get some property management
        if(typeProperty == property)
            return true;

    return false;
}
