#pragma once
#include <glm/glm.hpp>
#include <thero/smartenum.hpp>

smart_enum_class(ConstructionCategory, WALL, FLOOR)

struct ConstructionType
{
    ConstructionCategory category;
    int32_t type;
};

struct ConstructionTypeData
{
    int32_t constructionAmount;
    glm::ivec2 size;
    std::unordered_map<int32_t, int32_t> materialsNeeded;
};


bool operator==(const ConstructionType& a, const ConstructionType& b);

namespace std
{
    template<> struct hash<ConstructionType>
    {
        size_t operator()(const ConstructionType& x) const
        {
            return static_cast<size_t>(x.category) | (static_cast<size_t>(x.type) << 16u);
        }
    };
}
