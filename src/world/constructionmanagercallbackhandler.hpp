#pragma once
#include "constructiontype.hpp"

class ConstructionManagerCallbackHandler
{
    public:
        virtual void constructionCreated(ConstructionType type, const glm::ivec2& position, int32_t id) = 0;
        virtual void constructionFinished(int32_t id) = 0;
};
