#include "gameworld.hpp"
#include "worldmessages.hpp"
#include "jobai/brain.hpp"
#include "world/landscape.hpp"
#include "entity/entitysystem.hpp"
#include "facility/facility.hpp"
#include "world/constructionmanager.hpp"
#include "items/relationshipdatabase.hpp"
#include "world/itemmanager.hpp"
#include "entity/animatedspritecontroller.hpp"
#include "entity/physicscontroller.hpp"
#include "entity/evadetilecontroller.hpp"
#include "entity/workercontroller.hpp"
#include "entity/taskaicontroller.hpp"
#include "entity/movementcontroller.hpp"
#include "entity/braintype.hpp"
#include "entity/movementtype.hpp"
#include "entity/movementaction.hpp"
#include "entity/carrycontroller.hpp"
#include "entity/iteminvokercontroller.hpp"
#include "items/inventory.hpp"
#include "entity/workerstatus.hpp"
#include "entity/iteminvokertypefactory.hpp"

GameWorld::GameWorld(fea::MessageBus& bus, Landscape landscape, Definitions definitions, std::unordered_map<int32_t, ItemTypeData> itemTypeData, std::unordered_map<int32_t, RoomTypeData> roomTypeData):
    mBus(bus),
    mLandscape(landscape),
    mEntityFactory(mEntities),
    mFacility(mBus),
    mConstructionManager(*this, *mLandscape),
    mItemPropertyRelations(gItemPropertyRelations),
    mItemManager(*mItemPropertyRelations, std::move(itemTypeData), *this),
    mRoomManager(std::move(roomTypeData), *this)
{
    mBus.send(LandscapeLoadedMessage{*mLandscape});

    //add data types
    fea::addBasicDataTypes(mEntityFactory);
    fea::addGlmDataTypes(mEntityFactory);

    mEntityFactory.addDataType("work_status", [] (const fea::Parameters& params)
    {
        FEA_ASSERT(params.size() == 1, "work_status can only have one parameter");

        if(params[0] == "idle")
            return WorkerStatus::IDLE;
        else if(params[0] == "working")
            return WorkerStatus::WORKING;

        FEA_ASSERT(false, "work_status given value '" << params[0] << "' which is not valid");
        return WorkerStatus::IDLE;
    });

    mEntityFactory.addDataType("brain_type", [] (const fea::Parameters& params)
    {
        FEA_ASSERT(params.size() == 1, "brain_type can only have one parameter");

        if(params[0] == "humanoid")
            return BrainType::HUMANOID;

        FEA_ASSERT(false, "brain_type given value '" << params[0] << "' which is not valid");
        return BrainType::HUMANOID;
    });

    mEntityFactory.addDataType("movement_type", [] (const fea::Parameters& params)
    {
        FEA_ASSERT(params.size() == 1, "movement_type can only have one parameter");

        if(params[0] == "land")
            return MovementType::LAND_MOVEMENT;

        FEA_ASSERT(false, "movement_type given value '" << params[0] << "' which is not valid");
        return MovementType::LAND_MOVEMENT;
    });

    mEntityFactory.addDataType("movement_action", [] (const fea::Parameters& params)
    {
        FEA_ASSERT(params.size() == 1, "movement_action can only have one parameter");

        if(params[0] == "idle")
            return MovementAction::IDLE;
        else if(params[0] == "walk")
            return MovementAction::WALK;
        else if(params[0] == "run")
            return MovementAction::RUN;

        FEA_ASSERT(false, "movement_action given value '" << params[0] << "' which is not valid");
        return MovementAction::WALK;
    });

    mEntityFactory.addDataType("item_invoker_type", [] (const fea::Parameters& params)
    {
        FEA_ASSERT(params.size() == 1, "item_invoker_type can only have one parameter");

        if(params[0] == "humanoid")
            return ItemInvokerType::HUMANOID;

        FEA_ASSERT(false, "item_invoker_type given value '" << params[0] << "' which is not valid");
        return ItemInvokerType::HUMANOID;
    });

    mEntityFactory.addDataType<Brain>("brain");
    mEntityFactory.addDataType<Inventory>("inventory");
    mEntityFactory.addDataType<EntityItemInvoker>("item_invoker");

    //register attributes
    for(const auto& attribute : definitions.entityAttributes)
        mEntityFactory.registerAttribute(attribute.first, attribute.second);

    for(const auto& entityTemplate : definitions.entityTemplates)
        mEntityFactory.addTemplate(entityTemplate.first, entityTemplate.second);

    //add controllers
    mEntitySystem->addController(std::make_unique<AnimatedSpriteController>(mBus));
    mEntitySystem->addController(std::make_unique<PhysicsController>(mBus, *mLandscape));
    auto& evadeTileController = static_cast<EvadeTileController&>(mEntitySystem->addController(std::make_unique<EvadeTileController>(mBus)));
    auto& carryController = static_cast<CarryController&>(mEntitySystem->addController(std::make_unique<CarryController>(mBus, *this)));
    auto& itemInvokerController = static_cast<ItemInvokerController&>(mEntitySystem->addController(std::make_unique<ItemInvokerController>(mBus, *mItemManager)));
    auto& taskAIController = static_cast<TaskAIController&>(mEntitySystem->addController(std::make_unique<TaskAIController>(mBus, *this, evadeTileController, carryController, itemInvokerController)));
    evadeTileController.setTaskAIController(taskAIController);
    mEntitySystem->addController(std::make_unique<WorkerController>(mBus, *mFacility, taskAIController));
    mEntitySystem->addController(std::make_unique<MovementController>(mBus));

    //register constructions
    for(auto constructionIter : definitions.wallConstructions)
        mConstructionManager->addConstructionType({ConstructionCategory::WALL, constructionIter.first}, constructionIter.second);
    for(auto constructionIter : definitions.floorConstructions)
        mConstructionManager->addConstructionType({ConstructionCategory::FLOOR, constructionIter.first}, constructionIter.second);

    //landscape
    mLandscape->setCallbackHandler(*this);

    //add routines
    mFacility->routines().add({Jobs::STORAGE_KEEPING, {}});
}

GameWorld::~GameWorld()
{
}

void GameWorld::update(float deltaTime)
{
    mEntitySystem->update(deltaTime);
}

fea::EntityPtr GameWorld::createEntity(const std::string& entityTemplate)
{
    fea::EntityPtr createdEntity = mEntityFactory.instantiate(entityTemplate).lock();

    mEntitySystem->entityCreated(createdEntity);

    return createdEntity;
}

std::deque<fea::EntityPtr> GameWorld::entities(std::function<bool(fea::EntityPtr)> filter) const
{
    std::deque<fea::EntityPtr> result;

    auto all = mEntities.getAll();

    for(auto entityW : all)
    {
        auto entity = entityW.lock();
        if(filter)
        {
            if(filter(entity))
                result.push_back(entity);
        }
        else
            result.push_back(entity);
    }

    return result;
}

void GameWorld::destroyEntity(fea::EntityId id)
{
    mEntitySystem->entityRemoved(id);

    mEntities.removeEntity(id);
}

const Landscape& GameWorld::landscape() const
{
    return *mLandscape;
}

Landscape& GameWorld::landscape()
{
    return *mLandscape;
}

void GameWorld::wallSet(const glm::ivec2& position, WallType type)
{
    mBus.send(WallTileSetMessage{position, type});
}

void GameWorld::groundSet(const glm::ivec2& position, GroundType type)
{
    mBus.send(GroundTileSetMessage{position, type});
}

void GameWorld::floorSet(const glm::ivec2& position, FloorType type)
{
    mBus.send(FloorTileSetMessage{position, type});
}

void GameWorld::wallOreSet(const glm::ivec2& position, WallOre ore)
{
}

const Facility& GameWorld::facility() const
{
    return *mFacility;
}

Facility& GameWorld::facility()
{
    return *mFacility;
}

const ConstructionManager& GameWorld::constructions() const
{
    return *mConstructionManager;
}

ConstructionManager& GameWorld::constructions()
{
    return *mConstructionManager;
}

void GameWorld::constructionCreated(ConstructionType type, const glm::ivec2& position, int32_t id)
{
    mBus.send(ConstructionCreatedMessage{type, position, id});
}

void GameWorld::constructionFinished(int32_t id)
{
    mBus.send(ConstructionFinishedMessage{id});
}

const ItemManager& GameWorld::items() const
{
    return *mItemManager;
}

ItemManager& GameWorld::items()
{
    return *mItemManager;
}

void GameWorld::itemStackCreated(const ItemType& type, int32_t id, const glm::vec2& position, int32_t itemCount)
{
    mBus.send(ItemStackCreatedMessage{type.typeId, id, position, itemCount});
}

void GameWorld::itemStackUpdated(int32_t id, int32_t itemCount)
{
    mBus.send(ItemStackUpdatedMessage{id, itemCount});
}

void GameWorld::itemStackDepleted(int32_t id)
{
    mBus.send(ItemStackDepletedMessage{id});
}

RoomManager& GameWorld::rooms()
{
    return *mRoomManager;
}

const RoomManager& GameWorld::rooms() const
{
    return *mRoomManager;
}
