#pragma once
#include <fea/entitysystem.hpp>
#include <fea/util.hpp>
#include "world/landscapecallbackhandler.hpp"
#include "world/constructionmanagercallbackhandler.hpp"
#include "world/itemmanagercallbackhandler.hpp"
#include "world/worldmessages.hpp"
#include "itemtypedata.hpp"
#include "roommanager.hpp"
#include "constructiontype.hpp"
#include <util/pimpl.hpp>

class Landscape;
class EntitySystem;
class Facility;
class ConstructionManager;
class RelationshipDatabase;
class ItemManager;

class GameWorld : public LandscapeCallbackHandler, public ConstructionManagerCallbackHandler, public ItemManagerCallbackHandler
{
    public:
        struct Definitions
        {
            std::unordered_map<std::string, std::string> entityAttributes;
            std::vector<std::pair<std::string, fea::EntityTemplate>> entityTemplates;
            std::unordered_map<int32_t, ConstructionTypeData> wallConstructions;
            std::unordered_map<int32_t, ConstructionTypeData> floorConstructions;
        };

        GameWorld(fea::MessageBus& bus, Landscape landscape, Definitions definitions, std::unordered_map<int32_t, ItemTypeData> itemTypeData, std::unordered_map<int32_t, RoomTypeData> roomTypeData);
        ~GameWorld();
        void update(float deltaTime);
        //entities
        fea::EntityPtr createEntity(const std::string& entityTemplate);
        std::deque<fea::EntityPtr> entities(std::function<bool(fea::EntityPtr)> filter = std::function<bool(fea::EntityPtr)>()) const;
        void destroyEntity(fea::EntityId id);
        //landscape
        const Landscape& landscape() const;
        Landscape& landscape();
        virtual void wallSet(const glm::ivec2& position, WallType type) override;
        virtual void groundSet(const glm::ivec2& position, GroundType type) override;
        virtual void floorSet(const glm::ivec2& position, FloorType type) override;
        virtual void wallOreSet(const glm::ivec2& position, WallOre ore) override;
        //facility
        const Facility& facility() const;
        Facility& facility();
        //constructions
        const ConstructionManager& constructions() const;
        ConstructionManager& constructions();
        virtual void constructionCreated(ConstructionType type, const glm::ivec2& position, int32_t id) override;
        virtual void constructionFinished(int32_t id) override;
        //items
        const ItemManager& items() const;
        ItemManager& items();
        virtual void itemStackCreated(const ItemType& type, int32_t id, const glm::vec2& position, int32_t itemCount) override;
        virtual void itemStackUpdated(int32_t id, int32_t itemCount) override;
        virtual void itemStackDepleted(int32_t id) override;
        //rooms
        const RoomManager& rooms() const;
        RoomManager& rooms();
    private:
        fea::MessageBus& mBus;
        Pimpl<Landscape> mLandscape;

        fea::EntityManager mEntities;
        fea::EntityFactory mEntityFactory;
        Pimpl<EntitySystem> mEntitySystem;

        Pimpl<Facility> mFacility;
        Pimpl<ConstructionManager> mConstructionManager;
        Pimpl<RelationshipDatabase> mItemPropertyRelations;
        Pimpl<ItemManager> mItemManager;
        Pimpl<RoomManager> mRoomManager;
};
