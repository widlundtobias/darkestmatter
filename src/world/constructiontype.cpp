#include "constructiontype.hpp"

bool operator==(const ConstructionType& a, const ConstructionType& b)
{
    return a.type == b.type && a.category == b.category;
}
