#pragma once
#include "world/landscape.hpp"

class LandscapeCollisionChecker
{
    public:
        struct CollisionResult
        {
            glm::vec2 position;
            bool xCollided;
            bool yCollided;
            std::unordered_set<glm::ivec2> collidedTiles;
        };

        LandscapeCollisionChecker(const Landscape& landscape);
        CollisionResult evaluate(const glm::vec2& oldPosition, const glm::vec2& newPosition, float width) const;

    private:
        bool isSolidAt(const glm::ivec2& position) const;
        const Landscape& mLandscape;
};
