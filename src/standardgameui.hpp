#pragma once
#include "gameui.hpp"
#include "messages.hpp"
#include "world/worldmessages.hpp"
#include "renderingsystem/renderingmessages.hpp"

class GameController;
class ResourceStorage;

using CoordinateTransformer = std::function<glm::vec2(const glm::vec2&)>;

class StandardGameUI : public GameUI, public fea::MessageReceiver<
        MouseClickMessage,
        MouseMoveMessage,
        EntityMovedMessage
    >
{
    public:
        StandardGameUI(GameController& gameController, fea::MessageBus& bus, const ResourceStorage& resources, const glm::ivec2& size, CoordinateTransformer worldFromScreenTransformer, CoordinateTransformer screenFromWorldTransformer);
        void handleMessage(const MouseClickMessage& message) override;
        void handleMessage(const MouseMoveMessage& message) override;
        void handleMessage(const EntityMovedMessage& message) override;
        void highlightTiles(std::unordered_set<glm::ivec2> tiles, HighlightMode mode) override;
        void dehighlightTiles(std::unordered_set<glm::ivec2> tiles) override;
        void update() override;
        //mGameUI->setWallKnowledge();
        void setAvailableWalls(std::deque<std::reference_wrapper<const WallResource>> currentWallKnowledge) override;
        void resize(const glm::ivec2 newSize) override;
    private:
        void highlightTile(const glm::ivec2& tile, HighlightMode mode);
        GameController& mGameController;
        fea::MessageBus& mBus;
        const ResourceStorage& mResources;
        CoordinateTransformer mWorldFromScreenTransformer;
        CoordinateTransformer mScreenFromWorldTransformer;

        std::unordered_map<glm::ivec2, fea::Color> mOverlayTiles;

        gim::Element mUIRoot;

        //wall resources
        std::deque<std::reference_wrapper<const WallResource>> mAvailableWalls;

        //entity number displayers:
        std::unordered_map<int32_t, gim::Element*> mEntityNumbers;
};
