#include "jobsolver.hpp"

JobSolver::JobSolver(const Job& job, const AnyMap<std::string>& external):
    mJobToSolve(job),
    mExternal(external)
{
}

const Job& JobSolver::job()
{
    return mJobToSolve;
}

void JobSolver::notifySuccess(AnyMap<std::string>)
{
}

void JobSolver::notifyFail(AnyMap<std::string>)
{
}

JobSolver::UpdateResult JobSolver::fail(AnyMap<std::string> output)
{
    UpdateResult result;
    result.status = JobStatus::FAILED;
    result.output = std::move(output);
    return result;
}

JobSolver::UpdateResult JobSolver::succeed(AnyMap<std::string> output)
{
    UpdateResult result;
    result.status = JobStatus::COMPLETED;
    result.output = std::move(output);
    return result;
}

JobSolver::UpdateResult JobSolver::require(Job job)
{
    mRequiredJob = std::move(job);
    UpdateResult result;
    result.status = JobStatus::REQUIRES;
    result.requiredJob = &mRequiredJob;

    return result;
}

JobSolver::UpdateResult JobSolver::working(AnyMap<std::string> output)
{
    UpdateResult result;
    result.status = JobStatus::WORKING;
    result.output = std::move(output);

    return result;
}
