#pragma once
#include <util/anymap.hpp>

using JobData = AnyMap<std::string>;
struct Job
{
    int32_t type;
    JobData jobData;
};
