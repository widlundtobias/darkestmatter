#pragma once
#include "jobsolver.hpp"

template<typename SolverType>
std::unique_ptr<JobSolver> solverFactory(const Job& job, const AnyMap<std::string>& external)
{
    return std::make_unique<SolverType>(job, external);
}
