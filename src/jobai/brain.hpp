#pragma once
#include <deque>
#include <unordered_map>
#include <functional>
#include <memory>
#include "job.hpp"
#include "jobsolver.hpp"
#include "jobstatus.hpp"

using SolverFactoryMap = std::unordered_map<int32_t, std::function<std::unique_ptr<JobSolver>(const Job&, const AnyMap<std::string>&)>>;
using SolverStack = std::deque<std::unique_ptr<JobSolver>>;

class Brain
{
    public:
        struct UpdateResult
        {
            JobStatus status;
            AnyMap<std::string> output;
            const Job* job;
        };

        Brain(SolverFactoryMap capabilities, AnyMap<std::string> external);
        UpdateResult update();
        JobStatus status();
        bool solve(const Job& job);
        void cancel();
        const Job* currentJob(bool root);
        SolverStack retrieveState();
        void setState(SolverStack solverStack);
    private:
        SolverFactoryMap mCapabilities;
        JobStatus mCurrentStatus;
        AnyMap<std::string> mExternal;
        SolverStack mSolverStack;
};
