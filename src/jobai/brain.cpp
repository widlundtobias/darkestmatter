#include "brain.hpp"

Brain::Brain(SolverFactoryMap capabilities, AnyMap<std::string> external):
    mCapabilities(capabilities),
    mCurrentStatus(JobStatus::IDLE),
    mExternal(external)
{
}

Brain::UpdateResult Brain::update()
{
    Brain::UpdateResult result;

    if(mSolverStack.empty())
    {
        result.status = JobStatus::IDLE;
        result.job = nullptr;
        mCurrentStatus = result.status;
    }
    else
    {
        auto& topSolver = *mSolverStack.front();
        JobSolver::UpdateResult updateResult = topSolver.update();
        TH_ASSERT(updateResult.status != JobStatus::IDLE, "Solver shouldn't return IDLE state");
        TH_ASSERT(updateResult.status == JobStatus::WORKING || updateResult.status == JobStatus::COMPLETED || updateResult.status == JobStatus::FAILED || updateResult.status == JobStatus::REQUIRES, "Solver returned invalid value " << static_cast<int32_t>(updateResult.status));
        result.status = updateResult.status;
        result.job = currentJob(true);

        if(updateResult.status == JobStatus::WORKING)
        {
            result.output = std::move(updateResult.output);
        }
        else if(updateResult.status == JobStatus::REQUIRES)
        {
            TH_ASSERT(updateResult.requiredJob != nullptr, "Solver requires a job but the job required is null");
            result.status = JobStatus::WORKING;

            bool canSolve = solve(*updateResult.requiredJob);

            if(!canSolve)
                topSolver.notifyFail({{"cannot_perform", true}});
        }
        else if(updateResult.status == JobStatus::COMPLETED)
        {
            AnyMap<std::string> completedData = std::move(updateResult.output);

            mSolverStack.pop_front();

            if(!mSolverStack.empty())
            {
                result.status = JobStatus::WORKING;
                mSolverStack.front()->notifySuccess(std::move(completedData));
            }
            else
                result.output = std::move(completedData);
        }
        else if(updateResult.status == JobStatus::FAILED)
        {
            AnyMap<std::string> failedData = std::move(updateResult.output);

            mSolverStack.pop_front();

            if(!mSolverStack.empty())
            {
                result.status = JobStatus::WORKING;
                mSolverStack.front()->notifyFail(std::move(failedData));
            }
            else
                result.output = std::move(failedData);
        }

        mCurrentStatus = result.status;
    }

    return result;
}

JobStatus Brain::status()
{
    return mCurrentStatus;
}

bool Brain::solve(const Job& job)
{
    auto solverFactoryIter = mCapabilities.find(job.type);

    if(solverFactoryIter != mCapabilities.end())
    {
        auto solver = solverFactoryIter->second(job, mExternal);
        mSolverStack.emplace_front(std::move(solver));
        return true;
    }

    return false;
}

void Brain::cancel()
{
    if(!mSolverStack.empty())
        mSolverStack.clear();
}

const Job* Brain::currentJob(bool root)
{
    if(!mSolverStack.empty())
        return root ? &mSolverStack.back()->job() : &mSolverStack.front()->job();
    else
        return nullptr;
}

SolverStack Brain::retrieveState()
{
    mCurrentStatus = JobStatus::IDLE;
    return std::move(mSolverStack);
}

void Brain::setState(SolverStack solverStack)
{
    mSolverStack = std::move(solverStack);
}
