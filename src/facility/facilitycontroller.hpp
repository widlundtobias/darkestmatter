#pragma once
#include "facility/workstatus.hpp"

class FacilityController
{
    public:
        virtual void jobCreated(int32_t id, const Job& job) = 0;
        virtual void jobUpdated(int32_t id, WorkStatus status, const Job& job) = 0;
};
