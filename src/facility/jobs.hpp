#pragma once
#include <thero/smartenum.hpp>

namespace Jobs
{
    smart_enum(JobType,
            //routines
            STORAGE_KEEPING,
            //jobs
            CONSTRUCT, MINE_WALL,
            CLEAR_TILES, EVADE_TILES,
            GOTO,
            FIND_AND_FETCH,
            FETCH_FROM_STACK, FETCH_FROM_CONTAINER,
            FETCH_ITEM,
            PUT_ITEM_IN_CONTAINER, PUT_ITEM_ON_STACK,
            PUT_ITEM,
            FROM_STACK_TO_CONTAINER,
            FROM_STACK_TO_STACK,
            FROM_CONTAINER_TO_CONTAINER,
            FROM_CONTAINER_TO_STACK,
            MOVE_ITEM,
            STORE_ITEM,
            )
}
