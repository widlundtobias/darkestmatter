#pragma once
#include "workstatus.hpp"

struct Job;

class JobBoardCallbackHandler
{
    public:
        virtual void jobAdded(int32_t newId, const Job& job) = 0;
        virtual void jobUpdated(int32_t id, WorkStatus status, const Job& job) = 0;
};
