#include "facility.hpp"
#include "facilitycontroller.hpp"
#include "world/gameworld.hpp"

Facility::Facility(fea::MessageBus& bus):
    mBus(bus),
    mFacilityController(nullptr),
    mJobBoard(this),
    mRoutineBoard(this)
{
}

void Facility::setFacilityController(FacilityController& facilityController)
{
    mFacilityController = &facilityController;
}

const JobBoard& Facility::jobs() const
{
    return mJobBoard;
}

JobBoard& Facility::jobs()
{
    return mJobBoard;
}

void Facility::jobAdded(int32_t newId, const Job& job)
{
    FEA_ASSERT(
            job.type == Jobs::CONSTRUCT ||
            job.type == Jobs::MINE_WALL
            , "Routine board given '" << job.type << "' which is not a valid job order");

    std::stringstream extraMessage;

    if(job.type == Jobs::CONSTRUCT)
    {
        FEA_ASSERT(job.jobData.count("position") != 0 && job.jobData.at("position").isOfType<glm::ivec2>(), "Job '" << to_string((Jobs::JobType)job.type) << "' must have 'position' as glm::ivec2");
        FEA_ASSERT(mJobBoard.find<glm::ivec2>(job.type, "position", job.jobData.at("position").get<glm::ivec2>()).size() == 1, "Cannot add more than one construction job in the same spot");
        extraMessage << " at " << job.jobData.at("position").get<glm::ivec2>(); 
    }

    LOG_V("New job.type '" << newId << "' of type '" << to_string((Jobs::JobType)job.type) << extraMessage.str());

    if(mFacilityController)
        mFacilityController->jobCreated(newId, job);
}

void Facility::jobUpdated(int32_t id, WorkStatus status, const Job& job)
{
    if(mFacilityController)
        mFacilityController->jobUpdated(id, status, job);
}

const RoutineBoard& Facility::routines() const
{
    return mRoutineBoard;
}

RoutineBoard& Facility::routines()
{
    return mRoutineBoard;
}

void Facility::routineAdded(int32_t newId, const Job& job)
{
    FEA_ASSERT(job.type == Jobs::STORAGE_KEEPING, "Routine board given '" << job.type << "' which is not a recognised routine");
}

void Facility::routineUpdated(int32_t id, RoutineStatus status, const Job& routine)
{
}
