#pragma once
#include "precompiled.h"

smart_enum_class(WorkStatus, WAITING, IN_PROGRESS, COMPLETED, CANCELLED)
