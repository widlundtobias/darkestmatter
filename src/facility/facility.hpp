#pragma once
#include "jobai/job.hpp"
#include "jobboard.hpp"
#include "routineboard.hpp"
#include "facility/jobs.hpp"
#include "jobboardcallbackhandler.hpp"
#include "routineboardcallbackhandler.hpp"

class FacilityController;

class Facility : public JobBoardCallbackHandler, public RoutineBoardCallbackHandler
{
    public:
        Facility(fea::MessageBus& bus);
        void setFacilityController(FacilityController& facilityController);
        //jobs
        const JobBoard& jobs() const;
        JobBoard& jobs();
        virtual void jobUpdated(int32_t id, WorkStatus status, const Job& job) override;
        virtual void jobAdded(int32_t newId, const Job& job) override;
        //routines
        const RoutineBoard& routines() const;
        RoutineBoard& routines();
        virtual void routineAdded(int32_t newId, const Job& job) override;
        virtual void routineUpdated(int32_t id, RoutineStatus status, const Job& routine) override;
    private:
        fea::MessageBus& mBus;
        FacilityController* mFacilityController;
        JobBoard mJobBoard;
        RoutineBoard mRoutineBoard;
};
