#pragma once
#include "routinestatus.hpp"

struct Job;

class RoutineBoardCallbackHandler
{
    public:
        virtual void routineAdded(int32_t newId, const Job& job) = 0;
        virtual void routineUpdated(int32_t id, RoutineStatus status, const Job& job) = 0;
};
