#include "routineboard.hpp"
#include "jobcategory.hpp"
#include "jobs.hpp"
#include "routineboardcallbackhandler.hpp"

RoutineBoard::RoutineBoard(RoutineBoardCallbackHandler* callbackHandler):
    mCallbackHandler(callbackHandler)
{
}

int32_t RoutineBoard::add(Job job)
{
    int32_t newId = mJobIdPool.next();
    job.jobData["id"] = newId;
    job.jobData["job_category"] = JobCategory::ROUTINE;

    auto emplaced = mJobs.emplace(newId, JobEntry{std::make_unique<Job>(std::move(job)), RoutineStatus::ENABLED});

    if(mCallbackHandler)
        mCallbackHandler->routineAdded(newId, *emplaced.first->second.job);

    return newId;
}

std::deque<RoutineBoard::JobView> RoutineBoard::find(bool onlyEnabled) const
{
    std::deque<JobView> result;
    
    for(const auto& jobEntryIter : mJobs)
    {
        const auto& jobEntry = jobEntryIter.second;

        if(onlyEnabled ? (jobEntry.status == RoutineStatus::ENABLED) : true) // only waiting jobs if true
        {
            result.emplace_back(JobView{jobEntryIter.first, *jobEntry.job, jobEntry.status});
        }
    }

    return result;
}
