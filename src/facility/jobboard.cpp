#include "jobboard.hpp"
#include "jobcategory.hpp"
#include "jobs.hpp"
#include "jobboardcallbackhandler.hpp"

JobBoard::JobBoard(JobBoardCallbackHandler* callbackHandler):
    mCallbackHandler(callbackHandler)
{
}

int32_t JobBoard::add(Job job)
{
    int32_t newId = mJobIdPool.next();
    mJobIndex[job.type].emplace_back(newId);
    job.jobData["id"] = newId;
    job.jobData["job_category"] = JobCategory::JOB;
    auto emplaced = mJobs.emplace(newId, JobEntry{std::make_unique<Job>(std::move(job)), WorkStatus::WAITING});

    if(mCallbackHandler)
        mCallbackHandler->jobAdded(newId, *emplaced.first->second.job);

    return newId;
}

std::deque<JobBoard::JobView> JobBoard::find(bool onlyWaiting) const
{
    std::deque<JobView> result;
    
    for(auto jobBoard : mJobIndex)
    {
        for(const auto& jobId : jobBoard.second)
        {
            const auto& jobEntry = mJobs.at(jobId);

            if(onlyWaiting ? (jobEntry.status == WorkStatus::WAITING) : true) // only waiting jobs if true
            {
                result.emplace_back(JobView{jobId, *jobEntry.job, jobEntry.status});
            }
        }
    }

    return result;
}

std::deque<JobBoard::JobView> JobBoard::find(int32_t type, bool onlyWaiting) const
{
    std::deque<JobView> result;

    auto listIterator = mJobIndex.find(type);

    if(listIterator != mJobIndex.end())
    {
        for(const auto& jobId : listIterator->second)
        {
            const auto& jobEntry = mJobs.at(jobId);

            if(onlyWaiting ? (jobEntry.status == WorkStatus::WAITING) : true) // only waiting jobs if true
            {
                result.emplace_back(JobView{jobId, *jobEntry.job, jobEntry.status});
            }
        }
    }

    return result;
}

void JobBoard::update(int32_t id, WorkStatus newStatus)
{
    FEA_ASSERT(mJobs.count(id) != 0, "Cannot update job " << id << " since it doesn't exist\n");
    
    int32_t type = mJobs.at(id).job->type;

    if(newStatus == WorkStatus::WAITING || newStatus == WorkStatus::IN_PROGRESS)
    {
        mJobs.at(id).status = newStatus;

        if(mCallbackHandler)
            mCallbackHandler->jobUpdated(id, newStatus, *mJobs.at(id).job);
    }
    else
    {
        auto jobIter = mJobs.find(id);

        Job deletedJob = std::move(*jobIter->second.job);

        mJobs.erase(jobIter);

        auto& jobList = mJobIndex.at(type);

        bool deleted = false;
        for(auto iter = jobList.begin(); iter != jobList.end(); ++iter)
        {
            if(*iter == id)
            {
                jobList.erase(iter);
                deleted = true;
                break;
            }
        }

        FEA_ASSERT(deleted, "Woops, job got deleted from list but not from index. Id '" << id << "'");

        if(mCallbackHandler)
            mCallbackHandler->jobUpdated(id, newStatus, deletedJob);
    }
}
