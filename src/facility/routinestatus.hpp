#pragma once

enum class RoutineStatus { ENABLED, DISABLED };
