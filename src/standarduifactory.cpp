#include "standarduifactory.hpp"
#include "gimgui/feacoloradaptor.hpp"
#include "gimgui/glmrectangleadaptor.hpp"
#include "gimgui/glmvec2adaptor.hpp"
#include "resources/resourcestorage.hpp"
#include "world/tiles.hpp"
#include "gimgui/edgelayout.hpp"
#include "gimgui/gridlayout.hpp"

const glm::ivec2 textureCoordinateSpacing = glm::ivec2(5*2, 4*2) * HalfTiles::size();

gim::Element StandardUIFactory::create(const glm::ivec2& size)
{
    gim::Element ui = {
        {"root"},
        {//attributes
            {"position", glm::ivec2(0, 0) },
            {"size", size},
            {"layout", EdgeLayout()},
            //{"image_coords", Rectangle{{0, 0}, {32, 32}}},
            //{"color", fea::Color(51, 51, 51, 200)},
            //{"texture", std::string("white")},
            //{"text", std::string("Idle")},
            //{"text_color", fea::Color(237, 237, 237)},
            //{"text_size", 25},
            //{"font", mFont.name()},
        },  
        {//children
            {
                {"hammer", "action_button"},
                {//attributes
                    {"position", glm::ivec2(28, 500)},
                    {"size", glm::ivec2(64, 64)},
                    {"image_coords", Rectangle{{0, 0}, {64, 64}}},
                    {"color", fea::Color(200, 200, 200)},
                    {"hover_color", fea::Color(255, 255, 255)},
                    {"texture", std::string("hammericon")},
                    {"block_event", true},

                    {"layout_data", EdgeLayoutData
                        {
                            EdgeLayout::BOTTOM,
                            0,
                            {},
                            {32, -32},
                        }
                    },
                },
                {//children
                    {
                        {"wall", "action_button"},
                        {//attributes
                            {"position", glm::ivec2(0, -92)},
                            {"size", glm::ivec2(64, 64)},
                            {"image_coords", Rectangle{{0, 0}, {64, 64}}},
                            {"color", fea::Color(200, 200, 200)},
                            {"hover_color", fea::Color(255, 255, 255)},
                            {"texture", std::string("wallicon")},
                            {"hide", true},
                            {"block_event", true},
                        },
                    },
                    {
                        {"floor", "action_button"},
                        {//attributes
                            {"position", glm::ivec2(92, -92)},
                            {"size", glm::ivec2(64, 64)},
                            {"image_coords", Rectangle{{0, 0}, {64, 64}}},
                            {"color", fea::Color(200, 200, 200)},
                            {"hover_color", fea::Color(255, 255, 255)},
                            {"texture", std::string("flooricon")},
                            {"hide", true},
                            {"block_event", true},
                        },
                    },
                },
            },
            {
                {"selection_window"},
                {//attributes
                    {"position", glm::ivec2(280, 165) },
                    {"size", glm::ivec2(276, 652) },
                    {"image_coords", Rectangle{{0, 0}, {32, 32}}},
                    {"color", fea::Color(51, 51, 51, 230)},
                    {"texture", std::string("white")},
                    {"block_event", true},
                    {"hide", true},

                    {"layout", GridLayout{1, 100, GridLayout::COLUMNS}},

                    {"layout_data", EdgeLayoutData
                        {
                            EdgeLayout::LEFT,
                            200,
                            {560},
                            {32, -64},
                        }
                    },
                    //{"text", std::string("Idle")},
                    //{"text_color", fea::Color(237, 237, 237)},
                    //{"text_size", 25},
                    //{"font", mFont.name()},
                },
                {//children
                },
            },
        },
    };

    gim::forEach(ui, [] (gim::Element& element)
    {
        auto ensureAttribute = [] (gim::Element& elementToAddTo, const std::string& name, auto&& value)
        {
            GIM_ASSERT(!elementToAddTo.hasAttribute(name) || elementToAddTo.template hasAttribute<decltype(value)>(name), "Attribute " << name << " already exists of another type");

            if(!elementToAddTo.hasAttribute<decltype(value)>(name))
            {
                elementToAddTo.createAttribute(name, value);
            }
        };

        ensureAttribute(element, "position", glm::ivec2(0,0));
        ensureAttribute(element, "size", glm::ivec2(0,0));
        ensureAttribute(element, "hide", false);
        ensureAttribute(element, "color", fea::Color::White);
        ensureAttribute(element, "default_color", element.getAttribute<fea::Color>("color"));
        ensureAttribute(element, "hover_color", fea::Color::White);

        return false;
    });

    return ui;
}

gim::Element StandardUIFactory::wallSelection(const WallResource& wall, const ResourceStorage& resources)
{
    gim::Element toReturn = {
        {"wall_selection"},
            {//attributes
                {"position", glm::ivec2(0, 0) },
                {"size", glm::ivec2(32, 32) },
                //{"text", std::string("Idle")},
                //{"text_color", fea::Color(237, 237, 237)},
                //{"text_size", 25},
                //{"font", mFont.name()},
            },
            {//children
                {
                    {"name"},
                    {//attributes
                        {"position", glm::ivec2(0, 0) },
                        {"size", glm::ivec2(200, 32) },
                        {"text", wall.name},
                        {"text_color", fea::Color(237, 237, 237)},
                        {"text_size", 16},
                        {"font", std::string("Ubuntu Mono Regular")},
                    },
                    {//children
                    },
                },
                wallImage(wall, {0, 25}),
                {
                    {"description"},
                    {//attributes
                        {"position", glm::ivec2(54, 25) },
                        {"size", glm::ivec2(210, 32) },
                        {"text", wall.description},
                        {"text_color", fea::Color(237, 237, 237)},
                        {"text_size", 14},
                        {"font", std::string("Ubuntu Mono Regular")},
                    },
                    {//children
                    },
                },
                {
                    {"materials"},
                    {//attributes
                        {"position", glm::ivec2(0, 0) },
                        {"size", glm::ivec2(210, 32) },
                    },
                },
            },
    };

    int32_t counter = 0;
    for(const auto& requirement : wall.materialsNeeded)
    {
        toReturn.findChildren({"materials"})[0]->append(materialRequirement(requirement, {counter * 50, 75}, resources));
        ++counter;
    }

    return toReturn;
}

gim::Element StandardUIFactory::wallImage(const WallResource& wall, const glm::ivec2& offset)
{
    return {
        {"wall_image"},
            {//attributes
                {"position", glm::ivec2(0, 0) + offset},
                {"size", glm::ivec2(48, 48) },
                //{"text", std::string("Idle")},
                //{"text_color", fea::Color(237, 237, 237)},
                //{"text_size", 25},
                //{"font", mFont.name()},
            },
            {//children
                {
                    {""},
                    {//attributes
                        {"position", glm::ivec2(0, 0) },
                        {"size", glm::ivec2(32, 32) },
                        {"image_coords", Rectangle{wall.textureCoordinates * textureCoordinateSpacing, {32, 32}}},
                        {"color", fea::Color::White},
                        {"texture", wall.texture},
                        //{"text", std::string("Idle")},
                        //{"text_color", fea::Color(237, 237, 237)},
                        //{"text_size", 25},
                        //{"font", mFont.name()},
                    },
                    {//children
                    },
                },
                {
                    {""},
                    {//attributes
                        {"position", glm::ivec2(32, 0) },
                        {"size", glm::ivec2(16, 32) },
                        {"image_coords", Rectangle{wall.textureCoordinates * textureCoordinateSpacing + glm::ivec2(64, 0), {16, 32}}},
                        {"color", fea::Color::White},
                        {"texture", wall.texture},
                        //{"text", std::string("Idle")},
                        //{"text_color", fea::Color(237, 237, 237)},
                        //{"text_size", 25},
                        //{"font", mFont.name()},
                    },
                    {//children
                    },
                },
                {
                    {""},
                    {//attributes
                        {"position", glm::ivec2(0, 32) },
                        {"size", glm::ivec2(32, 16) },
                        {"image_coords", Rectangle{wall.textureCoordinates * textureCoordinateSpacing + glm::ivec2(0, 48), {32, 16}}},
                        {"color", fea::Color::White},
                        {"texture", wall.texture},
                        //{"text", std::string("Idle")},
                        //{"text_color", fea::Color(237, 237, 237)},
                        //{"text_size", 25},
                        //{"font", mFont.name()},
                    },
                    {//children
                    },
                },
                {
                    {""},
                    {//attributes
                        {"position", glm::ivec2(32, 32) },
                        {"size", glm::ivec2(16, 16) },
                        {"image_coords", Rectangle{wall.textureCoordinates * textureCoordinateSpacing + glm::ivec2(64, 48), {16, 16}}},
                        {"color", fea::Color::White},
                        {"texture", wall.texture},
                        //{"text", std::string("Idle")},
                        //{"text_color", fea::Color(237, 237, 237)},
                        //{"text_size", 25},
                        //{"font", mFont.name()},
                    },
                    {//children
                    },
                },
            },
    };
}

gim::Element StandardUIFactory::materialRequirement(const WallResource::MaterialRequirement& requirement, const glm::ivec2& offset, const ResourceStorage& resources)
{
    std::string materialTexture = resources.items().at(resources.itemIds().at(requirement.materialName)).gfx.texture;

    return {
        {"material_requirement"},
            {//attributes
                {"position", glm::ivec2(0, 0) + offset},
                {"size", glm::ivec2(48, 48) },
                //{"text", std::string("Idle")},
                //{"text_color", fea::Color(237, 237, 237)},
                //{"text_size", 25},
                //{"font", mFont.name()},
            },
            {//children
                {
                    {"material_image"},
                    {//attributes
                        {"position", glm::ivec2(0, 0) },
                        {"size", glm::ivec2(16, 16) },
                        {"image_coords", Rectangle{{0, 0}, {16, 16}}},
                        {"color", fea::Color::White},
                        {"texture", materialTexture},
                        //{"text", std::string("Idle")},
                        //{"text_color", fea::Color(237, 237, 237)},
                        //{"text_size", 25},
                        //{"font", mFont.name()},
                    },
                    {//children
                    },
                },
                {
                    {"amount"},
                    {//attributes
                        {"position", glm::ivec2(16, 0) },
                        {"size", glm::ivec2(16, 16) },
                        {"text", std::string("x" + std::to_string(requirement.amount))},
                        {"text_color", fea::Color(237, 237, 237)},
                        {"text_size", 14},
                        {"font", std::string("Ubuntu Mono Regular")},
                    },
                    {//children
                    },
                },
            },
    };
}
