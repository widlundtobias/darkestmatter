#define GLM_FORCE_RADIANS
#include "darkestmatter.hpp"

int main(int argc, char *argv[])
{
    DarkestMatter darkestMatter;
    darkestMatter.run(argc, argv);
}
