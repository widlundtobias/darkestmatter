#pragma once
#include "world/landscape.hpp"

class LandscapeGenerator
{
    public:
        struct Settings
        {
            int32_t wallType;
            int32_t wallOreType;
            int32_t groundType;

            struct WallOreSettings
            {
                int32_t lowAmount;
                int32_t midAmount;
                int32_t higAmount;
            } wallOreSettings;
        };

        LandscapeGenerator(Settings settings);
        Landscape generate(const glm::ivec2& size) const;

    private:
        Settings mSettings;
};
