#include "landscapegenerator.hpp"

LandscapeGenerator::LandscapeGenerator(Settings settings):
    mSettings(settings)
{
}

Landscape LandscapeGenerator::generate(const glm::ivec2& size) const
{
    Landscape result(size);

    fea::Noise noise;

    for(int32_t y = 0; y < size.y; y++)
    {
        for(int32_t x = 0; x < size.x; x++)
        {
            float value = noise.simplexOctave2D(static_cast<float>(x), static_cast<float>(y), 0.025f, 3u, 0.8f);

            result.setGround({x, y}, mSettings.groundType);
            result.setWall({x, y}, value > 0.2f ? mSettings.wallType : 0);

            int32_t amount = 0;
            if(value > 0.3f)
            {
                value -= 0.3f;
                value *= 2.f; //max value is 0.8f...

                amount = static_cast<int32_t>(mSettings.wallOreSettings.higAmount*static_cast<int32_t>(value));
            }

            result.setWallOre({x, y}, {amount ? mSettings.wallOreType : 0, amount});
        }
    }

    return result;
}
