#pragma once

namespace gim
{
    class Element;
}

struct EdgeLayout;
struct GridLayout;

class Layouter
{
    public:
        static void applyLayout(gim::Element& element, bool recursively);
    private:
        static void applyEdgeLayout(gim::Element& element, const EdgeLayout& edgeLayout);
        static void applyGridLayout(gim::Element& element, const GridLayout& gridLayout);
};
