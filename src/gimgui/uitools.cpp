#include "uitools.hpp"

void UiTools::addCallback(gim::Element& element, const std::string& name, Callback callback)
{
    bool hasSingleCallback = element.hasAttribute<Callback>(name);
    bool hasMultipleCallbacks = element.hasAttribute<CallbackList>(name);

    if(hasSingleCallback)
    {
        Callback oldCallback = element.getAttribute<Callback>(name);
        element.deleteAttribute(name);
        element.createAttribute(name, CallbackList{oldCallback, callback});
    }
    else if(hasMultipleCallbacks)
    {
        element.getAttribute<CallbackList>(name).emplace_back(callback);
    }
    else
    {
        element.createAttribute(name, CallbackList{callback});
    }
} 

void UiTools::toggleVisibility(gim::Element& element)
{
    bool currentlyHidden = element.getAttribute<bool>("hide");

    element.setAttribute("hide", !currentlyHidden);
}

void UiTools::toggleChildrenVisibility(gim::Element& element)
{
    auto& childList = element.children();

    if(!childList.empty())
    {
        bool currentlyHidden = childList[0]->getAttribute<bool>("hide");

        for(auto& child : childList)
            child->setAttribute("hide", !currentlyHidden);
    }
}

void UiTools::setOnHoverColor(gim::Element& element, const Parameters& params)
{
   TH_ASSERT(element.hasAttribute<fea::Color>("hover_color"), "hover_color not specified"); 

   element.setAttribute("color", element.getAttribute<fea::Color>("hover_color"));
}

void UiTools::setDefaultColor(gim::Element& element, const Parameters& params)
{
   TH_ASSERT(element.hasAttribute<fea::Color>("default_color"), "hover_color not specified"); 

   element.setAttribute("color", element.getAttribute<fea::Color>("default_color"));
}
