#include "precompiled.h"

struct GridLayout
{
    enum Order{ COLUMNS, ROWS };
    int32_t wrapSize;
    int32_t wrapDistance;
    Order order;
};
