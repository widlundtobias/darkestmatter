#include "glmrectangleadaptor.hpp"

RectangleAdaptor::RectangleAdaptor()
{       
}       

RectangleAdaptor::RectangleAdaptor(const Vec2Adaptor& start, const Vec2Adaptor& size):
    mRectangle{glm::ivec2{start.x(), start.y()}, glm::ivec2{size.x(), size.y()}}
{
}
    
RectangleAdaptor::RectangleAdaptor(const Rectangle& vec2):
    mRectangle(vec2)
{
}
    
Vec2Adaptor RectangleAdaptor::start() const
{
    return Vec2Adaptor(mRectangle.start);
}

Vec2Adaptor RectangleAdaptor::size() const
{
    return Vec2Adaptor(mRectangle.size);
}
