#pragma once
#include <unordered_map>
#include <vector>
#include <fea/userinterface.hpp>
#include <gimgui/data/variant.hpp>
#include <gimgui/data/element.hpp>
#include <string>
#include <glm/glm.hpp>

using Parameters = std::unordered_map<std::string, gim::Variant>;

using Callback = std::function<void(gim::Element&, const Parameters&)>;
using CallbackList = std::vector<Callback>;

class CallbackExecutor
{
    public:
        CallbackExecutor(const std::string& attributeName):
            mAttributeName(attributeName)
        {
        }
        void execute(gim::Element& element, const Parameters& parameters)
        {
            if(auto* callback = element.findAttribute<Callback>(mAttributeName))
            {
                (*callback)(element, parameters);
            }
            else if(auto* callbackList = element.findAttribute<CallbackList>(mAttributeName))
            {
                for(auto innerCallback : *callbackList)
                {
                    innerCallback(element, parameters);
                }
            }
        }
    private:
        std::string mAttributeName;
};

//goes to the element which was actually clicked
bool mouseClicked(gim::Element& element, const glm::ivec2& position, fea::Mouse::Button button);
//goes to all elements, notifying that a mouse button was released
void mouseReleased(gim::Element& element, const glm::ivec2& position, fea::Mouse::Button button);
void mouseMoved(gim::Element& element, const glm::ivec2& currentPosition, const glm::ivec2& lastPosition);
