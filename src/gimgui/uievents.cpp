#include "uievents.hpp"
#include <gimgui/logic/foreach.hpp>
#include <algorithm>

bool overlapsPoint(const gim::Element& element, const glm::ivec2& absolutePosition, const glm::ivec2& point)
{
    GIM_ASSERT(element.hasAttribute<glm::ivec2>("size"), "overlapsPoint was given element which doesn't have the attribute 'size'");
    const glm::ivec2& size = element.getAttribute<glm::ivec2>("size");

    return  point.x > absolutePosition.x &&
            point.x < absolutePosition.x + size.x &&
            point.y > absolutePosition.y &&
            point.y < absolutePosition.y + size.y;
}  

bool mouseClicked(gim::Element& element, const glm::ivec2& position, fea::Mouse::Button button)
{
    bool* hidden = element.findAttribute<bool>("hide");

    if(hidden && *hidden)
        return false;

    std::vector<std::pair<std::reference_wrapper<gim::Element>, glm::ivec2>> clickedElements;
    std::vector<std::pair<std::reference_wrapper<gim::Element>, glm::ivec2>> allElements;

    gim::forEach(element, [&position, &clickedElements, &allElements] (gim::Element& currentElement, const glm::ivec2& absolutePosition)
    {
        bool* currentElementHidden = currentElement.findAttribute<bool>("hide");

        if(currentElementHidden && *currentElementHidden)
            return false;

        bool overlaps = overlapsPoint(currentElement, absolutePosition, position);
        
        allElements.emplace_back(currentElement, absolutePosition);   

        if(overlaps)
            clickedElements.emplace_back(currentElement, absolutePosition);   

        return false;
    },
    [] (const gim::Element& currentElement, const glm::ivec2& parentPosition)
    {
        const glm::ivec2* positionPtr = currentElement.findAttribute<glm::ivec2>("position");

        GIM_ASSERT(positionPtr != nullptr, "Element lacks position value. Cannot be rendered.");
        
        return parentPosition + *positionPtr;
    });

    std::reverse(clickedElements.begin(), clickedElements.end());
    std::reverse(allElements.begin(), allElements.end());

    std::vector<std::reference_wrapper<gim::Element>> toDrag;

    bool blocked = false;

    for(auto&& currentElementIter : clickedElements)
    {
        auto&& currentElement = currentElementIter.first;
        glm::ivec2 absolutePosition = currentElementIter.second;

        CallbackExecutor executor("on_click");
        executor.execute(currentElement, {{"position", position},
                                           {"relative_position", glm::ivec2({position.x - absolutePosition.x, position.y - absolutePosition.y})},
                                           {"button", button}});

        toDrag.push_back(currentElement);

        const bool* blocksPtr = currentElement.get().findAttribute<bool>("block_event");
        if(blocksPtr && *blocksPtr)
        {
            blocked = true;
            break;
        }
    }

    for(auto&& currentElementIter : allElements)
    {
        auto&& currentElement = currentElementIter.first;
        glm::ivec2 absolutePosition = currentElementIter.second;
        CallbackExecutor executor("on_global_click");
        executor.execute(currentElement, {{"position", position},
                                           {"relative_position", glm::ivec2({position.x - absolutePosition.x, position.y - absolutePosition.y})},
                                           {"button", button}});
    }

    //for(auto currentElement : toDrag)
    //{
    //    //int32_t oldDrag = currentElement.get().getAttribute<int32_t>("dragged");
    //    //currentElement.get().setAttribute("dragged", oldDrag | button);
    //}

	return blocked;
}

void mouseReleased(gim::Element& element, const glm::ivec2& position, fea::Mouse::Button button)
{
    std::vector<std::pair<std::reference_wrapper<gim::Element>, glm::ivec2>> releasedElements;
    std::vector<std::pair<std::reference_wrapper<gim::Element>, glm::ivec2>> allElements;

    gim::forEach(element, [&position, &releasedElements, &allElements] (gim::Element& currentElement, const glm::ivec2& absolutePosition)
    {
        bool overlaps = overlapsPoint(currentElement, absolutePosition, position);
        
        allElements.emplace_back(currentElement, absolutePosition);   

        if(overlaps)
            releasedElements.emplace_back(currentElement, absolutePosition);   

        return false;
    },
    [] (const gim::Element& currentElement, const glm::ivec2& parentPosition)
    {
        const glm::ivec2* positionPtr = currentElement.findAttribute<glm::ivec2>("position");

        GIM_ASSERT(positionPtr != nullptr, "Element lacks position value. Cannot be rendered.");
        
        return parentPosition + *positionPtr;
    });

    std::reverse(releasedElements.begin(), releasedElements.end());
    std::reverse(allElements.begin(), allElements.end());

    std::vector<std::reference_wrapper<gim::Element>> toUnDrag;

    for(auto&& currentElementIter : releasedElements)
    {
        auto&& currentElement = currentElementIter.first;
        glm::ivec2 absolutePosition = currentElementIter.second;

        CallbackExecutor executor("on_release");
        executor.execute(currentElement, {{"position", position},
                                           {"relative_position", glm::ivec2({position.x - absolutePosition.x, position.y - absolutePosition.y})},
                                           {"button", button}});
        toUnDrag.push_back(currentElement);

        const bool* blocksPtr = currentElement.get().findAttribute<bool>("block_event");
        if(blocksPtr && *blocksPtr)
            break;
    }

    for(auto&& currentElementIter : allElements)
    {
        auto&& currentElement = currentElementIter.first;
        glm::ivec2 absolutePosition = currentElementIter.second;

        CallbackExecutor executor("on_global_release");
        executor.execute(currentElement, {{"position", position},
                                           {"relative_position", glm::ivec2({position.x - absolutePosition.x, position.y - absolutePosition.y})},
                                           {"button", button}});
    }

    //for(auto currentElement : toUnDrag)
    //{
    //    //int32_t oldDrag = currentElement.get().getAttribute<int32_t>("dragged");
    //    //currentElement.get().setAttribute("dragged", oldDrag & (~button));
    //}
}

void mouseMoved(gim::Element& element, const glm::ivec2& currentPosition, const glm::ivec2& lastPosition)
{
    std::vector<std::pair<std::reference_wrapper<gim::Element>, glm::ivec2>> movedElements;

    gim::forEach(element, [&currentPosition, &lastPosition, &movedElements] (gim::Element& currentElement, const glm::ivec2& absolutePosition)
    {
        bool overlaps = overlapsPoint(currentElement, absolutePosition, currentPosition) || overlapsPoint(currentElement, absolutePosition, lastPosition);
        
        if(overlaps)
            movedElements.emplace_back(currentElement, absolutePosition);   

        return false;
    },
    [] (const gim::Element& currentElement, const glm::ivec2& parentPosition)
    {
        const glm::ivec2* positionPtr = currentElement.findAttribute<glm::ivec2>("position");

        GIM_ASSERT(positionPtr != nullptr, "Element lacks position value. Cannot be rendered.");
        
        return parentPosition + *positionPtr;
    });

    std::reverse(movedElements.begin(), movedElements.end());

    auto overlaps = [] (const glm::ivec2& posA, const glm::ivec2& posB, const glm::ivec2& size) 
    {
        return posA.x > posB.x &&
            posA.x < posB.x + size.x &&
            posA.y > posB.y &&
            posA.y < posB.y + size.y;
    };

    glm::ivec2 delta;
    delta.x = currentPosition.x - lastPosition.x;
    delta.y = currentPosition.y - lastPosition.y;

    for(auto&& currentElementIter : movedElements)
    {
        auto&& currentElement = currentElementIter.first.get();
        glm::ivec2 absolutePosition = currentElementIter.second;

        //if(currentElement.getAttribute<int32_t>("dragged"))
        //{//got dragged
        //    CallbackExecutor executor("on_drag");
        //    executor.execute(currentElement, {{"position", currentPosition},
        //            {"delta", delta}});
        //}

        glm::ivec2 currentElementSize = currentElement.getAttribute<glm::ivec2>("size");

        bool currentPosOverlaps = overlaps(currentPosition, absolutePosition, currentElementSize);
        bool lastPosOverlaps = overlaps(lastPosition, absolutePosition, currentElementSize);

        glm::ivec2 relativePosition;
        relativePosition.x = currentPosition.x - absolutePosition.x;
        relativePosition.y = currentPosition.y - absolutePosition.y;

        currentElementSize = currentElement.getAttribute<glm::ivec2>("size");

        if(currentPosOverlaps && !lastPosOverlaps)
        {//got hovered
            CallbackExecutor executor("on_hover");
            executor.execute(currentElement, {{"position", currentPosition},
                    {"delta", delta}});
        }
        else if(lastPosOverlaps && !currentPosOverlaps)
        {//got blurred
            CallbackExecutor executor("on_blur");
            executor.execute(currentElement, {{"position", currentPosition},
                    {"delta", delta}});
            //currentElement.setAttribute("dragged", 0);
        }
    }
}
