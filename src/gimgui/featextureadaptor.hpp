#pragma once
#include <fea/render2d.hpp>
#include <glm/glm.hpp>
#include <gimgui/data/bitmap.hpp>

class FeaTextureAdaptor
{
    public:
        FeaTextureAdaptor(fea::Texture& texture);
        void initialize(uint32_t width, uint32_t height);
        void resize(uint32_t width, uint32_t height);
        void writeBitmap(uint32_t x, uint32_t y, const gim::BitMap& bitMap);
        glm::ivec2 size() const;
        uint32_t handle() const;
    private:
        fea::Texture& mTexture;
};
