#pragma once
#include <fea/rendering/color.hpp>

class ColorAdaptor
{       
    public:
        using Native = fea::Color;
        ColorAdaptor();
        ColorAdaptor(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
        ColorAdaptor(const fea::Color& color);
        uint8_t r() const;
        uint8_t g() const;
        uint8_t b() const;
        uint8_t a() const;
    private:
        fea::Color mColor;
};
