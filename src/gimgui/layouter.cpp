#include "layouter.hpp"
#include "edgelayout.hpp"
#include "gridlayout.hpp"
#include "uitools.hpp"

void Layouter::applyLayout(gim::Element& element, bool recursively)
{
    const EdgeLayout* edgeLayout = element.findAttribute<EdgeLayout>("layout");
    const GridLayout* gridLayout = element.findAttribute<GridLayout>("layout");

    if(edgeLayout)
        applyEdgeLayout(element, *edgeLayout);
    else if(gridLayout)
        applyGridLayout(element, *gridLayout);
    else if(element.hasAttribute("layout"))
    {
        TH_ASSERT(false, "Unsupported layout type");
    }

    if(recursively)
    {
        for(auto& childPtr : element.children())
        {
            applyLayout(*childPtr, true);
        }
    }
}

void Layouter::applyEdgeLayout(gim::Element& element, const EdgeLayout& edgeLayout)
{
    glm::ivec2 containerSize = *UiTools::getOrNone<glm::ivec2>(element, "size");

    for(auto& childPtr : element.children())
    {
        auto& child = *childPtr;
        auto layoutData = UiTools::getOrNone<EdgeLayoutData>(child, "layout_data");

        if(layoutData)
        {
            auto childSize = UiTools::getOrNone<glm::ivec2>(child, "size");
            FEA_ASSERT(childSize, "Edge layouted object has no size");

            float positionPercentage = layoutData->position / 1000.0f;
            float sizePercentage = layoutData->size ? *layoutData->size / 1000.0f : 0.0f; //will only be used if there exists a size

            glm::ivec2 targetPosition;

            if(layoutData->edge == EdgeLayout::TOP || layoutData->edge == EdgeLayout::BOTTOM)
            {
                if(layoutData->size)
                    childSize->x = static_cast<int32_t>(sizePercentage * containerSize.x);

                targetPosition.x = static_cast<int32_t>(positionPercentage * containerSize.x);

                if(layoutData->edge == EdgeLayout::BOTTOM)
                    targetPosition.y = containerSize.y - childSize->y;
            }
            else if(layoutData->edge == EdgeLayout::LEFT || layoutData->edge == EdgeLayout::RIGHT)
            {
                if(layoutData->size)
                    childSize->y = static_cast<int32_t>(sizePercentage * containerSize.y);

                targetPosition.y = static_cast<int32_t>(positionPercentage * containerSize.y);

                if(layoutData->edge == EdgeLayout::RIGHT)
                    targetPosition.x = containerSize.x - childSize->x;
            }

            child.setAttribute("position", targetPosition + layoutData->offset);
            child.setAttribute("size", *childSize);
        }
    }
}

void Layouter::applyGridLayout(gim::Element& element, const GridLayout& gridLayout)
{
    glm::ivec2 containerSize = *UiTools::getOrNone<glm::ivec2>(element, "size");

    int32_t wrapIndex = 0;
    int32_t wrappedAmount = 0;

    int32_t wrapWidth;
   
    if(gridLayout.order == GridLayout::COLUMNS)
        wrapWidth = containerSize.x / gridLayout.wrapSize;
    else if(gridLayout.order == GridLayout::ROWS)
        wrapWidth = containerSize.y / gridLayout.wrapSize;

    for(auto& childPtr : element.children())
    {
        auto& child = *childPtr;

        glm::ivec2 targetPosition(wrapIndex * wrapWidth, wrappedAmount * gridLayout.wrapDistance);
        glm::ivec2 targetSize(wrapWidth, gridLayout.wrapDistance);

        if(gridLayout.order == GridLayout::ROWS)
        {
            std::swap(targetPosition.x, targetPosition.y);
            std::swap(targetSize.x, targetSize.y);
        }

        child.setAttribute("position", targetPosition);
        child.setAttribute("size", targetSize);

        ++wrapIndex;
        if(wrapIndex == gridLayout.wrapSize)
        {
            wrapIndex = 0;
            ++wrappedAmount;
        }
    }
}
