#include "glmvec2adaptor.hpp"

Vec2Adaptor::Vec2Adaptor()
{
}

Vec2Adaptor::Vec2Adaptor(int32_t x, int32_t y):
        mVec2({x, y})
{
}

Vec2Adaptor::Vec2Adaptor(const glm::ivec2& vec2):
        mVec2(vec2)
{
}

Vec2Adaptor Vec2Adaptor::operator+(const Vec2Adaptor& other) const
{
        return mVec2 + other.mVec2;
}

int32_t Vec2Adaptor::x() const
{
        return mVec2.x;
}

int32_t Vec2Adaptor::y() const
{
        return mVec2.y;
}

void Vec2Adaptor::x(int32_t x)
{
        mVec2.x = x;
}

void Vec2Adaptor::y(int32_t y)
{
        mVec2.y = y;
}
