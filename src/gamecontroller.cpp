#include "gamecontroller.hpp"
#include "facility/facility.hpp"
#include "world/gameworld.hpp"
#include "resources/resourcestorage.hpp"
#include "facility/jobs.hpp"
#include "world/landscape.hpp"
#include "world/constructionmanager.hpp"
#include "world/tiles.hpp"
#include "gameui.hpp"
#include "util/anytools.hpp"

GameController::GameController(fea::MessageBus& bus, Facility& facility, GameWorld& gameWorld, const ResourceStorage& resources):
    mBus(bus),
    mFacility(facility),
    mWorld(gameWorld),
    mResources(resources),
    mGameUI(nullptr)
{
    subscribe(mBus, *this);
}

void GameController::positionAction(const glm::vec2& worldPosition, PositionAction action)
{
    glm::ivec2 tilePosition = Tiles::toTile(worldPosition);

    if(mWorld.constructions().constructionAllowed(tilePosition))
    {
        if(action == PositionAction::PRIMARY)
        {
            WallType type = mResources.wallIds().at("steel_wall");
            newConstructionOrder(tilePosition, {ConstructionCategory::WALL, type});
        }
        if(action == PositionAction::TERTIARY)
        {
            WallType type = mResources.floorIds().at("steel_floor");
            newConstructionOrder(tilePosition, {ConstructionCategory::FLOOR, type});
        }
        else if(action == PositionAction::SECONDARY)
        {
            cancelWallOrder(tilePosition);
            createRoom(tilePosition);
        }
    }
    else if(mWorld.landscape().walls().at(tilePosition))
    {
       if(action == PositionAction::TERTIARY)
        {
            newMineOrder(tilePosition);
        }
    }
}

void GameController::handleMessage(const ConstructionCreatedMessage& message)
{
    if(mGameUI)
        mGameUI->dehighlightTiles({message.position});
}

void GameController::jobCreated(int32_t id, const Job& job)
{
    if(mGameUI)
    {
        if(job.type == Jobs::CONSTRUCT)
        {
            ConstructionType constructionType = AnyTools::getOrAssert<ConstructionType>(std::string("type"), job.jobData);
            const glm::ivec2& position = AnyTools::getOrAssert<glm::ivec2>(std::string("position"), job.jobData);

            if(constructionType.category == ConstructionCategory::WALL)
                mGameUI->highlightTiles({position}, GameUI::HighlightMode::IDLE_WALL_ORDER);
            else if(constructionType.category == ConstructionCategory::FLOOR)
                mGameUI->highlightTiles({position}, GameUI::HighlightMode::IDLE_FLOOR_ORDER);
        }
        else if(job.type == Jobs::MINE_WALL)
        {
            const glm::ivec2& position = AnyTools::getOrAssert<glm::ivec2>(std::string("position"), job.jobData);

            mGameUI->highlightTiles({position}, GameUI::HighlightMode::IDLE_MINE_ORDER);
        }
    }
}

void GameController::jobUpdated(int32_t id, WorkStatus status, const Job& job)
{
    if(mGameUI)
    {
        if(job.type == Jobs::CONSTRUCT)
        {
            ConstructionType constructionType = AnyTools::getOrAssert<ConstructionType>(std::string("type"), job.jobData);
            const glm::ivec2& position = AnyTools::getOrAssert<glm::ivec2>(std::string("position"), job.jobData);

            if(status == WorkStatus::WAITING)
            {
                if(constructionType.category == ConstructionCategory::WALL)
                    mGameUI->highlightTiles({position}, GameUI::HighlightMode::IDLE_WALL_ORDER);
                else if(constructionType.category == ConstructionCategory::FLOOR)
                    mGameUI->highlightTiles({position}, GameUI::HighlightMode::IDLE_FLOOR_ORDER);
            }
            else if(status == WorkStatus::IN_PROGRESS)
            {
                if(constructionType.category == ConstructionCategory::WALL)
                    mGameUI->highlightTiles({position}, GameUI::HighlightMode::BUSY_WALL_ORDER);
                else if(constructionType.category == ConstructionCategory::FLOOR)
                    mGameUI->highlightTiles({position}, GameUI::HighlightMode::BUSY_FLOOR_ORDER);
            }
            else if(status == WorkStatus::COMPLETED || status == WorkStatus::CANCELLED)
            {
                mGameUI->dehighlightTiles({position});
            }
        }
        else if(job.type == Jobs::MINE_WALL)
        {
            const glm::ivec2& position = AnyTools::getOrAssert<glm::ivec2>(std::string("position"), job.jobData);

            if(status == WorkStatus::WAITING)
                mGameUI->highlightTiles({position}, GameUI::HighlightMode::IDLE_MINE_ORDER);
            else if(status == WorkStatus::IN_PROGRESS)
                mGameUI->highlightTiles({position}, GameUI::HighlightMode::BUSY_MINE_ORDER);
            else if(status == WorkStatus::COMPLETED || status == WorkStatus::CANCELLED)
                mGameUI->dehighlightTiles({position});
        }
    }
}

void GameController::setUI(GameUI& ui)
{
    mGameUI = &ui;


    std::deque<std::reference_wrapper<const WallResource>> wallKnowledge;
    for(const auto& wall : mResources.walls())
    {
        wallKnowledge.emplace_back(wall.second);
    }

    mGameUI->setAvailableWalls(wallKnowledge);
}

void GameController::newConstructionOrder(const glm::ivec2& position, ConstructionType type)
{
    if(mFacility.jobs().find(Jobs::CONSTRUCT, "position", position, false).size() == 0)
    {
        Job wallJob{ Jobs::CONSTRUCT,
            {
                {"position", position},
                {"type", type}
            }};

        mFacility.jobs().add(std::move(wallJob));
    }
}

void GameController::cancelWallOrder(const glm::ivec2& position)
{
    const auto& currentWallJob = mFacility.jobs().find(Jobs::CONSTRUCT, "position", position);

    //FIXME check if it can cancel even if it started work on it
    if(currentWallJob.size() != 0)
        mFacility.jobs().update(currentWallJob.front().id, WorkStatus::CANCELLED);
}

void GameController::newMineOrder(const glm::ivec2& position)
{
    if(mFacility.jobs().find(Jobs::MINE_WALL, "position", position, false).size() == 0)
    {
        Job mineJob{ Jobs::MINE_WALL,
            {
                {"position", position},
            }};

        mFacility.jobs().add(std::move(mineJob));
    }
}

void GameController::cancelMineOrder(const glm::ivec2& position)
{
    const auto& currentMineJob = mFacility.jobs().find(Jobs::MINE_WALL, "position", position);

    //FIXME check if it can cancel even if it started work on it
    if(currentMineJob.size() != 0)
        mFacility.jobs().update(currentMineJob.front().id, WorkStatus::CANCELLED);
}

void GameController::createRoom(const glm::ivec2& position)
{
    std::unordered_set<glm::ivec2> tiles;
    glm::ivec2 size = {1, 1};
//    for(int x = position.x - 1; x <= position.x + 1; x++)
//    {
//        for(int y = position.y - 1; y <= position.y + 1; y++)
//        {
//            tiles.insert({x, y});
//        }
//    }
//    auto id = mWorld.rooms().createRoom(0, tiles);
    auto id = mWorld.rooms().createRoom(0, position, size);
    FEA_ASSERT(id == *mWorld.rooms().roomAt(position), "some bug here");
    mGameUI->highlightTiles(tiles, GameUI::HighlightMode::BUSY_MINE_ORDER);

    LOG_W("Created Room, is it valid? " << (int)mWorld.rooms().isRoomValid(id));
}
