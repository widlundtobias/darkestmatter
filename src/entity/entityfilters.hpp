#pragma once
#include <functional>
#include <fea/entitysystem.hpp>
#include <glm/glm.hpp>

class EntityFilters
{
    public:
        static std::function<bool(fea::EntityPtr)> overlapsTile(const glm::ivec2& tile);
};
