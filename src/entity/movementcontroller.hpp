#pragma once
#include <fea/util.hpp>
#include <fea/entitysystem.hpp>
#include "entitymessages.hpp"

class MovementController: public fea::EntityController, public fea::MessageReceiver<
    MovementIntentionMessage
    >
{
    public:
        MovementController(fea::MessageBus& bus);
        bool keepEntity(fea::EntityPtr entity) const override;
        void entityKept(fea::EntityPtr entity) override;
        void entityDestroyed(fea::EntityPtr entity) override;
        void update(float deltaTime) override;
        void handleMessage(const MovementIntentionMessage& message) override;
    private:
        fea::MessageBus& mBus;
};
