#pragma once
#include <fea/util.hpp>
#include <fea/entitysystem.hpp>
#include "world/worldmessages.hpp"
#include "entity/jobid.hpp"

class Facility;
class TaskAIController;

class WorkerController: public fea::EntityController, public fea::MessageReceiver<
    JobFinishedMessage,
    JobFailedMessage
    >
{
    public:
        WorkerController(fea::MessageBus& bus, Facility& facility, TaskAIController& taskAIController);
        virtual bool keepEntity(fea::EntityPtr entity) const override;
        virtual void entityKept(fea::EntityPtr entity) override;
        virtual void entityDestroyed(fea::EntityPtr entity) override;
        virtual void update(float deltaTime) override;
        void handleMessage(const JobFinishedMessage& message) override;
        void handleMessage(const JobFailedMessage& message) override;
    private:
        fea::MessageBus& mBus;
        Facility& mFacility;
        TaskAIController& mTaskAIController;

        std::unordered_map<fea::EntityId, std::unordered_map<int32_t, int32_t>> mFailCooldowns;
        std::unordered_map<JobId, fea::EntityId> mNowPerforming;
};
