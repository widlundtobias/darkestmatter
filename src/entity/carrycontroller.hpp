#pragma once
#include <fea/entitysystem.hpp>
#include <fea/util.hpp>
#include "world/itemmanager.hpp"
#include "items/inventory.hpp"

class GameWorld;

class CarryController: public fea::EntityController
{
    public:
        CarryController(fea::MessageBus& bus, GameWorld& world);
        virtual bool keepEntity(fea::EntityPtr entity) const override;
        virtual void entityKept(fea::EntityPtr entity) override;
        virtual void entityDestroyed(fea::EntityPtr entity) override;
        virtual void update(float deltaTime) override;
        th::Optional<int32_t> inRange(fea::EntityId id) const;
        bool canPickUp(fea::EntityId id, const ItemType& itemType) const;
        bool pickupItem(fea::EntityId id, int32_t itemTypeId);
        bool dropItem(fea::EntityId id, int32_t itemTypeId);
        th::Optional<Item> giveItem(fea::EntityId id, Item item);
        th::Optional<Item> takeItem(fea::EntityId id, int32_t itemTypeId);
        bool hasItem(fea::EntityId id, int32_t itemTypeId) const;
        const Inventory& inventory(fea::EntityId id);
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
};
