#pragma once
#include <fea/entitysystem.hpp>
#include <glm/glm.hpp>
#include "movementaction.hpp"

struct MovementIntentionMessage
{
    fea::EntityId entityId;
    MovementAction action;
    glm::vec2 direction;
};
