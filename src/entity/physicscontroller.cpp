#include "physicscontroller.hpp"
#include "world/worldmessages.hpp"

PhysicsController::PhysicsController(fea::MessageBus& bus, const Landscape& landscape):
    mBus(bus),
    mLandscapeCollisionChecker(landscape)
{
}

bool PhysicsController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("position") &&
                entity->hasAttribute("velocity") &&
                entity->hasAttribute("acceleration") &&
                entity->hasAttribute("width");

    return keep;
}

void PhysicsController::entityKept(fea::EntityPtr entity)
{
}

void PhysicsController::entityDestroyed(fea::EntityPtr entity)
{
}

void PhysicsController::update(float deltaTime)
{
    for(auto entity : mEntities)
    {
        auto& position = entity.second->getAttribute<glm::vec2>("position");
        auto oldPosition = position;
        auto& velocity = entity.second->getAttribute<glm::vec2>("velocity");
        auto oldVelocity = velocity;
        auto& acceleration = entity.second->getAttribute<glm::vec2>("acceleration");
        float width = entity.second->getAttribute<float>("width");

        velocity += acceleration;
        if(glm::length(velocity) > 0.0f)
        {
            auto collisionResult = mLandscapeCollisionChecker.evaluate(position, position + velocity, width);
            position = collisionResult.position;

            int32_t entityId = entity.second->getId();

            if(collisionResult.xCollided)
                velocity.x = 0.0;
            if(collisionResult.yCollided)
                velocity.y = 0.0;

            mBus.send(EntityMovedMessage{entityId, position, oldPosition});

            if(collisionResult.xCollided || collisionResult.yCollided)
            {
                mBus.send(LandscapeCollisionMessage{entityId, collisionResult.collidedTiles, oldVelocity});
            }
        }
    }
}
