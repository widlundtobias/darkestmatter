#include "brainfactory.hpp"
#include "facility/jobs.hpp"
#include "jobai/solverfactory.hpp"
#include "entity/jobsolvers/constructsolver.hpp"
#include "entity/jobsolvers/minewallsolver.hpp"
#include "entity/jobsolvers/gotowalksolver.hpp"
#include "entity/jobsolvers/evadetilessolver.hpp"
#include "entity/jobsolvers/cleartilessolver.hpp"
#include "entity/jobsolvers/storagekeepingsolver.hpp"
#include "entity/jobsolvers/fetchfromstacksolver.hpp"
#include "entity/jobsolvers/fetchfromcontainersolver.hpp"
#include "entity/jobsolvers/fetchitemsolver.hpp"
#include "entity/jobsolvers/findandfetchsolver.hpp"
#include "entity/jobsolvers/putitemonstasksolver.hpp"
#include "entity/jobsolvers/putitemincontainersolver.hpp"
#include "entity/jobsolvers/fromstacktostacksolver.hpp"
#include "entity/jobsolvers/fromstacktocontainersolver.hpp"

Brain BrainFactory::generate(BrainType type, AnyMap<std::string> external)
{
    if(type == BrainType::HUMANOID)
        return Brain({
                { Jobs::GOTO, solverFactory<GotoWalkSolver> },

                { Jobs::CONSTRUCT, solverFactory<ConstructSolver> },
                { Jobs::MINE_WALL, solverFactory<MineWallSolver> },

                { Jobs::CLEAR_TILES, solverFactory<ClearTilesSolver> },
                { Jobs::EVADE_TILES, solverFactory<EvadeTilesSolver> },

                { Jobs::STORAGE_KEEPING, solverFactory<StorageKeepingSolver> },

                { Jobs::FIND_AND_FETCH, solverFactory<FindAndFetchSolver> },

                { Jobs::FETCH_FROM_STACK, solverFactory<FetchFromStackSolver> },
                { Jobs::FETCH_FROM_CONTAINER, solverFactory<FetchFromContainerSolver> },
                { Jobs::FETCH_ITEM, solverFactory<FetchItemSolver> },

                { Jobs::PUT_ITEM_IN_CONTAINER, solverFactory<PutItemInContainerSolver> },
                { Jobs::PUT_ITEM_ON_STACK, solverFactory<PutItemOnStackSolver> },

                { Jobs::FROM_STACK_TO_CONTAINER, solverFactory<FromStackToContainerSolver> },
                { Jobs::FROM_STACK_TO_STACK, solverFactory<FromStackToStackSolver> }
        },
        external);
    else
        return Brain({},{});
}
