#include "taskaicontroller.hpp"
#include "world/worldmessages.hpp"
#include "facility/facility.hpp"
#include "entity/braintype.hpp"
#include "entity/brainfactory.hpp"
#include "world/gameworld.hpp"

TaskAIController::TaskAIController(fea::MessageBus& bus, GameWorld& gameWorld, EvadeTileController& evadeTileController, CarryController& carryController, ItemInvokerController& itemInvokerController):
    mBus(bus),
    mGameWorld(gameWorld),
    mEvadeTileController(evadeTileController),
    mCarryController(carryController),
    mItemInvokerController(itemInvokerController),
    mCurrentlySolving(0)
{
}

bool TaskAIController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("brain_type") &&
                entity->hasAttribute("brain");

    return keep;
}

void TaskAIController::entityKept(fea::EntityPtr entity)
{
    BrainType brainType = entity->getAttribute<BrainType>("brain_type");
    entity->setAttribute("brain", BrainFactory::generate(brainType,
    {
        {"world", std::ref(mGameWorld)},
        {"entity", entity},
        {"message_bus", std::ref(mBus)},
        {"evade_tiles", std::ref(mEvadeTileController)},
        {"carry_controller", std::ref(mCarryController)},
        {"item_invoker_controller", std::ref(mItemInvokerController)},
    }));
}

void TaskAIController::entityDestroyed(fea::EntityPtr entity)
{
}

void TaskAIController::update(float deltaTime)
{
    for(auto entityIter : mEntities)
    {
        fea::EntityPtr entity = entityIter.second;
        auto& brain = entity->getAttribute<Brain>("brain");

        auto updateResult = brain.update();

        if(updateResult.status == JobStatus::COMPLETED)
        {
            FEA_ASSERT(updateResult.job != nullptr, "No job on succeed?!");
            const auto& job = *updateResult.job;
            //FEA_ASSERT(job.jobData.count("id") != 0, "Job lacks id");
            //LOG_V("I am entity " << entity.second->getId() << " and I finished my job of id '" << job.jobData.at("id").get<int32_t>() << "!");
            
            FEA_ASSERT(mCurrentlySolving.count(entity->getId()) != 0, "Entity finished a job without working on it");
            JobId currentlySolving = mCurrentlySolving.at(entity->getId()).jobId;
            mJobIdPool.release(currentlySolving);
            mCurrentlySolving.erase(entity->getId());

            doneWithTask(entity->getId());
            mBus.send(JobFinishedMessage{currentlySolving, job, entity});
        }
        else if(updateResult.status == JobStatus::FAILED)
        {
            FEA_ASSERT(updateResult.job != nullptr, "No job on fail?!");
            const auto& job = *updateResult.job;
            //FEA_ASSERT(job.jobData.count("id") != 0, "Job lacks id");
            //LOG_V("I am entity " << entity.second->getId() << " and I failed my job of id '" << job.jobData.at("id").get<int32_t>() << "!");
            FEA_ASSERT(mCurrentlySolving.count(entity->getId()) != 0, "Entity finished a job without working on it");
            JobId currentlySolving = mCurrentlySolving.at(entity->getId()).jobId;
            mJobIdPool.release(currentlySolving);
            mCurrentlySolving.erase(entity->getId());

            doneWithTask(entity->getId());
            mBus.send(JobFailedMessage{currentlySolving, job, entity});
        }
    }
}

th::Optional<int32_t> TaskAIController::doJob(fea::EntityId id, const Job& job, JobPriority priority)
{
    if(mEntities.count(id) != 0)
    {
        auto alreadySolvingIterator = mCurrentlySolving.find(id);

        bool newJobAccepted = false;

        if(alreadySolvingIterator != mCurrentlySolving.end())
        {//this entity is already solving something. Let's see if the priority of the new task is higher
            JobPriority oldPriority = alreadySolvingIterator->second.priority;
            JobId oldId = alreadySolvingIterator->second.jobId;

            if(priority > oldPriority)
            {//priority is higher. Stash away current task and see if the brain accepts the new task
                auto& brain = mEntities.at(id)->getAttribute<Brain>("brain");

                auto oldTaskStack = brain.retrieveState();

                newJobAccepted = brain.solve(job);

                if(newJobAccepted)
                {//new job was accepted and we must stash away the old task for later
                    mStoredStacks[id].emplace(oldPriority, StashedSolving{oldId, std::move(oldTaskStack)});
                }
                else
                {//new job was rejected, so put back the old task state
                    brain.setState(std::move(oldTaskStack));
                }
            }
            else
            {//priority is not higher. So do not accept the new task
                return {};
            }
        }
        else
        {//this brain is not solving anything so see if the brain is been on solving this job
            auto& brain = mEntities.at(id)->getAttribute<Brain>("brain");
            newJobAccepted = brain.solve(job);
        }

        if(newJobAccepted)
        {//the brain accepted the job, so generate an ID for it and return it
            JobId newId = mJobIdPool.next();
            mCurrentlySolving[id] = { newId, priority };
            LOG_I("I am entity " << id << " and I am now solving a job!");
            return {newId};
        }
        else
        {//the brain did not accept. return null ID
            LOG_I("I am entity " << id << " and I denied solving a job!");
            return {};
        }
    }

    return {};
}

void TaskAIController::doneWithTask(fea::EntityId entityId)
{
    auto storedStacksIter = mStoredStacks.find(entityId);

    LOG_V("DONE WITH TASK");
    if(storedStacksIter != mStoredStacks.end())
    {//there is an old solver stack to ressurect
        LOG_V("RESUMING TASK");
        auto& storedStacks = storedStacksIter->second;
        FEA_ASSERT(!storedStacks.empty(), "It is empty! A bug.");

        auto& brain = mEntities.at(entityId)->getAttribute<Brain>("brain");

        //find the one with hightest priority
        auto highestIter = storedStacks.rbegin();

        //let brain solve it again
        StashedSolving oldStack = std::move(highestIter->second);
        brain.setState(std::move(oldStack.solverStack));
        mCurrentlySolving[entityId] = {oldStack.jobId, highestIter->first};

        //cleanup
        storedStacks.erase(--highestIter.base());
        if(storedStacks.empty())
            mStoredStacks.erase(storedStacksIter);
    }
}
