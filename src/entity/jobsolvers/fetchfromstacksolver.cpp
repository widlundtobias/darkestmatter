#include "fetchfromstacksolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"
#include "items/inventory.hpp"
#include "facility/jobs.hpp"

FetchFromStackSolver::FetchFromStackSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mItemStackId(job.jobData.at("from_item_stack_id").get<int32_t>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mItemType(mWorld.items().itemType(mItemTypeId))
{
}

JobSolver::UpdateResult FetchFromStackSolver::update()
{
    //if we cant pick up an item of the desired type, fail
    if(!mCarryController.canPickUp(mEntity->getId(), mItemType))
    {
        return fail();
    }

    glm::ivec2 currentHalfTilePosition = HalfTiles::toTile(mEntity->getAttribute<glm::vec2>("position"));

    if(!mTargetStack)
    {
        mTargetStack = mWorld.items().stack(mItemStackId);

        if(!mTargetStack)
        {//no such stack. fail
            LOG_I("Stack didn't exist ");
            return fail();
        }
    }

    if(mTargetStack->position == currentHalfTilePosition)
    {//we are at the stack, rejoice

        //stack might have been deleted already by someone else taking it. we must validate it
        validateStack(mTargetStack);

        if(!mTargetStack)
        {//it is not valid anymore, fail
            return fail();
        }
        else
        {//we can now pick it up
            if(mCarryController.pickupItem(mEntity->getId(), mItemTypeId))
            {
                LOG_I("picked up item");
                return succeed();
            }
            else
            {
                LOG_I("Cannot pick up item. Why? I don't know. Fail");
                return fail();
            }
        }
    }
    else
    {//else we need to go to the stack
        float acceptableDistance = glm::length((glm::vec2)HalfTiles::size())/4.f;

        return require({Jobs::GOTO, {{"position", HalfTiles::tileCenter(mTargetStack->position)}, {"minimumDistance", acceptableDistance}}});
    }


    return working();
}

void FetchFromStackSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void FetchFromStackSolver::notifyFail(AnyMap<std::string> failedData)
{
}

void FetchFromStackSolver::validateStack(th::Optional<ItemManager::ItemStackView>& itemStack) const
{
    auto stackView = mWorld.items().stackAt(itemStack->position);
    if(stackView)
    {//there is a stack for sure, now make sure it is the right type, if it is then we can use it
        if(stackView->stack.get().type == mItemType)
            itemStack.reset(*stackView);
        else
            itemStack.reset();
    }
    else
        return itemStack.reset();
}
