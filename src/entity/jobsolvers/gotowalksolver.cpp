#include "gotowalksolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"

GotoWalkSolver::GotoWalkSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mTargetPosition(job.jobData.at("position").get<glm::vec2>()),
    mMinimumDistance(job.jobData.at("minimumDistance").get<float>()),
    mDistanceTravelledLately(60, 10.0f) //10.0f is not a numer that makes sense. it's just meant to not be zero
{
}

GotoWalkSolver::~GotoWalkSolver()
{
    mBus.send(MovementIntentionMessage{mEntity->getId(), MovementAction::IDLE, glm::vec2()});
}

JobSolver::UpdateResult GotoWalkSolver::update()
{
    glm::vec2 currentPosition = mEntity->getAttribute<glm::vec2>("position");
    glm::ivec2 currentTilePosition = Tiles::toTile(currentPosition);
    glm::ivec2 targetTilePosition = Tiles::toTile(mTargetPosition);
    //        bool atDestination = currentTilePosition == mTargetPosition;
    bool atDestination = glm::distance(mTargetPosition, currentPosition) <= mMinimumDistance;

    //performing
    if(!atDestination)
    {//I am not at the destination, let's see what I should do
        if(!mPath)
        {//I have no path to follow! I need to find one
            std::fill(mDistanceTravelledLately.begin(), mDistanceTravelledLately.end(), 10.0f);
            auto path = mPathfinder.findPath(LandscapePathAdaptor(mWorld.landscape().walls()), currentTilePosition, targetTilePosition).path;
            mPath.reset(std::move(path));

            if(mPath->size() == 0)
            {//There was no path to the destination. Nothing I can do
                return fail();
            }
        }

        //now there is a path

        FEA_ASSERT(mPath, "path is null, what have you done? D:");
        const glm::ivec2& nextTilePosition = mPath->front();

        //if not much progress has been done the last frames, then invalidate the path and skip
        if(std::accumulate(mDistanceTravelledLately.begin(), mDistanceTravelledLately.end(), 0.0f) < 5.0f) //5.0f is not much to travel during 60 frames
        {
            LOG_V("My path doesn't seem to work, I'll make a new one. I am at " << currentTilePosition << " and the next position was " << nextTilePosition << ". The target building will be at " << mTargetPosition << ".");
            mPath.reset();
            return working();
        }

        if(currentTilePosition != nextTilePosition || mPath->size() == 1)
        {

            glm::vec2 targetPosition;

            if(mPath->size() == 1)
            {//now we should go directly to the target position
                targetPosition = mTargetPosition;
            }
            else
            {//issue move intentions towards node
                targetPosition = Tiles::tileCenter(nextTilePosition);
            }

            float distanceToTarget = glm::distance(currentPosition, targetPosition);
            //               LOG_I("Distance to next tile in path is " << distanceToTarget);

            if(distanceToTarget > 1000.f)
            {
                LOG_V("Path seem too long, let's try another one. distance was " << distanceToTarget << " and that is between " << currentPosition << " and " << targetPosition << " when nextTilePosition is " << nextTilePosition << " and size of path is " << mPath->size());
                FEA_ASSERT(false, "blopp");
                mPath.reset();
                return working();
            }

            glm::vec2 walkDirection = glm::normalize(targetPosition - currentPosition);

            mBus.send(MovementIntentionMessage{mEntity->getId(), MovementAction::WALK, walkDirection});

            mDistanceTravelledLately.pop_front();
            mDistanceTravelledLately.push_back(glm::distance(currentPosition, mLastPosition));

            mLastPosition = currentPosition;
        }
        else
        {
            mPath->pop_front();
        }
    }
    else
    {//It appears I am at the destination, well done boys!
        return succeed();
    }
    return working();
}

void GotoWalkSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void GotoWalkSolver::notifyFail(AnyMap<std::string> failedData)
{
}
