#include "fetchfromcontainersolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "world/landscapesemantics.hpp"
#include "entity/entitymessages.hpp"
#include "facility/jobs.hpp"
#include "entity/iteminvokercontroller.hpp"

FetchFromContainerSolver::FetchFromContainerSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mItemInvokerController(external.at("item_invoker_controller").get<std::reference_wrapper<ItemInvokerController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mContainerStackId(job.jobData.at("from_container_stack_id").get<int32_t>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mItemType(mWorld.items().itemType(mItemTypeId))
{
}

JobSolver::UpdateResult FetchFromContainerSolver::update()
{
    glm::ivec2 currentHalfTilePosition = HalfTiles::toTile(mEntity->getAttribute<glm::vec2>("position"));

    if(!mTargetContainer)
    {
        mTargetContainer = mWorld.items().stack(mContainerStackId);

        if(!mTargetContainer)
        {
            //no such container. fail
            LOG_I("Container didn't exist");
            return fail();
        }

        mContainerStackId = mTargetContainer->stackId;
    }

    if(mTargetContainer->position == currentHalfTilePosition)
    {
        //we are at the container, rejoice

        //stack might have been deleted already by someone else taking it. we must validate it
        mTargetContainer = mWorld.items().renewStackView(*mTargetContainer);

        if(!mTargetContainer)
        {
            //this container has become invalid
            return fail();
        }
        else
        {
            //we can now take the item from the container

            auto returned = mItemInvokerController.invokeItemStack(mEntity->getId(), mContainerStackId, 0, ItemAction::TAKE_OUT,
            {
                {"type", mItemTypeId}
            });

            auto returnedItem = AnyTools::moveOutOrNone<Item>(std::string("item"), *returned);

            if(!returnedItem)
            {
                //item was not take from the container, let it go
                LOG_V("eugh, the container didn't have the item, (damn, i knew the first date was too soon)");
                return fail();
            }
            else
            {
                auto item = mCarryController.giveItem(mEntity->getId(), *returnedItem);
                if(!item)
                {
                    LOG_V("i did it! i took item from container");
                    return succeed();
                }
                else
                {
                    //damnit inventory rejected the item, let's put it back in container
                    auto puttedItem = mItemInvokerController.invokeItemStack(mEntity->getId(), mContainerStackId, 0, ItemAction::PUT,
                    {
                        {"item", std::move(*item)}
                    });

                    if(!puttedItem)
                    {//at least we could put the item back in the container

                        LOG_V("i got some item from a container but coulndt put it in my inventory, so here it is back in the container");
                        return fail();
                    }
                    else
                    {//having a item i cant put anywhere ptu me in a bery bad position, better crash
                        FEA_ASSERT(false, "Item cannot be put in container nor inventory and I didn't know what to do so I crashed :D");
                        return fail(); //if I reach this in release mode, the item will disappear
                    }
                }
            }
        }
    }
    else
    {
        //else we need to go to the container
        float acceptableDistance = glm::length((glm::vec2)HalfTiles::size())/4.f;

        return require({Jobs::GOTO, {{"position", HalfTiles::tileCenter(mTargetContainer->position)}, {"minimumDistance", acceptableDistance}}});
    }


    return working();
}

void FetchFromContainerSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void FetchFromContainerSolver::notifyFail(AnyMap<std::string> failedData)
{
}
