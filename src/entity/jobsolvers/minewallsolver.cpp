#include "minewallsolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "items/inventory.hpp"
#include "entity/evadetileset.hpp"
#include "facility/jobs.hpp"

MineWallSolver::MineWallSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mTargetTile(job.jobData.at("position").get<glm::ivec2>())
{
}

JobSolver::UpdateResult MineWallSolver::update()
{
    glm::vec2 currentPosition = mEntity->getAttribute<glm::vec2>("position");
    glm::ivec2 currentTilePosition = Tiles::toTile(currentPosition);

    glm::ivec2 diff = currentTilePosition - mTargetTile;

    bool inRange = (std::abs(diff.x) + std::abs(diff.y)) <= 1;

    if(inRange)
    {//we can mine it
        mWorld.landscape().setWall(mTargetTile, 0);
        return succeed();
    }
    else
    {//need to go to tha place
        glm::ivec2 targetPosition;

        if(!mWorld.landscape().walls().at(mTargetTile + glm::ivec2(0, 1)))
            targetPosition = mTargetTile + glm::ivec2(0, 1);
        else if(!mWorld.landscape().walls().at(mTargetTile + glm::ivec2(1, 0)))
            targetPosition = mTargetTile + glm::ivec2(1, 0);
        else if(!mWorld.landscape().walls().at(mTargetTile + glm::ivec2(0, -1)))
            targetPosition = mTargetTile + glm::ivec2(0, -1);
        else if(!mWorld.landscape().walls().at(mTargetTile + glm::ivec2(-1, 0)))
            targetPosition = mTargetTile + glm::ivec2(-1, 0);
        else
        {//no accesible tile, fail
            return fail();
        }

        return require({Jobs::GOTO, {{"position", Tiles::tileCenter(targetPosition)}, {"minimumDistance", glm::length((glm::vec2)Tiles::size()/15.f)}}});
    }


    return working();
}

void MineWallSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void MineWallSolver::notifyFail(AnyMap<std::string> failedData)
{
}
