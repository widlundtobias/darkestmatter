#include "findandfetchsolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"
#include "items/inventory.hpp"
#include "util/anytools.hpp"
#include "facility/jobs.hpp"

FindAndFetchSolver::FindAndFetchSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mItemInvokerController(external.at("item_invoker_controller").get<std::reference_wrapper<ItemInvokerController>>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mItemType(mWorld.items().itemType(mItemTypeId)),
    mItemFetched(false)
{
}

JobSolver::UpdateResult FindAndFetchSolver::update()
{
    if(mItemFetched)
        return succeed();

    //if we cant pick up an item of the desired type, fail
    if(!mCarryController.canPickUp(mEntity->getId(), mItemType))
    {
        return fail();
    }

    if(mAvailableStacks.empty())
    {
        //available stacks list is empty, get some

        LOG_I("Starting fetch task");

        mAvailableStacks = mWorld.items().findItems(mItemTypeId);
        if(mAvailableStacks.empty())
        {//still empty? let's search for container then

            mAvailableStacks = mWorld.items().findItems(); //fetch all item stacks

            //exclude non-containers
            for(auto iter = mAvailableStacks.begin(); iter != mAvailableStacks.end();)
            {
                if(!mWorld.items().typeSatisfiesProperty(iter->stack.get().type, CONTAINER))
                {
                    //this is not a container
                    iter = mAvailableStacks.erase(iter);
                }
                else
                    ++iter;
            }

            //now contain all container
            for(auto iter = mAvailableStacks.begin(); iter != mAvailableStacks.end();)
            {
                auto returned = mItemInvokerController.invokeItemStack(mEntity->getId(), iter->stackId, 0, ItemAction::INSPECT, {});
                auto inventory = AnyTools::getOrNone<std::reference_wrapper<const Inventory>>(std::string("inventory"), *returned);

                FEA_ASSERT(inventory, "well, our container did not return any inventory...");

                if(!inventory->get().hasItem(mItemTypeId))
                {
                    //there is not the item we want in the container
                    iter = mAvailableStacks.erase(iter);
                }
                else
                    ++iter;
            }

            if(mAvailableStacks.empty())
            {
                //and no container had what we want? ok we can only fail now
                return fail();
            }
        }
    }


    if(mTargetStack)
        mTargetStack = mWorld.items().renewStackView(*mTargetStack);//make sure it is valid

    if(!mTargetStack)
    {
        //we dont have a choosen stack
        glm::vec2 currentPosition = mEntity->getAttribute<glm::vec2>("position");

        //sort availables by distance
        std::sort(mAvailableStacks.begin(), mAvailableStacks.end(), [&](const ItemManager::ItemStackView& first, const ItemManager::ItemStackView& second)
        {
            return glm::distance(HalfTiles::tileCenter(first.position), currentPosition) < glm::distance(HalfTiles::tileCenter(second.position), currentPosition);
        });

        mTargetStack = mAvailableStacks.front();
        mAvailableStacks.pop_front();

        LOG_I("choosen stack " << mTargetStack->stackId);
        LOG_I("currentPos: " << HalfTiles::toTile(currentPosition) << " targetPos: " << mTargetStack->position);
    }
    else
    {
        if(mWorld.items().typeSatisfiesProperty(mTargetStack->stack.get().type, CONTAINER))
        {//cotnainer? launch get from container job
            return require({Jobs::FETCH_FROM_CONTAINER,
                {
                    {"item_type_id", mItemTypeId},
                    {"from_container_stack_id", mTargetStack->stackId},
                }
            });
        }
        else
        {//just a stack? get from stack!
            return require({Jobs::FETCH_FROM_STACK,
                {
                    {"item_type_id", mItemTypeId},
                    {"from_item_stack_id", mTargetStack->stackId},
                }
            });
        }
    }

    return working();
}

void FindAndFetchSolver::notifySuccess(AnyMap<std::string> completedData)
{
    if(mRequiredJob.type == Jobs::FETCH_FROM_STACK || mRequiredJob.type == Jobs::FETCH_FROM_CONTAINER)
        mItemFetched = true;
}

void FindAndFetchSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == Jobs::FETCH_FROM_STACK || mRequiredJob.type == Jobs::FETCH_FROM_CONTAINER)
    {
        //I failed fetching an item, let's try another one
        LOG_I("failed to pick up item, lets try another one");
        mTargetStack.reset();
    }
}
