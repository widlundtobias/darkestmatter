#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class FromStackToContainerSolver : public JobSolver
{
    public:
        FromStackToContainerSolver(const Job& job, const AnyMap<std::string>& external);
        JobSolver::UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        fea::EntityPtr mEntity;

        int32_t mItemTypeId;
        int32_t mItemStackId;
        int32_t mContainerId;

        bool mFetchedItem;
        bool mCannotFetch;
        bool mPutItem;
};
