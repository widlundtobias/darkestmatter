#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/landscape.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class EvadeTilesSolver : public JobSolver
{
    public:
        EvadeTilesSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        CarryController& mCarryController;
        fea::EntityPtr mEntity;

        std::deque<ItemManager::ItemStackView> mAvailableStacks;
        th::Optional<ItemManager::ItemStackView> mTargetStacks;

        std::unordered_set<glm::ivec2> mEvadeTiles;

        bool mAtTarget;
        bool mNoPath;
};
