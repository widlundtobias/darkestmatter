#pragma once
#include "jobai/jobsolver.hpp"
#include "world/landscape.hpp"
#include "world/constructiontype.hpp"

class GameWorld;
class CarryController;

class ConstructSolver : public JobSolver
{
    public:
        ConstructSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        fea::EntityPtr mEntity;
        CarryController& mCarryController;
        glm::ivec2 mTargetPosition;
        ConstructionType mTargetType;
        th::Optional<int32_t> mSite;
        bool mCannotReachSite;
        bool mCannotFindMaterial;
        bool mCannotClearTiles;
        bool mSiteCleared;
};
