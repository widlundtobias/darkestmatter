#include "entity/jobsolvers/putitemonstasksolver.hpp"
#include "world/gameworld.hpp"
#include "facility/jobs.hpp"
#include "world/tiles.hpp"

PutItemOnStackSolver::PutItemOnStackSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mStackPosition(job.jobData.at("to_item_stack_position").get<glm::ivec2>()),
    mCannotGo(false)
{
}

JobSolver::UpdateResult PutItemOnStackSolver::update()
{
    if(mCannotGo)
        return fail();

    glm::ivec2 currentHalfTilePosition = HalfTiles::toTile(mEntity->getAttribute<glm::vec2>("position"));

    if(mStackPosition == currentHalfTilePosition)
    {//we are at the stack, rejoice
        if(mCarryController.dropItem(mEntity->getId(), mItemTypeId))
        {//we did yay
            return succeed();
        }
        else
        {//god damnit, not again
            return fail();
        }
    }
    else
    {//else we need to go to the stack
        float acceptableDistance = glm::length((glm::vec2)HalfTiles::size())/4.f;

        return require({Jobs::GOTO, {{"position", HalfTiles::tileCenter(mStackPosition)}, {"minimumDistance", acceptableDistance}}});
    }
}

void PutItemOnStackSolver::notifySuccess(AnyMap<std::string> completedData)
{

}

void PutItemOnStackSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == Jobs::GOTO)
    {//Cant go to the position, fail
        mCannotGo = true;
    }
}
