#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class FetchFromStackSolver : public JobSolver
{
    public:
        FetchFromStackSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        void validateStack(th::Optional<ItemManager::ItemStackView>& itemStack) const;
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        CarryController& mCarryController;
        fea::EntityPtr mEntity;

        int32_t mItemStackId;
        int32_t mItemTypeId;
        const ItemType& mItemType;
        th::Optional<ItemManager::ItemStackView> mTargetStack;
};
