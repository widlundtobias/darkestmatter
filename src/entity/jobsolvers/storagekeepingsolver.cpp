#include "storagekeepingsolver.hpp"
#include "world/gameworld.hpp"
#include "facility/jobs.hpp"
#include "world/tiles.hpp"
#include <util/log.hpp>

StorageKeepingSolver::StorageKeepingSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mPutSomethingAway(false),
    mFailed(false)
{
}

JobSolver::UpdateResult StorageKeepingSolver::update()
{
    //OK, I'll tend to storage keeping. I must identify misplaced things, or things that just litter and put them in a good spot

    if(mPutSomethingAway)
    {
        //I have managed to put something away! I succeeded
        LOG_V("I stashed away an item");
        return succeed();
    }

    if(mFailed)
    {
        return fail();
    }

    if(mContainerStacks.empty())
    {
        //have no containers to look through yet
        mContainerStacks = mWorld.items().findItems(CONTAINER);

        glm::vec2 currentPosition = mEntity->getAttribute<glm::vec2>("position");
        std::sort(mContainerStacks.begin(), mContainerStacks.end(), [&](const ItemManager::ItemStackView& first, const ItemManager::ItemStackView& second)
        {
            return glm::distance(HalfTiles::tileCenter(first.position), currentPosition) < glm::distance(HalfTiles::tileCenter(second.position), currentPosition);
        });

        for(auto stackView = mContainerStacks.begin(); stackView != mContainerStacks.end();)
        {
            auto roomId = mWorld.rooms().roomAt(Tiles::toTile(HalfTiles::tileCenter(stackView->position)));
            if(!roomId)
            {//the container is not in a room, let's put him in one

                auto rooms = mWorld.rooms().rooms(0); //0 is the hardcoded type for storage room
                std::vector<glm::ivec2> tiles;
                for(auto it = rooms.begin(); it != rooms.end(); it++)
                {
                        for(const auto& halfTile : mWorld.rooms().room(*it)->halfTiles()) //only keep empty one
                        {
                            if(!mWorld.items().stackAt(halfTile))
                                tiles.push_back(halfTile);
                        }
                }

                //sort them by distance
                std::sort(tiles.begin(), tiles.end(), [&](const glm::ivec2& first, const glm::ivec2& second)
                {
                    return glm::distance(Tiles::tileCenter(first), currentPosition) < glm::distance(Tiles::tileCenter(second), currentPosition);
                });

                //check if we dont already have a container in inventory
                if(mCarryController.inventory(mEntity->getId()).items().size())
                {
                    for(const auto& item : mCarryController.inventory(mEntity->getId()).items())
                    {
                        if(mWorld.items().typeSatisfiesProperty(item.type(), CONTAINER))
                        {//the item is a container, let's store it
                            mCurrentContainer = mContainerStacks.begin();
                            for(auto tile : tiles)
                            {
                                return require({Jobs::PUT_ITEM_ON_STACK,
                                    {
                                        {"item_type_id", item.type().typeId},
                                        {"to_item_stack_position", tile},
                                    }
                                });
                        }
                        }
                    }
                }

                for(auto tile : tiles)
                {//let's move the container to it!
                    stackView = mContainerStacks.erase(stackView);
                    mCurrentContainer = mContainerStacks.begin();
                    return require({Jobs::FROM_STACK_TO_STACK,
                        {
                            {"item_type_id", stackView->typeId},
                            {"from_item_stack_id", stackView->stackId},
                            {"to_item_stack_position", tile},
                        }
                    });
                }
                stackView = mContainerStacks.erase(stackView);
            }
            else if(!mWorld.rooms().isRoomValid(*roomId))
            {//room is invalide cant store items in it
                stackView = mContainerStacks.erase(stackView);
            }
            else
                stackView++;
        }

        if(mContainerStacks.empty())
        {
            //I am still empty. There are no containers!
            LOG_V("No containers to stash in");
            return fail();
        }

        mCurrentContainer = mContainerStacks.begin();
    }

    //I now have a list of containers to try out! Let's try them in order
    if(mCurrentContainer == mContainerStacks.end())
    {
        //None seemed to work! I have no containers left to try then so I must fail
        return fail();
    }

    auto containerStack = mWorld.items().renewStackView(*mCurrentContainer);
    mCurrentContainer++;

    if(!containerStack)
    {
        //container stack expired
        return working();
    }

    if(mCarryController.inventory(mEntity->getId()).items().size())// we have item in inventory, we should store them
    {
        return require({Jobs::PUT_ITEM_IN_CONTAINER,
            {
                {"item_type_id", mCarryController.inventory(mEntity->getId()).items()[0].type().typeId},
                {"to_container_stack_id", containerStack->stackId},
            }
        });
    }
    else if(mStashableStacks.empty())
    {
        //have no stashables yet
        mStashableStacks = mWorld.items().findItems(); //fetch all item stacks

        //exclude containers
        for(auto iter = mStashableStacks.begin(); iter != mStashableStacks.end();)
        {
            if(mWorld.items().typeSatisfiesProperty(iter->stack.get().type, CONTAINER))
            {
                //this is a container
                iter = mStashableStacks.erase(iter);
            }
            else
                ++iter;
        }

        if(mStashableStacks.empty())
        {
            //I am still empty. Nothing to stash away
            LOG_V("No items to stash");
            return succeed();
        }

        glm::vec2 currentPosition = mEntity->getAttribute<glm::vec2>("position");

        //sort stashables by distance
        std::sort(mStashableStacks.begin(), mStashableStacks.end(), [&](const ItemManager::ItemStackView& first, const ItemManager::ItemStackView& second)
        {
            return glm::distance(HalfTiles::tileCenter(first.position), currentPosition) < glm::distance(HalfTiles::tileCenter(second.position), currentPosition);
        });

        mCurrentStashable = mStashableStacks.begin();
    }

    if(mCurrentStashable == mStashableStacks.end())
    {
        //no more stashables to try
        LOG_V("No more stashables to try");
        return fail();
    }

    //Try a new stashable. renew them to make sure they are valid
    auto stashableStack = mWorld.items().renewStackView(*mCurrentStashable);
    ++mCurrentStashable;

    if(!stashableStack)
    {
        //item stack expired
        return working();
    }

    return require({Jobs::FROM_STACK_TO_CONTAINER,
        {
            {"item_type_id", stashableStack->stack.get().type.typeId},
            {"from_item_stack_id", stashableStack->stackId},
            {"to_container_stack_id", containerStack->stackId},
        }
    });

return working();
}

void StorageKeepingSolver::notifySuccess(AnyMap<std::string> completedData)
{
    mPutSomethingAway = true;
}

void StorageKeepingSolver::notifyFail(AnyMap<std::string> failedData)
{
    if(mRequiredJob.type == Jobs::FROM_STACK_TO_STACK || mRequiredJob.type == Jobs::FROM_STACK_TO_CONTAINER)
    {
        mFailed = true;
    }
}
