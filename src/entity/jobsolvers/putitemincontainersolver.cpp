#include "putitemincontainersolver.hpp"
#include "world/gameworld.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"
#include "util/anytools.hpp"
#include "facility/jobs.hpp"

PutItemInContainerSolver::PutItemInContainerSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mBus(external.at("message_bus").get<std::reference_wrapper<fea::MessageBus>>()),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mCarryController(external.at("carry_controller").get<std::reference_wrapper<CarryController>>()),
    mItemInvokerController(external.at("item_invoker_controller").get<std::reference_wrapper<ItemInvokerController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mItemTypeId(job.jobData.at("item_type_id").get<int32_t>()),
    mItemType(mWorld.items().itemType(mItemTypeId)),
    mContainerStackId(job.jobData.at("to_container_stack_id").get<int32_t>())
{
}

JobSolver::UpdateResult PutItemInContainerSolver::update()
{
    //if we don't have the needed item, we can't do anything
    if(!mCarryController.hasItem(mEntity->getId(), mItemTypeId))
    {
        LOG_V("I don't have the item! Fail time");
        return fail();
    }

    glm::ivec2 currentHalfTilePosition = HalfTiles::toTile(mEntity->getAttribute<glm::vec2>("position"));

    if(!mTargetContainer)
    {
        mTargetContainer = mWorld.items().stack(mContainerStackId);

        if(!mTargetContainer)
        {//no such container. fail
            LOG_I("Container didn't exist");
            return fail();
        }

        mContainerStackId = mTargetContainer->stackId;
    }

    if(mTargetContainer->position == currentHalfTilePosition)
    {//we are at the container, rejoice

        //stack might have been deleted already by someone else taking it. we must validate it
        mTargetContainer = mWorld.items().renewStackView(*mTargetContainer);

        if(!mTargetContainer)
        {//this container has become invalid
            return fail();
        }
        else
        {//we can now stash the item into the container
            auto item = mCarryController.takeItem(mEntity->getId(), mItemTypeId);

            if(item)
            {
                auto returned = mItemInvokerController.invokeItemStack(mEntity->getId(), mContainerStackId, 0, ItemAction::PUT,
                {
                    {"item", std::move(*item)}
                });

                FEA_ASSERT(returned, "woot, container became consumed by putting something into it...");
                auto returnedItem = AnyTools::moveOutOrNone<Item>(std::string("item"), *returned);

                if(returnedItem)
                {//item was rejected by the container! Put it back in inventory
                    LOG_V("eugh, the container didn't like it");

                    returnedItem = mCarryController.giveItem(mEntity->getId(), std::move(*returnedItem));

                    if(!returnedItem)
                    {//ok, the item is back in the inventory, but since the container didn't want the item, I must now fail
                        LOG_V("Could not put in container, but I have it and will fail");
                        return fail();
                    }
                    else
                    {//wow wow... the container refused the item and my inventory refused the item. That puts me in a rather bad spot. I can't just let the item disappear and I can't put it anywhere. better crash
                        FEA_ASSERT(false, "Item cannot be put in container nor inventory and I didn't know what to do so I crashed :D");
                        return fail(); //if I reach this in release mode, the item will disappear
                    }
                }
                else
                {//all good, we have put the item!
                    LOG_V("Wow, I put an item in a container! I am proud!");
                    return succeed();
                }
            }
            else
            {
                LOG_E("Cannot take item out of inventory!");
                return fail();
            }
        }
    }
    else
    {//else we need to go to the container
        float acceptableDistance = glm::length((glm::vec2)HalfTiles::size())/4.f;

        return require({Jobs::GOTO, {{"position", HalfTiles::tileCenter(mTargetContainer->position)}, {"minimumDistance", acceptableDistance}}});
    }


    return working();
}

void PutItemInContainerSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void PutItemInContainerSolver::notifyFail(AnyMap<std::string> failedData)
{
}

void PutItemInContainerSolver::validateStack(th::Optional<ItemManager::ItemStackView>& itemStack) const
{
    auto stackView = mWorld.items().stackAt(itemStack->position);
    if(stackView)
    {//there is a stack for sure, now make sure it is the right type, if it is then we can use it
        if(stackView->stack.get().type == mItemType)
            itemStack.reset(*stackView);
        else
            itemStack.reset();
    }
    else
        return itemStack.reset();
}
