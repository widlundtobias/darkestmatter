#include "cleartilessolver.hpp"
#include "world/gameworld.hpp"
#include "entity/evadetilecontroller.hpp"
#include "world/tiles.hpp"
#include "entity/entitymessages.hpp"
#include "world/landscapesemantics.hpp"
#include "entity/entityfilters.hpp"

ClearTilesSolver::ClearTilesSolver(const Job& job, const AnyMap<std::string>& external):
    JobSolver::JobSolver(job, external),
    mWorld(external.at("world").get<std::reference_wrapper<GameWorld>>()),
    mEvadeTileController(external.at("evade_tiles").get<std::reference_wrapper<EvadeTileController>>()),
    mEntity(external.at("entity").get<fea::EntityPtr>()),
    mTilesToEvade(job.jobData.at("target_tiles").get<std::unordered_set<glm::ivec2>>()),
    mAddedEvadeTiles(false),
    mTimeSpentClearing(0)
{
}

ClearTilesSolver::~ClearTilesSolver()
{
    if(mAddedEvadeTiles)
    {
        for(const auto& tile : mTilesToEvade)
            mEvadeTileController.evadeTiles().erase(tile);
    }
}

JobSolver::UpdateResult ClearTilesSolver::update()
{
    if(!mAddedEvadeTiles)
    {//I haven't added evade tiles yet, let's do it
        mEvadeTileController.evadeTiles().insert(mTilesToEvade.begin(), mTilesToEvade.end());
        mAddedEvadeTiles = true;
    }
    
    bool cleared = true;

    //this only checks for entities. In the future, items should be checked for and carried away too
    for(const auto& tile : mTilesToEvade)
    {
        cleared = mWorld.entities(EntityFilters::overlapsTile(tile)).empty();
        if(!cleared)
            break;
    }

    if(cleared)
    {//I can't believe it is cleared! Success!
        return succeed();
    }

    if(mTimeSpentClearing > 600)
    {//this is not cleared after 10 seconds of trying. I'll just fail
        return fail();
    }

    ++mTimeSpentClearing;
    return working();
}

void ClearTilesSolver::notifySuccess(AnyMap<std::string> completedData)
{
}

void ClearTilesSolver::notifyFail(AnyMap<std::string> failedData)
{
}
