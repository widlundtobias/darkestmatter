#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "entity/iteminvokercontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class FindAndFetchSolver : public JobSolver
{
    public:
        FindAndFetchSolver(const Job& job, const AnyMap<std::string>& external);
        JobSolver::UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        CarryController& mCarryController;
        fea::EntityPtr mEntity;
        ItemInvokerController& mItemInvokerController;

        std::deque<ItemManager::ItemStackView> mAvailableStacks;
        th::Optional<ItemManager::ItemStackView> mTargetStack;

        int32_t mItemTypeId;
        const ItemType& mItemType;

        bool mItemFetched;
};
