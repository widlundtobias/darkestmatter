#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;
class ItemInvokerController;

class FetchFromContainerSolver : public JobSolver
{
    public:
        FetchFromContainerSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        CarryController& mCarryController;
        ItemInvokerController& mItemInvokerController;
        fea::EntityPtr mEntity;

        int32_t mContainerStackId;
        int32_t mItemTypeId;
        const ItemType& mItemType;
        th::Optional<ItemManager::ItemStackView> mTargetContainer;
};
