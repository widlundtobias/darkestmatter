#pragma once
#include <unordered_set>
#include <fea/entitysystem.hpp>
#include <fea/rendering/glmhash.hpp>
#include "jobai/jobsolver.hpp"

class GameWorld;
class EvadeTileController;

class ClearTilesSolver : public JobSolver
{
    public:
        ClearTilesSolver(const Job& job, const AnyMap<std::string>& external);
        ~ClearTilesSolver();
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        GameWorld& mWorld;
        EvadeTileController& mEvadeTileController;
        fea::EntityPtr mEntity;
        const std::unordered_set<glm::ivec2> mTilesToEvade;
        bool mAddedEvadeTiles;
        int32_t mTimeSpentClearing;
};
