#pragma once
#include "jobai/jobsolver.hpp"
#include "entity/carrycontroller.hpp"
#include "world/itemmanager.hpp"

class GameWorld;

class PutItemOnStackSolver : public JobSolver
{
    public:
        PutItemOnStackSolver(const Job& job, const AnyMap<std::string>& external);
        UpdateResult update() override;
        void notifySuccess(AnyMap<std::string> completedData) override;
        void notifyFail(AnyMap<std::string> failedData) override;
    private:
        fea::MessageBus& mBus;
        GameWorld& mWorld;
        fea::EntityPtr mEntity;
        CarryController& mCarryController;

        int32_t mItemTypeId;
        glm::ivec2 mStackPosition;

        bool mCannotGo;
};
