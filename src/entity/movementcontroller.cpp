#include "movementcontroller.hpp"
#include "movementtype.hpp"
#include "movementaction.hpp"
#include "util/accelerator.hpp"

MovementController::MovementController(fea::MessageBus& bus):
    mBus(bus)
{
    subscribe(mBus, *this);
}

bool MovementController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("movement_direction") &&
                entity->hasAttribute("movement_type") &&
                entity->hasAttribute("movement_action") &&
                entity->hasAttribute("max_acceleration") &&
                entity->hasAttribute("max_speed") &&
                entity->hasAttribute("acceleration");

    return keep;
}

void MovementController::entityKept(fea::EntityPtr entity)
{
    MovementType type = entity->getAttribute<MovementType>("movement_type");

    (void)type;
    //determine the correct mover in the future
}

void MovementController::entityDestroyed(fea::EntityPtr entity)
{
    
}

void MovementController::update(float deltaTime)
{
    for(auto entityIter : mEntities)
    {
        auto entity = entityIter.second;
        //right now just pretend everything is a land mover

        MovementAction movementAction = entity->getAttribute<MovementAction>("movement_action");
        glm::vec2 moveDirection = entity->getAttribute<glm::vec2>("movement_direction");

        if(movementAction == MovementAction::IDLE)
        {
            FEA_ASSERT(moveDirection == glm::vec2(), "Cannot have a movement direction when being idle");
        }
        else if(movementAction == MovementAction::WALK || movementAction == MovementAction::RUN)
        {
            float distanceToNormalized = std::fabs(glm::length(moveDirection) - 1.0f);
            FEA_ASSERT(distanceToNormalized < 0.001f, "Direction not normalized when running/walking");

            (void)distanceToNormalized;
        }

        float maxSpeed = entity->getAttribute<float>("max_speed");
        float maxAcceleration = entity->getAttribute<float>("max_acceleration");
        glm::vec2 currentVelocity = entity->getAttribute<glm::vec2>("velocity");

        entity->setAttribute("acceleration", Accelerator::get(moveDirection, maxSpeed, currentVelocity, maxAcceleration));
    }
}

void MovementController::handleMessage(const MovementIntentionMessage& message)
{
    auto entityIter = mEntities.find(message.entityId);

    if(entityIter != mEntities.end())
    {
        fea::EntityPtr entity = entityIter->second;

        MovementAction movementAction = message.action;
        glm::vec2 moveDirection = message.direction;

        if(movementAction == MovementAction::IDLE)
        {
            FEA_ASSERT(moveDirection == glm::vec2(), "Cannot have a movement direction when being idle");
        }
        else if(movementAction == MovementAction::WALK || movementAction == MovementAction::RUN)
        {
            float distanceToNormalized = std::fabs(glm::length(moveDirection) - 1.0f);
            FEA_ASSERT(distanceToNormalized < 0.001f, "Direction not normalized when running/walking");

            (void)distanceToNormalized;
        }

        entity->setAttribute("movement_direction", moveDirection);
        entity->setAttribute("movement_action", movementAction);
    }
}
