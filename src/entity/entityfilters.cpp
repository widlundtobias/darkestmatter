#include "entityfilters.hpp"
#include "world/tiles.hpp"

std::function<bool(fea::EntityPtr)> EntityFilters::overlapsTile(const glm::ivec2& tile)
{
    return [=] (fea::EntityPtr entity)
    {
        if(entity->hasAttribute("width") && entity->hasAttribute("position"))
        {
            const glm::vec2& position = entity->getAttribute<glm::vec2>("position");
            float halfWidth = entity->getAttribute<float>("width") / 2.0f;
            if(Tiles::withinTile(position + glm::vec2(-halfWidth, -halfWidth), tile) ||
               Tiles::withinTile(position + glm::vec2(halfWidth, -halfWidth), tile) ||
               Tiles::withinTile(position + glm::vec2(-halfWidth, halfWidth), tile) ||
               Tiles::withinTile(position + glm::vec2(halfWidth, halfWidth), tile))
                return true;
        }
        return false;
    }; 
}
