#include "iteminvokercontroller.hpp"
#include "iteminvokertype.hpp"
#include "iteminvokertypefactory.hpp"

ItemInvokerController::ItemInvokerController(fea::MessageBus& bus, ItemManager& itemManager):
    mBus(bus),
    mItemManager(itemManager)
{
    (void)mBus;
}

bool ItemInvokerController::keepEntity(fea::EntityPtr entity) const
{
    bool keep = entity->hasAttribute("item_invoker_type") &&
                entity->hasAttribute("item_invoker");

    return keep;
}

void ItemInvokerController::entityKept(fea::EntityPtr entity)
{
    ItemInvokerType itemInvokerType = entity->getAttribute<ItemInvokerType>("item_invoker_type");

    entity->setAttribute("item_invoker", ItemInvokerTypeFactory::generate(entity, itemInvokerType));
}

void ItemInvokerController::entityDestroyed(fea::EntityPtr entity)
{
}

void ItemInvokerController::update(float deltaTime)
{
    for(auto entityIter : mEntities)
    {
    }
}

th::Optional<AnyMap<std::string>> ItemInvokerController::invokeItem(fea::EntityId entityId, Item& item, ItemAction action, AnyMap<std::string> parameters)
{
    FEA_ASSERT(mEntities.count(entityId) != 0, "Id " << entityId << " given but that entity doesn't have an item invoker controller attached\n");

    fea::EntityPtr entity = mEntities.at(entityId);
    
    EntityItemInvoker& itemInvoker = entity->getAttribute<EntityItemInvoker>("item_invoker");

    return item.invoke(action, *itemInvoker, std::move(parameters));
}

th::Optional<AnyMap<std::string>> ItemInvokerController::invokeItemStack(fea::EntityId entityId, int32_t stackId, int32_t stackIndex, ItemAction action, AnyMap<std::string> parameters)
{
    FEA_ASSERT(mEntities.count(entityId) != 0, "Id " << entityId << " given but that entity doesn't have an item invoker controller attached\n");

    fea::EntityPtr entity = mEntities.at(entityId);
    
    EntityItemInvoker& itemInvoker = entity->getAttribute<EntityItemInvoker>("item_invoker");

    return mItemManager.invokeItem(stackId, stackIndex, action, *itemInvoker, std::move(parameters));
}
