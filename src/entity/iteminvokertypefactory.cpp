#include "iteminvokertypefactory.hpp"
#include "entity/iteminvokers/humanoiditeminvoker.hpp"

EntityItemInvoker ItemInvokerTypeFactory::generate(fea::EntityPtr entity, ItemInvokerType type)
{
    if(type == ItemInvokerType::HUMANOID)
    {
        return HumanoidItemInvoker(entity);
    }
    else
    {
        FEA_ASSERT(false, "unsupported item invoker type " << type);
        return HumanoidItemInvoker(entity);
    }
}
