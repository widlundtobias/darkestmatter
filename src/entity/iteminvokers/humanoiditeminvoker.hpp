#pragma once
#include <fea/entitysystem.hpp>
#include "items/iteminvoker.hpp"

class HumanoidItemInvoker : public ItemInvoker
{
    public:
        HumanoidItemInvoker(fea::EntityPtr entity);
    private:
        fea::EntityPtr mEntity;
};
