#pragma once
#include <map>
#include <fea/util.hpp>
#include <fea/entitysystem.hpp>
#include <thero/optional.hpp>
#include "jobai/job.hpp"
#include "entity/jobpriority.hpp"
#include "jobai/brain.hpp"
#include <util/numberpool.hpp>
#include "entity/jobid.hpp"

class Facility;
class GameWorld;
class EvadeTileController;
class CarryController;
class ItemInvokerController;

class TaskAIController: public fea::EntityController
{
    public:
        struct SolvingEntry
        {
            JobId jobId;
            JobPriority priority;
        };

        struct StashedSolving
        {
            JobId jobId;
            SolverStack solverStack;
        };
        
        TaskAIController(fea::MessageBus& bus, GameWorld& gameWorld, EvadeTileController& evadeTileController, CarryController& carryController, ItemInvokerController& itemInvokerController);
        virtual bool keepEntity(fea::EntityPtr entity) const override;
        virtual void entityKept(fea::EntityPtr entity) override;
        virtual void entityDestroyed(fea::EntityPtr entity) override;
        virtual void update(float deltaTime) override;
        th::Optional<JobId> doJob(fea::EntityId id, const Job& job, JobPriority priority = JobPriority::NORMAL);
    private:
        void doneWithTask(fea::EntityId entityId);
        fea::MessageBus& mBus;
        GameWorld& mGameWorld;
        EvadeTileController& mEvadeTileController;
        CarryController& mCarryController;
        ItemInvokerController& mItemInvokerController;
        NumberPool<JobId> mJobIdPool;
        std::unordered_map<fea::EntityId, SolvingEntry> mCurrentlySolving;
        std::unordered_map<fea::EntityId, std::map<JobPriority, StashedSolving>> mStoredStacks;
};
