#include "relationshipdatabase.hpp"

RelationshipDatabase::RelationshipDatabase(std::unordered_map<ItemProperty, std::vector<ItemProperty>> properties):
    mProperties(properties)
{
}

bool RelationshipDatabase::is(ItemProperty a, ItemProperty b) const
{
    if(a == b)
        return true;

    auto superSetListIterator = mProperties.find(a);

    if(superSetListIterator != mProperties.end())
    {
        for(ItemProperty superSet : superSetListIterator->second)
        {
            if(is(superSet, b))
                return true;
        }
    }

    return false;
}
