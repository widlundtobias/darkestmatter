#include "itemproperties.hpp"
#include <fea/assert.hpp>

std::unordered_map<ItemProperty, std::vector<ItemProperty>> gItemPropertyRelations =
{
    //{ ANIMAL, { LIVING, ORGANIC }},
    //{ FELINE, { ANIMAL }},
    //{ TIGER, { FELINE, }},
    //{ LION, { FELINE, }},
    //{ TOMATO, { LIVING, ORGANIC }},
};

std::unordered_map<std::string, ItemProperty> gItemPropertyStringMap =
{
    {"construction_material", CONSTRUCTION_MATERIAL},
    {"metal", METAL},
    {"container", CONTAINER},
};

ItemProperty itemPropertyFromString(const std::string& string)
{
    class ItemPropertyStringMapVerifier
    {
        public:
            ItemPropertyStringMapVerifier()
            {
                for(ItemProperty property : ItemProperty_list)
                {
                    bool existed = false;

                    for(const auto& entry : gItemPropertyStringMap)
                    {
                        if(entry.second == property)
                        {
                            existed = true;
                            break;
                        }
                    }

                    FEA_ASSERT(existed, "Item property '" << to_string(property) << "' has no string representation given, or it is a duplicate!");
                }
            };
    };

    static ItemPropertyStringMapVerifier verifier;

    FEA_ASSERT(gItemPropertyStringMap.count(string) != 0, "Given property '" << string << "' does not exist!");

    return gItemPropertyStringMap.at(string);
}
