#pragma once
#include <thero/optional.hpp>
#include "itemtype.hpp"
#include "itemaction.hpp"
#include "iteminvoker.hpp"

class Item
{
    public:
        Item(const ItemType& type, AnyMap<std::string> attributes);
        th::Optional<AnyMap<std::string>> invoke(ItemAction action, ItemInvoker& invoker, AnyMap<std::string> parameters = {});
        const ItemType& type() const;
        void destroy(const ItemInvokerHandler&);
        const AnyMap<std::string>& attributes() const;
        AnyMap<std::string>& attributes();
        bool destroyed() const;
    private:
        std::reference_wrapper<const ItemType> mType;
        bool mDestroyed;
        AnyMap<std::string> mAttributes;
};
