#pragma once
#include "itemproperties.hpp"

class RelationshipDatabase
{
    public:
        RelationshipDatabase(std::unordered_map<ItemProperty, std::vector<ItemProperty>> properties);
        bool is(ItemProperty a, ItemProperty b) const;
    private:
        std::unordered_map<ItemProperty, std::vector<ItemProperty>> mProperties;
};
