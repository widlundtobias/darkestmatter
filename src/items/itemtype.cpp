#include "itemtype.hpp"

bool operator==(const ItemType& a, const ItemType& b)
{
    return a.typeId == b.typeId;
}
