#pragma once
#include <unordered_map>
#include <thero/smartenum.hpp>

smart_enum(ItemProperty, CONSTRUCTION_MATERIAL, METAL, CONTAINER)

namespace std
{
    template<> struct hash<ItemProperty>
    {
        size_t operator()(const ItemProperty& x) const
        {
            return static_cast<size_t>(x);
        }
    };
}

extern std::unordered_map<ItemProperty, std::vector<ItemProperty>> gItemPropertyRelations;
extern std::unordered_map<std::string, ItemProperty> gItemPropertyStringMap;

ItemProperty itemPropertyFromString(const std::string& string);
