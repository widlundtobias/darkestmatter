#include "inventory.hpp"

Inventory::Inventory(th::Optional<int32_t> maxCarryAmount, th::Optional<int64_t> maxCarryWeight, th::Optional<ItemSize> maxCarrySize):
    mMaxCarryAmount(maxCarryAmount),
    mMaxCarryWeight(maxCarryWeight),
    mMaxCarrySize(maxCarrySize)
{
}

th::Optional<Item> Inventory::insert(Item item)
{
    if(item.destroyed())
    {//this item doesn't exist anymore so just let it die
        return{};
    }

    if(canFit(item.type()))
    {
        mItems.push_back(std::move(item));
        return {};
    }
    else
    {
        return {std::move(item)};
    }
}

bool Inventory::canFit(const ItemType& itemType) const
{
    bool fits = true;

    if(mMaxCarryAmount)
    {
        if(static_cast<int32_t>(mItems.size()) == *mMaxCarryAmount)
            fits = false;
    }

    if(fits && mMaxCarryWeight)
    {
        int64_t weight = itemType.weight;

        if(weight + totalWeight() > *mMaxCarryWeight)
            fits = false;
    }

    if(fits && mMaxCarrySize)
    {
        ItemSize size = itemType.size;

        if(size + totalSize() > *mMaxCarrySize)
            fits = false;
    }

    return fits;
}

int64_t Inventory::totalWeight() const
{
    int64_t totalWeight = 0;

    for(const auto& item : mItems)
        totalWeight += item.type().weight;

    return totalWeight;
}

ItemSize Inventory::totalSize() const
{
    ItemSize totalSize = 0;

    for(const auto& item : mItems)
        totalSize += item.type().size;

    return totalSize;
}

const std::deque<Item>& Inventory::items() const
{
    return mItems;
}

int32_t Inventory::itemCount(int32_t id) const //can be cached
{
    int32_t count = 0;

    for(const auto& item : mItems)
        if(item.type().typeId == id)
            count++;

    return count;
}

th::Optional<Item> Inventory::takeItem(int32_t id)
{
    for(size_t x = 0; x < mItems.size(); x++)
    {
        if(mItems[x].type().typeId == id)
        {
            Item item = std::move(mItems[x]);
            mItems.erase(mItems.begin() + static_cast<long>(x));
            return std::move(item);
        }
    }

    return {};
}

bool Inventory::hasItem(int32_t id) const
{
    for(size_t x = 0; x < mItems.size(); x++)
    {
        if(mItems[x].type().typeId == id)
            return true;
    }

    return false;
}
