#pragma once
#include <deque>
#include "item.hpp"

class Inventory
{
    public:
        Inventory(th::Optional<int32_t> maxCarryAmount = {}, th::Optional<int64_t> maxCarryWeight = {}, th::Optional<ItemSize> maxCarrySize = {});
        th::Optional<Item> insert(Item item);
        bool canFit(const ItemType& itemType) const;
        int64_t totalWeight() const;
        ItemSize totalSize() const;
        const std::deque<Item>& items() const;
        int32_t itemCount(int32_t id) const;
        th::Optional<Item> takeItem(int32_t id);
        bool hasItem(int32_t id) const;
    private:
        th::Optional<int32_t> mMaxCarryAmount;
        th::Optional<int64_t> mMaxCarryWeight;
        th::Optional<ItemSize> mMaxCarrySize;
        std::deque<Item> mItems;
};
