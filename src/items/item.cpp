#include "invokehandler.hpp"
#include "item.hpp"
#include <fea/assert.hpp>

Item::Item(const ItemType& type, AnyMap<std::string> attributes):
    mType(type),
    mDestroyed(false),
    mAttributes(std::move(attributes))
{
}

th::Optional<AnyMap<std::string>> Item::invoke(ItemAction action, ItemInvoker& invoker, AnyMap<std::string> parameters)
{
    FEA_ASSERT(!mDestroyed, "Invoking consumed item is a bug");

    if(!mDestroyed)
    {
        auto invokerIter = mType.get().mInvokeHandlers.find(action);
        if(invokerIter != mType.get().mInvokeHandlers.end())
        {
            auto ret = invokerIter->second.get().invoke(action, *this, invoker, std::move(parameters));
            return ret;
        }
        else
            return th::Optional<AnyMap<std::string>>{{}};
    }
    else
    {
        return {};
    }
}

void Item::destroy(const ItemInvokerHandler&)
{
    mDestroyed = true;
}

const AnyMap<std::string>& Item::attributes() const
{
    return mAttributes;
}

AnyMap<std::string>& Item::attributes()
{
    return mAttributes;
}

const ItemType& Item::type() const
{
    return mType;
}

bool Item::destroyed() const
{
    return mDestroyed;
}
