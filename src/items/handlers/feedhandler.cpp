#include "feedhandler.hpp"
#include "items/item.hpp"

AnyMap<std::string> FeedHandler::invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters)
{
    std::cout << "eat eat\n";
    item.destroy(*this);

    return {};
}
