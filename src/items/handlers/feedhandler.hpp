#pragma once
#include "../item.hpp"

class FeedHandler : public ItemInvokerHandler
{
    public:
        AnyMap<std::string> invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters) override;
};
