#pragma once
#include "../iteminvoker.hpp"

class InspectHandler : public ItemInvokerHandler
{
    public:
        AnyMap<std::string> invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters) override;
};
