#include "takeouthandler.hpp"
#include "items/inventory.hpp"
#include <fea/assert.hpp>

AnyMap<std::string> TakeOutHandler::invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters)
{
    FEA_ASSERT(parameters.count("type") != 0, "Must provide 'type' parameter when removing");
    FEA_ASSERT(parameters.at("type").isOfType<int32_t>(), "'type' parameter must be of type int32_t");

    int32_t itemToTake = parameters.at("type").get<int32_t>();

    FEA_ASSERT(item.attributes().count("inventory") != 0, "Item has take out handler but no inventory. My type id is " << item.type().typeId);
    FEA_ASSERT(item.attributes().at("inventory").isOfType<Inventory>(), "Item has inventory but it is not of type Inventory");

    Inventory& inventory = item.attributes().at("inventory").get<Inventory>();

    auto returnedItem = inventory.takeItem(itemToTake);

    if(returnedItem)
        return {{"item", std::move(*returnedItem)}};

    return {};
}
