#include "puthandler.hpp"
#include "items/inventory.hpp"
#include <fea/assert.hpp>

AnyMap<std::string> PutHandler::invoke(ItemAction action, Item& item, ItemInvoker& invoker, AnyMap<std::string> parameters)
{
    FEA_ASSERT(parameters.count("item") != 0, "Must provide 'item' parameter when putting");
    FEA_ASSERT(parameters.at("item").isOfType<Item>(), "'item' parameter must be of type Item");

    Item itemToPut = std::move(parameters.at("item").get<Item>());

    FEA_ASSERT(item.attributes().count("inventory") != 0, "Item has put handler but no inventory. My type id is " << item.type().typeId);
    FEA_ASSERT(item.attributes().at("inventory").isOfType<Inventory>(), "Item has inventory but it is not of type Inventory");

    Inventory& inventory = item.attributes().at("inventory").get<Inventory>();

    if(inventory.canFit(itemToPut.type()))
    {
        inventory.insert(std::move(itemToPut));
    }
    else
    {//can't fit it, so return it
        return {{"item", std::move(itemToPut)}};
    }

    return {};
}
