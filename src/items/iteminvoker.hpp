#pragma once

#include "invokehandler.hpp"
#include "itemaction.hpp"

class ItemInvoker
{
};

class Item;

using ItemInvokerHandler = InvokeHandler<Item, ItemAction, ItemInvoker>;
