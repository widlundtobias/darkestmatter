#pragma once

#ifndef NLOG
#include <sstream>
#include "messages.hpp"
#   define LOG(message, logLevel) \
        { \
            std::stringstream msg;\
            msg << message; \
            mBus.send(LogMessage{msg.str(), logLevel}); \
        }
#   define LOG_E(message) \
        LOG(message, LogLevel::ERROR);
#   define LOG_W(message) \
        LOG(message, LogLevel::WARNING);
#   define LOG_I(message) \
        LOG(message, LogLevel::INFO);
#   define LOG_V(message) \
        LOG(message, LogLevel::VERBOSE);
#else
#   define LOG_E(message) do { } while (false)
#endif

