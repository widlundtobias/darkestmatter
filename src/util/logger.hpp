#pragma once
#include "loglevel.hpp"
#include <string>
#include <vector>

class Logger
{
    public:
        Logger(LogLevel logLevel);
        ~Logger();
        void log(const std::string& message, LogLevel level);
        void setLogLevel(LogLevel logLevel);
        std::string getTimeString() const;
    private:
        void printLine(const std::string& lineStart, const std::string& message, LogLevel level) const;
        std::vector<std::string> explode(const std::string& str, const char& ch) const;
        std::string spaces(int32_t amount);
        LogLevel mLogLevel;
};
