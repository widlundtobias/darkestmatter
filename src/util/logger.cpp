#include "logger.hpp"
#include "console/console.hpp"
#include <chrono>

Logger::Logger(LogLevel logLevel) :
    mLogLevel(logLevel)
{
    Console::Initialise();
}

Logger::~Logger()
{
    Console::ResetColor();
}

void Logger::log(const std::string& message, LogLevel level)
{
    if(static_cast<int32_t>(level) <= static_cast<int32_t>(mLogLevel))
    {
        for(auto& line : explode(message, '\n'))
        {
            printLine("[" + getTimeString() + "|", line, level);
        }
    }
}

void Logger::setLogLevel(LogLevel level)
{
    mLogLevel = level;
}

std::string Logger::getTimeString() const
{
    auto now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    struct tm *parts = std::localtime(&now_c);

    std::string hour = parts->tm_hour < 10 ? std::string("0") + std::to_string(parts->tm_hour) : std::to_string(parts->tm_hour);
    std::string min = parts->tm_min < 10 ? std::string("0") + std::to_string(parts->tm_min) : std::to_string(parts->tm_min);
    std::string sec = parts->tm_sec < 10 ? std::string("0") + std::to_string(parts->tm_sec) : std::to_string(parts->tm_sec);

    return hour + ":" + min + ":" + sec;
}

void Logger::printLine(const std::string& lineStart, const std::string& message, LogLevel level) const
{
    Console::Write(lineStart);

    switch(level)
    {
        case LogLevel::ERROR:
            Console::SetFGColor(ConsoleColour::RED);
            Console::Write(std::string("ERROR  "));
            Console::ResetColor();
            break;
        case LogLevel::WARNING:
            Console::SetFGColor(ConsoleColour::YELLOW);
            Console::Write(std::string("WARNING"));
            Console::ResetColor();
            break;
        case LogLevel::INFO:
            Console::Write(std::string("INFO   "));
            break;
        case LogLevel::VERBOSE:
            Console::SetFGColor(ConsoleColour::CYAN);
            Console::Write(std::string("VERBOSE"));
            Console::ResetColor();
            break;
    }

    Console::Write(std::string("]: "));

    Console::Write(message + "\n");
    //std::cout << lineStart << message << "\n";
}

std::vector<std::string> Logger::explode(const std::string& str, const char& ch)  const
{
    std::string next;
    std::vector<std::string> result;

    // For each character in the string
    for (std::string::const_iterator it = str.begin(); it != str.end(); it++) 
    {
        // If we've hit the terminal character
        if (*it == ch) 
        {
            // If we have some characters accumulated
            if (!next.empty()) 
            {
                // Add them to the result vector
                result.push_back(next);
                next.clear();
            }
        } else 
        {
            // Accumulate the next character into the sequence
            next += *it;
        }
    }
    if (!next.empty())
        result.push_back(next);
    return result;
}

std::string Logger::spaces(int32_t amount)
{
    std::string result;

    for(int32_t i = 0; i < amount; i++)
        result.push_back(' ');

    return result;
}
