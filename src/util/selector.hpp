#pragma once
#include "precompiled.h"

template<typename Type>
using Selector = th::RandomSelector<Type>;
