#pragma once
#include <thero/any.hpp>
#include <unordered_map>

template<typename Key>
using AnyMap = std::unordered_map<Key, th::Any>;
