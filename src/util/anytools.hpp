#pragma once

class AnyTools
{
    public:
        template<typename ValueType, typename KeyType>
        static th::Optional<ValueType> getOrNone(const KeyType& key, const std::unordered_map<KeyType, th::Any>& map)
        {
            auto iterator = map.find(key);

            if(iterator != map.end())
            {
                return iterator->second.template get<ValueType>();
            }
            else
            {
                return {};
            }
        }

        template<typename ValueType, typename KeyType>
        static ValueType getOrAssert(const KeyType& key, const std::unordered_map<KeyType, th::Any>& map)
        {
            auto value = getOrNone<ValueType>(key, map);

            TH_ASSERT(value, "Value at " << key << " doesn't exist in the map");

            return *value;
        }

        template<typename ValueType, typename KeyType>
        static th::Optional<ValueType> moveOutOrNone(const KeyType& key, std::unordered_map<KeyType, th::Any>& map)
        {
            auto iterator = map.find(key);

            if(iterator != map.end())
            {
                return std::move(iterator->second.template get<ValueType>());
            }
            else
            {
                return {};
            }
        }

        template<typename ValueType, typename KeyType>
        static ValueType moveOutOrAssert(const KeyType& key, const std::unordered_map<KeyType, th::Any>& map)
        {
            auto value = moveOutOrNone<ValueType>(key, map);

            TH_ASSERT(value, "Value at " << key << " doesn't exist in the map");

            return *value;
        }
};
