#include "cappedint.hpp"
#include <algorithm>

CappedInt::CappedInt(int32_t min, int32_t max, int32_t number):
    mMin(min),
    mMax(max)
{
    *this = number;
}

CappedInt::operator int32_t() const
{
    return mNumber;
}

CappedInt& CappedInt::operator = (int32_t number)
{
    mNumber = std::min(mMax, std::max(mMin, number));
    return *this;
}

CappedInt& CappedInt::operator ++()
{
    int32_t number = mNumber + 1;
    *this = number;
    return *this;
}

CappedInt& CappedInt::operator --()
{
    int32_t number = mNumber - 1;
    *this = number;
    return *this;
}

CappedInt& CappedInt::operator +=(int32_t number)
{
    int32_t newNumber = mNumber + number;
    *this = newNumber;
    return *this;
}

CappedInt& CappedInt::operator -=(int32_t number)
{
    int32_t newNumber = mNumber - number;
    *this = newNumber;
    return *this;
}

CappedInt& CappedInt::operator *=(int32_t number)
{
    int32_t newNumber = mNumber * number;
    *this = newNumber;
    return *this;
}

CappedInt& CappedInt::operator /=(int32_t number)
{
    int32_t newNumber = mNumber / number;
    *this = newNumber;
    return *this;
}
