#pragma once
#include "resources/walls.hpp"

class ResourceStorage;

class StandardUIFactory
{
    public:
        static gim::Element create(const glm::ivec2& size);
        static gim::Element wallSelection(const WallResource& wall, const ResourceStorage& resources);
        static gim::Element wallImage(const WallResource& wall, const glm::ivec2& offset);
        static gim::Element materialRequirement(const WallResource::MaterialRequirement& requirement, const glm::ivec2& offset, const ResourceStorage& resources);
};
