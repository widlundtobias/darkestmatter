#pragma once
#include "precompiled.h"
#include "util/loglevel.hpp"

struct QuitMessage
{
};

struct LogMessage
{
    const std::string& message;
    LogLevel level;
};

struct ResizeMessage
{
    glm::uvec2 size;
};


struct TogglePauseMessage
{
};

struct PlayMusicMessage
{
    std::string name;
    bool loop;
};

struct PlaySoundMessage
{
    std::string name;
    bool loop;
};

struct StopSoundMessage
{
};

struct MouseClickMessage
{
    glm::uvec2 position;
    fea::Mouse::Button button;   
};

struct MouseReleaseMessage
{
    glm::uvec2 position;
};

struct MouseMoveMessage
{
    glm::uvec2 newPosition;
    glm::uvec2 lastPosition;
};

struct KeyPressedMessage
{
    int32_t key;
};

struct ViewPanDirectionMessage
{
    glm::vec2 direction;
};

struct ViewZoomedMessage
{
    int32_t delta;
};
